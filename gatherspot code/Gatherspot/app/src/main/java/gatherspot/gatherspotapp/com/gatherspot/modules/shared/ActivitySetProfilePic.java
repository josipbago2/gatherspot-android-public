package gatherspot.gatherspotapp.com.gatherspot.modules.shared;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.modules.loginreg.ActivityTutorial;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Josip on 24.8.2015..
 */
public class ActivitySetProfilePic extends AppCompatActivity {

    Button done;
    ImageView profilePic;
    ImageButton addPhoto, takePhoto;

    RestAdapter adapter;
    ApiInterface api;

    ProgressBar pb;

    private static final String ENDPOINT = "https://service.gatherspot.me";

    SharedPreferences myTokenPrefs, gcmPrefs;
    SharedPreferences.Editor edtToken;
    String token, user_id;

    Uri selectedImageUri;
    TypedFile typedFile;

    int cameraO;
    private static final int SELECT_PICTURE = 1;
    File photoFile;
    private String selectedImagePath;
    String mCurrentPhotoPath;
    private int imageSelected = 0;
    Bitmap bitmap, send;
    private static final int IMAGE_SELECTED = 1;
    private static final int IMAGE_NOT_SELECTED = 0;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setprofilepic_layout);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        pb = (ProgressBar) findViewById(R.id.progressBar);
        done = (Button) findViewById(R.id.btnDone);
        profilePic = (ImageView) findViewById(R.id.profilePic);
        addPhoto = (ImageButton) findViewById(R.id.addPhotoAlbum);
        takePhoto = (ImageButton) findViewById(R.id.addPhotoTake);

        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");
        edtToken = myTokenPrefs.edit();
        user_id = myTokenPrefs.getString("myuserid", "a");
        gcmPrefs = getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);

        api = InitHttpClient.getInstance().getApiInterface();

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {

                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent,
                                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                    }
                }
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 21) {
                    Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, SELECT_PICTURE);
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            SELECT_PICTURE);
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadDataPic();
            }
        });

    }

    private void uploadDataPic() {
        if (imageSelected == IMAGE_NOT_SELECTED) {
            Toast.makeText(this, "Choose your profile picture", Toast.LENGTH_SHORT).show();
        } else {
            pb.setVisibility(View.VISIBLE);
            done.setClickable(false);
            takePhoto.setClickable(false);
            addPhoto.setClickable(false);
            typedFile = new TypedFile("application/octet-stream",
                    imageToFile(send));
            api.setProfilePicture(token, typedFile,
                    new Callback<Response>() {
                        @Override
                        public void success(Response arg0, Response arg1) {
                            Intent intent = new Intent(ActivitySetProfilePic.this, ActivityTutorial.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                        @Override
                        public void failure(RetrofitError arg0) {
                            handleBan(arg0);
                            Response r = arg0.getResponse();
                            if (r != null && r.getStatus() == 401) {

                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.setProfilePicture(token, typedFile,
                                                        new Callback<Response>() {
                                                            @Override
                                                            public void success(Response response, Response response2) {
                                                                Intent intent = new Intent(ActivitySetProfilePic.this, ActivityTutorial.class);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                pb.setVisibility(View.INVISIBLE);
                                                                done.setClickable(true);
                                                                takePhoto.setClickable(true);
                                                                addPhoto.setClickable(true);
                                                                handleBan(error);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                pb.setVisibility(View.INVISIBLE);
                                                done.setClickable(true);
                                                takePhoto.setClickable(true);
                                                addPhoto.setClickable(true);
                                                handleBan(error);
                                            }
                                        });

                            }
                        }
                    });
        }

    }

    private File imageToFile(Bitmap send) {
        File photo = new File(getApplicationContext().getCacheDir(), "photo");
        try {
            photo.createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        send.compress(Bitmap.CompressFormat.JPEG, 50, bos);
        byte[] bitmapdata = bos.toByteArray();

        try {
            FileOutputStream fos = new FileOutputStream(photo);
            fos.write(bitmapdata);
            fos.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return photo;
    }

    private Bitmap resizeImage(Bitmap image) {
        Bitmap imageToSend = image;
        float ratio = 1;
        int height = image.getHeight();
        int width = image.getWidth();
        if (width > 200 || height > 200) {
            int heightRatio = height - 200;
            int widthRatio = width - 200;
            if (heightRatio > 0 && widthRatio <= 0) {
                ratio = (float) image.getHeight() / 200;
            } else if (widthRatio > 0 && heightRatio <= 0) {
                ratio = (float) image.getWidth() / 200;
            } else if (widthRatio > 0 && heightRatio > 0) {
                if (widthRatio > heightRatio) {
                    ratio = (float) image.getWidth() / 200;
                } else {
                    ratio = (float) image.getHeight() / 200;
                }
            }
            height = (int) ((float) height / ratio);
            width = (int) ((float) width / ratio);
            imageToSend = Bitmap.createScaledBitmap(image, width, height, true);
        }

        return imageToSend;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private int checkOrientation(String imagePath) {
        int degree = 0;
        try {
            ExifInterface ei = new ExifInterface(imagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                default:
                    degree = 90;
            }
            return degree;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    private Bitmap setOrientation(Bitmap b, int orientation) {
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.setRotate(orientation);
            return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, false);
        } else {
            return b;
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 4;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeBitmap(String imagePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);
    }

    public Bitmap decodeBitmapDescriptor(Uri selectedImageUri, int reqWidth, int reqHeight) {
        Bitmap bitmap = null;
        ParcelFileDescriptor parcelFileDescriptor;
        FileDescriptor fileDescriptor;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            parcelFileDescriptor = getContentResolver()
                    .openFileDescriptor(selectedImageUri, "r");
            fileDescriptor = parcelFileDescriptor
                    .getFileDescriptor();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            parcelFileDescriptor.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            System.gc();
            if (requestCode == SELECT_PICTURE) {
                selectedImageUri = data.getData();

                selectedImagePath = getPath(ActivitySetProfilePic.this, selectedImageUri);

                cameraO = checkOrientation(selectedImagePath);


                if (Build.VERSION.SDK_INT < 19) {
                    bitmap = decodeBitmap(selectedImagePath, 300, 300);
                    imageSelected = IMAGE_SELECTED;
                    send = resizeImage(bitmap);
                    if (cameraO > 0) {
                        send = setOrientation(send, cameraO);
                    }

                } else {
                    bitmap = decodeBitmapDescriptor(selectedImageUri, 300, 300);
                    imageSelected = IMAGE_SELECTED;
                    send = resizeImage(bitmap);
                    if (cameraO > 0) {
                        send = setOrientation(send, cameraO);
                    }
                }
                profilePic.setImageBitmap(send);
            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
                Uri uri = Uri.fromFile(photoFile);
                selectedImagePath = getPath(ActivitySetProfilePic.this, uri);
                cameraO = checkOrientation(selectedImagePath);
                if (Build.VERSION.SDK_INT < 19) {
                    bitmap = decodeBitmap(selectedImagePath, 300, 300);
                    imageSelected = IMAGE_SELECTED;
                    send = resizeImage(bitmap);
                    if (cameraO > 0) {
                        send = setOrientation(send, cameraO);
                    }

                } else {
                    bitmap = decodeBitmapDescriptor(uri, 300, 300);
                    imageSelected = IMAGE_SELECTED;
                    send = resizeImage(bitmap);
                    if (cameraO > 0) {
                        send = setOrientation(send, cameraO);
                    }

                }
                profilePic.setImageBitmap(send);
            }

        }
    }

    public static String getPath(final Context context, final Uri uri) {

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    /*
    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null,
                null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }
    */

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivitySetProfilePic.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

}
