package gatherspot.gatherspotapp.com.gatherspot.utils;

/**
 * Created by Josip on 29.5.2015..
 */

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class FullScreenImageActivity extends AppCompatActivity {

    String image;
    ImageView fsImage;
    PhotoViewAttacher mAttacher;
    ImageButton close;
    ProgressBar progressBar;

    Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen_image_activity);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        fsImage = (ImageView) findViewById(R.id.full_screen_image);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        Intent intent = getIntent();
        image = intent.getStringExtra("URLimage");

        progressBar.setVisibility(View.VISIBLE);
        picasso = InitHttpClient.getInstance().getImageLoader();
        picasso.load(image).placeholder(R.drawable.grey_image)
                .into(fsImage, new Callback() {

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                        progressBar.setVisibility(View.GONE);
                    }

                });

        close = (ImageButton) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        mAttacher = new PhotoViewAttacher(fsImage);
    }

}
