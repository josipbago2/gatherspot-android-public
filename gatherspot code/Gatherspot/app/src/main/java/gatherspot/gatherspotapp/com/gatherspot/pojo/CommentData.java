package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 29.5.2015..
 */

import java.io.Serializable;

public class CommentData implements Serializable {

    private String imageNumberFeedImage, imageNumberProfile;

    public String getImageNumberFeedImage() {
        return imageNumberFeedImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setImageNumberFeedImage(String imageNumberFeedImage) {
        this.imageNumberFeedImage = imageNumberFeedImage;

    }

    public String getImageNumberProfile() {
        return imageNumberProfile;
    }

    public void setImageNumberProfile(String imageNumberProfile) {
        this.imageNumberProfile = imageNumberProfile;
    }

    public String voteValueSend, numCommentsSend, usernameSend, timeStampSend,
            distanceSend, statusSend, place, upvoted, downvoted, postId, userId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUpvoted() {
        return upvoted;
    }

    public void setUpvoted(String upvoted) {
        this.upvoted = upvoted;
    }

    public String getDownvoted() {
        return downvoted;
    }

    public void setDownvoted(String downvoted) {
        this.downvoted = downvoted;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getVoteValueSend() {
        return voteValueSend;
    }

    public void setVoteValueSend(String voteValueSend) {
        this.voteValueSend = voteValueSend;
    }

    public String getNumCommentsSend() {
        return numCommentsSend;
    }

    public void setNumCommentsSend(String numCommentsSend) {
        this.numCommentsSend = numCommentsSend;
    }

    public String getUsernameSend() {
        return usernameSend;
    }

    public void setUsernameSend(String usernameSend) {
        this.usernameSend = usernameSend;
    }

    public String getTimeStampSend() {
        return timeStampSend;
    }

    public void setTimeStampSend(String timeStampSend) {
        this.timeStampSend = timeStampSend;
    }

    public String getDistanceSend() {
        return distanceSend;
    }

    public void setDistanceSend(String distanceSend) {
        this.distanceSend = distanceSend;
    }

    public String getStatusSend() {
        return statusSend;
    }

    public void setStatusSend(String statusSend) {
        this.statusSend = statusSend;
    }

}
