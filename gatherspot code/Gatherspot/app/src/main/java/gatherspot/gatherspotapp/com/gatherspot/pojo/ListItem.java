package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 29.5.2015..
 */
public class ListItem {

    private int imageNumberFeedImage, imageNumberProfile;
    private String username, numComments, distance, status, timeStamp,
            voteValue, type, place;

    private String info1, info2;

    //za my local posts
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumComments() {
        return numComments;
    }

    public void setNumComments(String numComments) {
        this.numComments = numComments;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(String voteValue) {
        this.voteValue = voteValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImageNumberProfile() {
        return imageNumberProfile;
    }

    public void setImageNumberProfile(int imageNumberProfile) {
        this.imageNumberProfile = imageNumberProfile;
    }

    public int getImageNumberFeedImage() {
        return imageNumberFeedImage;
    }

    public void setImageNumberFeedImage(int imageNumberFeedImage) {
        this.imageNumberFeedImage = imageNumberFeedImage;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getInfo1() {
        return info1;
    }

    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    public String getInfo2() {
        return info2;
    }

    public void setInfo2(String info2) {
        this.info2 = info2;
    }

}
