package gatherspot.gatherspotapp.com.gatherspot.modules.livefeed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.EndlessScrollListener;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.utils.InterleaveUtil;
import gatherspot.gatherspotapp.com.gatherspot.MainActivity;
import gatherspot.gatherspotapp.com.gatherspot.modules.newpost.ActivityNewPost;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterLiveFeed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.Feed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LocalPostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PanoramioImage;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PostItem;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Josip on 21.6.2015..
 */
public class LiveFeedFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener, SwipeRefreshLayout.OnRefreshListener, ObservableScrollViewCallbacks {


    Activity mActivity;
    View rootView;
    android.support.v7.app.ActionBar ab;

    ApiInterface api;

    SwipeRefreshLayout swipeLayout;
    ObservableListView lv1;
    ProgressBar lfCircle, footerCircle;
    View footerView;
    RelativeLayout background;

    LocationManager locationManager;

    String backgroundImageUrl;

    private static final String PAGE_SIZE = "10";

    Map<String, String> ppUsername;
    Map<String, String> ppProfilePic;
    Map<String, String> lpUsername;
    Map<String, String> lpProfilePic;
    List<PostItem> liveFeedList;
    List<PostItem> listOfPostsPersonal;
    List<PostItem> listOfPostsLocal;
    List<PostItem> listOfPostsAds;

    BitmapDrawable image;

    List<PostItem> listOfPostsLocalEmpty;
    List<PostItem> listOfPostsPersonalEmpty;

    Target target;

    String aToken;
    CallbackManager callbackManager;
    AccessTokenTracker tracker;

    boolean resume;
    boolean editRadius;
    boolean first;
    SharedPreferences radiusValue, myTokenPrefs,
            resumeHandler, firstRun, lUpdatesPrefs, loginTypePrefs, gcmPrefs, backgroundImagePrefs;
    String radius, token, loginType;
    SharedPreferences.Editor edtResume, edtRadius, edtFirstRun, edtlUpdatesPrefs, edtToken, edtImage;

    String longitude, latitude;
    double longitudeDouble;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;


    private void sharedPreferencesInitialize() {
        // "url" i "path"
        backgroundImagePrefs = mActivity.getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
        edtImage = backgroundImagePrefs.edit();

        gcmPrefs = mActivity.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = mActivity.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");
        firstRun = mActivity.getSharedPreferences("FIRST",
                Context.MODE_PRIVATE);
        edtFirstRun = firstRun.edit();
        first = firstRun.getBoolean("LIVEFEED", false);

        lUpdatesPrefs = mActivity.getSharedPreferences("lupdates",
                Context.MODE_PRIVATE);
        edtlUpdatesPrefs = lUpdatesPrefs.edit();

        myTokenPrefs = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");

        radiusValue = mActivity.getSharedPreferences("RADIUSPREF",
                Context.MODE_PRIVATE);
        edtRadius = radiusValue.edit();
        radius = radiusValue.getString("radius", "300");

        resumeHandler = mActivity.getSharedPreferences("RESUMEHANDLER",
                Context.MODE_PRIVATE);
        edtResume = resumeHandler.edit();
        resume = resumeHandler.getBoolean("resume?", true);

        edtResume.putBoolean("resume?", false);
        edtResume.commit();

    }

    private void initializeRestAdapter() {
        api = InitHttpClient.getInstance().getApiInterface();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferencesInitialize();

        setHasOptionsMenu(true);
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        buildGoogleApiClient();
        initializeRestAdapter();
        createLocationRequest(5000);

    }

    private void handleAccessTokenTracker() {
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
            }
            /*
            if(AccessToken.getCurrentAccessToken() == null){
                aToken = AccessToken.getCurrentAccessToken().getToken();
            }
            */
            tracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {
                    aToken = accessToken1.getToken();
                    if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
                        mGoogleApiClient.connect();
                    }
                }
            };
            callbackManager = CallbackManager.Factory.create();
        }
    }


    private void setBackground(String imageUrl) {

        backgroundImageUrl = imageUrl;
        if (!imageUrl.equals(backgroundImagePrefs.getString("url", ""))) {
            edtImage.putString("url", imageUrl);
            edtImage.commit();
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    // BackgroundImageHandler.saveToInternalSorage(bitmap, mActivity);
                    //bitmap = BackgroundImageHandler.blur(bitmap, mActivity);
                    bitmap = BackgroundImageHandler.changeBitmapContrastBrightness(bitmap, 0.45f, 130.f);
                    ColorMatrix matrix = new ColorMatrix();
                    matrix.setSaturation(0.45f);
                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                    image = new BitmapDrawable(mActivity.getResources(), bitmap);
                    image.setColorFilter(filter);
                    if (Build.VERSION.SDK_INT > 15) {
                        if (BackgroundImageHandler.fileExistance(mActivity)) {
                            BackgroundImageHandler.deleteFile(mActivity);
                        }
                        BackgroundImageHandler.saveToInternalStorage(bitmap, mActivity);
                        background.setBackground(image);
                    } else {
                        if (BackgroundImageHandler.fileExistance(mActivity)) {
                            BackgroundImageHandler.deleteFile(mActivity);
                        }
                        BackgroundImageHandler.saveToInternalStorage(bitmap, mActivity);
                        background.setBackgroundDrawable(image);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    backgroundImageUrl = backgroundImageUrl.replace("/original", "/large");
                    InitHttpClient.getInstance().getImageLoaderNoCert()
                            .load(backgroundImageUrl)
                            .resize(300, 540)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .centerCrop()
                            .into(target);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            InitHttpClient.getInstance().getImageLoaderNoCert()
                    .load(imageUrl)
                    .resize(300, 540)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .centerCrop()
                    .into(target);
        }
        /*
        if (imageUrl.equals(backgroundImagePrefs.getString("url", ""))) {
            if (Build.VERSION.SDK_INT > 15) {
                background.setBackground(new BitmapDrawable(mActivity.getResources(),
                        getImageReuse(backgroundImagePrefs.getString("path", ""))));
            } else {
                background.setBackgroundDrawable(new BitmapDrawable(mActivity.getResources(),
                        getImageReuse(backgroundImagePrefs.getString("path", ""))));
            }
        } else {
            edtImage.putString("url", imageUrl);
            edtImage.commit();
            InitHttpClient.getInstance().getImageLoaderNoCert()
                    .load(imageUrl)
                    .resize(280, 500)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            saveImageForReuse(bitmap);
                            if (Build.VERSION.SDK_INT > 15) {
                                background.setBackground(new BitmapDrawable(mActivity.getResources(), bitmap));
                            } else {
                                background.setBackgroundDrawable(new BitmapDrawable(mActivity.getResources(), bitmap));
                            }
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        */
    }

    private boolean checkGps() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkLocationProvider() {
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    protected void createLocationRequest(int interval) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        ab = ((AppCompatActivity) activity).getSupportActionBar();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (loginType.equals("facebook")) {
            if (tracker == null) {
                handleAccessTokenTracker();
            } else {
                if (!FacebookSdk.isInitialized()) {
                    FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
                }
                if (!tracker.isTracking()) {
                    tracker.startTracking();
                }
            }
        }
        if (!checkLocationProvider()) {
            if (myTokenPrefs.getBoolean("showwarning1", true)) {
                edtToken.putBoolean("showwarning1", false);
                edtToken.commit();
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mActivity);
                alertBuilder.setTitle("Enable Location Provider");
                alertBuilder.setMessage("Your phone's network location provider is disabled. You may get an inaccurate location fix or not get a fix at all." +
                        "Please turn your network location provider on. It's advisable you turn your GPS provider on as well.");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Enable Location Provider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        }
        if (!checkGps()) {
            if (myTokenPrefs.getBoolean("showwarning2", true)) {
                edtToken.putBoolean("showwarning2", false);
                edtToken.commit();
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mActivity);
                alertBuilder.setTitle("Enable GPS");
                alertBuilder.setMessage("Your GPS is disabled. " +
                        "You may get an inaccurate location fix or may not get a fix at all. Please enable your GPS in your phone settings");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        }


        if (loginType.equals("facebook")) {
            if (AccessToken.getCurrentAccessToken() != null) {
                aToken = AccessToken.getCurrentAccessToken().getToken();
                if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
            }
        } else {
            if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLocationUpdates();
        if (loginType.equals("facebook")) {
            if (tracker != null && tracker.isTracking()) {
                tracker.stopTracking();
            }
        }
        mGoogleApiClient.disconnect();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.livefeed_layout, null);

        // refresh circle za paging
        footerView = inflater.inflate(R.layout.feed_item_lf_circlescroll, null);
        background = (RelativeLayout) rootView.findViewById(R.id.layout);

        if (BackgroundImageHandler.fileExistance(mActivity)) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.45f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(mActivity.getResources(),
                    BackgroundImageHandler.loadImageFromStorage(mActivity));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
        }

        lfCircle = (ProgressBar) rootView.findViewById(R.id.progressBarLiveFeed);
        footerCircle = (ProgressBar) footerView.findViewById(R.id.progressBarFooter);
        swipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(R.id.swipe_container);

        // spec za refresh circle
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setSize(SwipeRefreshLayout.DEFAULT);
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_purple, android.R.color.holo_orange_light,
                android.R.color.holo_blue_dark);
        int actionBarHeight;
        int[] abSzAttr;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            abSzAttr = new int[]{android.R.attr.actionBarSize};
        } else {
            abSzAttr = new int[]{R.attr.actionBarSize};
        }
        TypedArray a = mActivity.obtainStyledAttributes(abSzAttr);
        actionBarHeight = a.getDimensionPixelSize(0, -1);
        swipeLayout.setProgressViewOffset(true, actionBarHeight + 15, actionBarHeight + 55);

        // listview init
        lv1 = (ObservableListView) rootView.findViewById(R.id.list);

        lv1.addFooterView(footerView);

        lv1.setScrollViewCallbacks(this);

        lv1.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (page == 0) {

                } else {
                    requestDataPersonal(page, true);
                }
            }
        });
        if (first == false) {
            first = true;
            System.gc();
            ((MainActivity) mActivity).startBackgroundService();
            edtlUpdatesPrefs.putBoolean("lonoff", true);
            edtlUpdatesPrefs.commit();
            edtFirstRun.putBoolean("LIVEFEED", true);
            edtFirstRun.commit();
        }

        return rootView;
    }



    /*
    private void setOnScrollListener(){

        lv1.setOnScrollListener(new EndlessScrollListener(1, 0) {

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                pagingInProgress = true;
                pagingCounter++;
                Log.d("NUMBER PAGE", String.valueOf(pagingCounter));
                requestDataPersonal(pagingCounter, pagingInProgress);

                /*
                if (firstRun) {
                    Log.d("NUMBER PAGE", "HERE FIRST RUN");
                    if(page - 1 > 0){
                        pagingInProgress = true;
                        Log.d("NUMBER PAGE", "a " + String.valueOf(page - 1));
                        requestDataPersonal(page - 1, pagingInProgress);
                    }
                } else {
                    Log.d("NUMBER PAGE", "HERE NOT FIRST RUN");
                    if(page > 0){
                        pagingInProgress = true;
                        Log.d("NUMBER PAGE", "b " + String.valueOf(page));
                        requestDataPersonal(page, pagingInProgress);
                    }
                }

            }
        });
    }*/


    @Override
    public void onConnected(Bundle bundle) {
        resume = resumeHandler.getBoolean("resume?", false);
        if (resume) {
            edtResume.putBoolean("resume?", false);
            edtResume.commit();
            // nema refresha, ažuriranje broja komentara lokalno ako iz komentara
        } else {
            footerCircle.setVisibility(View.VISIBLE);
            //lfCircle.setVisibility(View.VISIBLE);
            swipeLayout.setRefreshing(true);
            footerCircle.setVisibility(View.GONE);
            startLocationUpdates();
        }
    }

    private void updateUserLocation(final Location location) {
        api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                    @Override
                    public void success(PanoramioImage response, Response response2) {
                        if (response.getUrl() != null) {
                            setBackground(response.getUrl());
                        }
                        requestDataPersonal(0, false);
                    }

                    @Override
                    public void failure(RetrofitError arg0) {
                        handleBan(arg0);
                        Response r = arg0.getResponse();
                        if (r != null && r.getStatus() == 401) {
                            if (loginType.equals("facebook")) {
                                api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                        aToken, "facebook", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                                                        String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                                                            @Override
                                                            public void success(PanoramioImage response, Response response2) {
                                                                if (response.getUrl() != null) {
                                                                    setBackground(response.getUrl());
                                                                }

                                                                requestDataPersonal(0, false);
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            } else {
                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                                                        String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                                                            @Override
                                                            public void success(PanoramioImage response, Response response2) {
                                                                if (response.getUrl() != null) {
                                                                    setBackground(response.getUrl());
                                                                }
                                                                requestDataPersonal(0, false);
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            }
                        }
                    }
                });

    }

    @Override
    public void onLocationChanged(Location location) {
        // grab location (hvatanje lokacije nakon 2. updejta
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        longitudeDouble = location.getLongitude();
        updateUserLocation(location);
        stopLocationUpdates();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    public void onRefresh() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                //footerCircle.setVisibility(View.VISIBLE);
                swipeLayout.setEnabled(false);
                footerCircle.setVisibility(View.GONE);
                //lfCircle.setVisibility(View.GONE);
                startLocationUpdates();
            }
        });
    }


    private void setList() {
        liveFeedList = new ArrayList<PostItem>();
        if (lv1.getAdapter() != null) {
            ((ListAdapterLiveFeed) ((HeaderViewListAdapter) lv1.getAdapter()).getWrappedAdapter()).removeAll();
            lv1.setOnScrollListener(new EndlessScrollListener() {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    if (page == 0) {

                    } else {
                        requestDataPersonal(page, true);
                    }
                }
            });
        }

        liveFeedList = InterleaveUtil.interleave(listOfPostsPersonal,
                listOfPostsLocal, listOfPostsAds);
        ListAdapterLiveFeed listAdapter = new ListAdapterLiveFeed(
                mActivity, liveFeedList);
        lv1.setAdapter(listAdapter);
        footerCircle.setVisibility(View.VISIBLE);

        if (listOfPostsPersonal.size() < 10
                && listOfPostsAds.size() < 10
                && listOfPostsLocal.size() < 10) {
            footerCircle.setVisibility(View.GONE);
        }

        // na kraju counter = 0 i stop location updates i set adapter
        //lfCircle.setVisibility(View.GONE);
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
        swipeLayout.setRefreshing(false);
        swipeLayout.setRefreshing(false);
        swipeLayout.setEnabled(true);
    }

    private void setListPaging() {
        liveFeedList = new ArrayList<PostItem>();
        liveFeedList = InterleaveUtil.interleave(listOfPostsPersonal,
                listOfPostsLocal, listOfPostsAds);


        if (listOfPostsPersonal.size() < 10
                && listOfPostsAds.size() < 10
                && listOfPostsLocal.size() < 10) {
            footerCircle.setVisibility(View.GONE);
        }
        if (lv1.getAdapter() != null) {
            ((ListAdapterLiveFeed) ((HeaderViewListAdapter) lv1.getAdapter()).getWrappedAdapter()).addMore(liveFeedList);
        }
    }

    private void requestDataPersonal(final int pagingCounter, final boolean page) {
        radius = radiusValue.getString("radius", "300");

        api.getPersonalPosts(token, longitude, latitude, radius,
                PAGE_SIZE, String.valueOf(pagingCounter), new Callback<Feed>() {
                    @Override
                    public void failure(RetrofitError arg0) {
                        handleBan(arg0);
                        Response r = arg0.getResponse();
                        if (r != null && r.getStatus() == 401) {
                            if (loginType.equals("facebook")) {
                                api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                        aToken, "facebook", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.getPersonalPosts(token, longitude, latitude, radius,
                                                        PAGE_SIZE, String.valueOf(pagingCounter), new Callback<Feed>() {
                                                            @Override
                                                            public void success(Feed arg0, Response response) {
                                                                ppUsername = new HashMap<String, String>();
                                                                ppProfilePic = new HashMap<String, String>();

                                                                for (Map.Entry<String, Map<String, String>> userEntry : arg0
                                                                        .getUsers().entrySet()) {
                                                                    final String userId = userEntry.getKey();

                                                                    final Map<String, String> userProperties = userEntry
                                                                            .getValue();

                                                                    final String username = userProperties.get("username");
                                                                    final String avatarUrl = userProperties
                                                                            .get("profile_pic_url");

                                                                    ppUsername.put(userId, username);
                                                                    ppProfilePic.put(userId, avatarUrl);

                                                                }

                                                                listOfPostsPersonal = new ArrayList<PostItem>();
                                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                                    PostItem personalPost = new PostItem();
                                                                    personalPost.setId(item.getId());
                                                                    personalPost.setUser_id(item.getUser_id());
                                                                    personalPost.setStatus(item.getStatus());
                                                                    personalPost.setUsername(ppUsername.get(item.getUser_id()));
                                                                    if (ppProfilePic.get(item.getUser_id()) == null) {
                                                                        personalPost.setProfilePic("");
                                                                    } else {
                                                                        personalPost.setProfilePic(ppProfilePic.get(item
                                                                                .getUser_id()));
                                                                    }
                                                                    personalPost.setComments_number(item.getComments_number());
                                                                    if (item.getImage() == null) {
                                                                        personalPost.setImage("");
                                                                    } else {
                                                                        personalPost.setImage(item.getImage());
                                                                    }
                                                                    personalPost.setTimeStamp(item.getTimeStamp());
                                                                    personalPost.setUp_votes(item.getUp_votes());
                                                                    personalPost.setDownvoted(item.getDownvoted());
                                                                    personalPost.setUpvoted(item.getUpvoted());
                                                                    personalPost.setType("personal");
                                                                    listOfPostsPersonal.add(personalPost);
                                                                }
                                                                if (listOfPostsPersonal.isEmpty() && pagingCounter == 0) {
                                                                    listOfPostsPersonal = emptyPersonal();
                                                                }

                                                                requestDataLocal(pagingCounter, page);
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            } else {
                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.getPersonalPosts(token, longitude, latitude, radius,
                                                        PAGE_SIZE, String.valueOf(pagingCounter), new Callback<Feed>() {
                                                            @Override
                                                            public void success(Feed arg0, Response response) {
                                                                ppUsername = new HashMap<String, String>();
                                                                ppProfilePic = new HashMap<String, String>();

                                                                for (Map.Entry<String, Map<String, String>> userEntry : arg0
                                                                        .getUsers().entrySet()) {
                                                                    final String userId = userEntry.getKey();

                                                                    final Map<String, String> userProperties = userEntry
                                                                            .getValue();

                                                                    final String username = userProperties.get("username");
                                                                    final String avatarUrl = userProperties
                                                                            .get("profile_pic_url");

                                                                    ppUsername.put(userId, username);
                                                                    ppProfilePic.put(userId, avatarUrl);

                                                                }

                                                                listOfPostsPersonal = new ArrayList<PostItem>();
                                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                                    PostItem personalPost = new PostItem();
                                                                    personalPost.setId(item.getId());
                                                                    personalPost.setUser_id(item.getUser_id());
                                                                    personalPost.setStatus(item.getStatus());
                                                                    personalPost.setUsername(ppUsername.get(item.getUser_id()));
                                                                    if (ppProfilePic.get(item.getUser_id()) == null) {
                                                                        personalPost.setProfilePic("");
                                                                    } else {
                                                                        personalPost.setProfilePic(ppProfilePic.get(item
                                                                                .getUser_id()));
                                                                    }
                                                                    personalPost.setComments_number(item.getComments_number());
                                                                    if (item.getImage() == null) {
                                                                        personalPost.setImage("");
                                                                    } else {
                                                                        personalPost.setImage(item.getImage());
                                                                    }
                                                                    personalPost.setTimeStamp(item.getTimeStamp());
                                                                    personalPost.setUp_votes(item.getUp_votes());
                                                                    personalPost.setDownvoted(item.getDownvoted());
                                                                    personalPost.setUpvoted(item.getUpvoted());
                                                                    personalPost.setType("personal");
                                                                    listOfPostsPersonal.add(personalPost);
                                                                }
                                                                if (listOfPostsPersonal.isEmpty() && pagingCounter == 0) {
                                                                    listOfPostsPersonal = emptyPersonal();
                                                                }
                                                                requestDataLocal(pagingCounter, page);
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            }
                        }

                    }

                    @Override
                    public void success(Feed arg0, Response arg1) {
                        // TODO Auto-generated method stub

                        ppUsername = new HashMap<String, String>();
                        ppProfilePic = new HashMap<String, String>();

                        for (Map.Entry<String, Map<String, String>> userEntry : arg0
                                .getUsers().entrySet()) {
                            final String userId = userEntry.getKey();

                            final Map<String, String> userProperties = userEntry
                                    .getValue();

                            final String username = userProperties.get("username");
                            final String avatarUrl = userProperties
                                    .get("profile_pic_url");

                            ppUsername.put(userId, username);
                            ppProfilePic.put(userId, avatarUrl);

                        }

                        listOfPostsPersonal = new ArrayList<PostItem>();
                        for (PersonalPostItem item : arg0.getPersonalPosts()) {
                            PostItem personalPost = new PostItem();
                            personalPost.setId(item.getId());
                            personalPost.setUser_id(item.getUser_id());
                            personalPost.setStatus(item.getStatus());
                            personalPost.setUsername(ppUsername.get(item.getUser_id()));
                            if (ppProfilePic.get(item.getUser_id()) == null) {
                                personalPost.setProfilePic("");
                            } else {
                                personalPost.setProfilePic(ppProfilePic.get(item
                                        .getUser_id()));
                            }
                            personalPost.setComments_number(item.getComments_number());
                            if (item.getImage() == null) {
                                personalPost.setImage("");
                            } else {
                                personalPost.setImage(item.getImage());
                            }
                            personalPost.setTimeStamp(item.getTimeStamp());
                            personalPost.setUp_votes(item.getUp_votes());
                            personalPost.setDownvoted(item.getDownvoted());
                            personalPost.setUpvoted(item.getUpvoted());
                            personalPost.setType("personal");
                            listOfPostsPersonal.add(personalPost);
                        }
                        if (listOfPostsPersonal.isEmpty() && pagingCounter == 0) {
                            listOfPostsPersonal = emptyPersonal();
                        }

                        requestDataLocal(pagingCounter, page);

                    }

                });
    }

    private void requestDataLocal(final int pagingCounter, final boolean page) {

        listOfPostsLocal = new ArrayList<PostItem>();
        api.getLocalPosts(token, longitude, latitude, radius, PAGE_SIZE, String.valueOf(pagingCounter), new Callback<Feed>() {

            @Override
            public void failure(RetrofitError arg0) {

                handleBan(arg0);
            }

            @Override
            public void success(Feed arg0, Response arg1) {
                // TODO Auto-generated method stub

                lpUsername = new HashMap<String, String>();
                lpProfilePic = new HashMap<String, String>();

                for (Map.Entry<String, Map<String, String>> userEntry : arg0
                        .getUsers().entrySet()) {
                    final String userId = userEntry.getKey();
                    final Map<String, String> userProperties = userEntry
                            .getValue();


                    final String username = userProperties.get("username");
                    final String avatarUrl = userProperties
                            .get("profile_pic_url");

                    lpUsername.put(userId, username);
                    lpProfilePic.put(userId, avatarUrl);

                }

                for (LocalPostItem item : arg0.getFeed()) {
                    PostItem listItem = new PostItem();
                    listItem.setStatus(item.getStatus());
                    listItem.setTimeStamp(item.getTimeStamp());
                    listItem.setId(item.getId());
                    listItem.setUser_id(item.getUser_id());
                    listItem.setUsername(lpUsername.get(item.getUser_id()));
                    if (lpProfilePic.get(item.getUser_id()) == null) {
                        listItem.setProfilePic("");
                    } else {
                        listItem.setProfilePic(lpProfilePic.get(item
                                .getUser_id()));
                    }
                    listItem.setRadius(item.getRadius());
                    listItem.setVotes(item.getVotes());
                    listItem.setUpvoted(item.getUpvoted());
                    listItem.setDownvoted(item.getDownvoted());
                    listItem.setType("local");
                    listItem.setComments_number(item.getComments_number());
                    if (item.getImage() == null) {
                        listItem.setImage("");
                    } else {
                        listItem.setImage(item.getImage());
                    }
                    listOfPostsLocal.add(listItem);
                }
                if (listOfPostsLocal.isEmpty() && pagingCounter == 0) {
                    listOfPostsLocal = emptyLocal();
                }

                requestDataAds(pagingCounter, page);

            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingError) {
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(mActivity, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        } else {
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }
    }

    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(mActivity.getFragmentManager(), "errordialog");

    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            LiveFeedFragment fg = (LiveFeedFragment) getFragmentManager().findFragmentById(R.id.mainContent);
            fg.onDialogDismissed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loginType.equals("facebook")) {
            if (tracker != null && tracker.isTracking()) {
                tracker.stopTracking();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == Activity.RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    private List<PostItem> emptyLocal() {
        listOfPostsLocalEmpty = new ArrayList<PostItem>();

        PostItem localEmpty = new PostItem();
        localEmpty.setUsername("Gatherspot Team");
        localEmpty
                .setStatus("No local posts in your area. Post a new local post to your location and leave a mark on the place you are currently at!");
        localEmpty.setImage("emptylocal");
        localEmpty.setTimeStamp("1h");
        localEmpty.setProfilePic("adsprofile");
        localEmpty.setComments_number("something");
        localEmpty.setType("local");
        localEmpty.setRadius(radius);
        localEmpty.setDownvoted("something");
        localEmpty.setUpvoted("something");
        localEmpty.setUp_votes("something");
        localEmpty.setVotes("something");
        localEmpty.setUser_id("something");
        localEmpty.setId("something");
        listOfPostsLocalEmpty.add(localEmpty);

        return listOfPostsLocalEmpty;

    }

    private List<PostItem> emptyPersonal() {
        listOfPostsPersonalEmpty = new ArrayList<PostItem>();

        PostItem personalEmpty = new PostItem();
        personalEmpty.setUsername("Gatherspot Team");
        personalEmpty
                .setStatus("No personal posts in your area. Post a personal post and be the first to broadcast your content to everyone nearby!");
        personalEmpty.setImage("emptypersonal");
        personalEmpty.setTimeStamp("1h");
        personalEmpty.setProfilePic("adsprofile");
        personalEmpty.setComments_number("something");
        personalEmpty.setType("personal");
        personalEmpty.setDownvoted("something");
        personalEmpty.setDownvoted("something");
        personalEmpty.setUpvoted("something");
        personalEmpty.setUp_votes("something");
        personalEmpty.setVotes("something");
        personalEmpty.setUser_id("something");
        personalEmpty.setId("something");
        listOfPostsPersonalEmpty.add(personalEmpty);

        return listOfPostsPersonalEmpty;
    }

    private void requestDataAds(int pagingCounter, boolean page) {
        listOfPostsAds = new ArrayList<PostItem>();

        if (pagingCounter == 0) {
            PostItem localAd = new PostItem();
            localAd.setUsername("Gatherspot Team");
            localAd.setStatus("There are no local ads in your area at the moment. Stay tuned in with Gatherspot, " +
                    "local ads are coming soon and you'll never again miss a promotion, discount or some interesting event");
            localAd.setImage("adsimage1");
            localAd.setTimeStamp("1h");
            localAd.setProfilePic("adsprofile");
            localAd.setComments_number("something");
            localAd.setType("ad");
            localAd.setRadius(radius);
            localAd.setDownvoted("something");
            localAd.setUpvoted("something");
            localAd.setUp_votes("something");
            localAd.setVotes("something");
            localAd.setUser_id("something");
            localAd.setId("something");
            listOfPostsAds.add(localAd);
        }
        if (page) {
            setListPaging();
        } else {
            setList();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_activity_actions, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newLocalPost:
                Intent intentPost = new Intent(mActivity, ActivityNewPost.class);
                startActivity(intentPost);
                break;
            case R.id.setRadius:
                View menuItemRadius = mActivity.findViewById(R.id.setRadius);
                final PopupMenu popup = new PopupMenu(mActivity, menuItemRadius);
                if (longitudeDouble < -30) {
                    edtToken.putBoolean("imperialunits", true);
                    edtToken.commit();
                    popup.getMenuInflater().inflate(R.menu.radius_set, popup.getMenu());
                } else {
                    edtToken.putBoolean("imperialunits", false);
                    edtToken.commit();
                    popup.getMenuInflater().inflate(R.menu.radius_set_europe, popup.getMenu());
                }
                radiusValue = mActivity.getBaseContext()
                        .getSharedPreferences("RADIUSPREF",
                                Context.MODE_PRIVATE);
                if ((radiusValue.getString("radius", "300")).equals("50")) {
                    popup.getMenu().getItem(0).setChecked(true);
                } else if ((radiusValue.getString("radius", "300")).equals("300")) {
                    popup.getMenu().getItem(1).setChecked(true);
                } else if ((radiusValue.getString("radius", "300")).equals("1000")) {
                    popup.getMenu().getItem(2).setChecked(true);
                } else {
                    popup.getMenu().getItem(3).setChecked(true);
                }
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        radiusValue = mActivity.getBaseContext()
                                .getSharedPreferences("RADIUSPREF",
                                        Context.MODE_PRIVATE);
                        //lfCircle.setVisibility(View.VISIBLE);
                        swipeLayout.setRefreshing(true);
                        footerCircle.setVisibility(View.GONE);
                        //footerCircle.setVisibility(View.VISIBLE);
                        if (item.getItemId() == R.id.fifty) {
                            popup.getMenu().getItem(0).setChecked(true);
                            edtRadius.putString("radius", "50");
                            edtRadius.commit();

                            editRadius = true;
                            requestDataPersonal(0, false);

                        } else if (item.getItemId() == R.id.threeh) {
                            popup.getMenu().getItem(1).setChecked(true);
                            edtRadius.putString("radius", "300");
                            edtRadius.commit();

                            editRadius = true;
                            requestDataPersonal(0, false);

                        } else if (item.getItemId() == R.id.thousand) {
                            popup.getMenu().getItem(2).setChecked(true);
                            edtRadius.putString("radius", "1000");
                            edtRadius.commit();

                            editRadius = true;
                            requestDataPersonal(0, false);

                        } else {
                            popup.getMenu().getItem(3).setChecked(true);
                            edtRadius.putString("radius", "2000");
                            edtRadius.commit();

                            editRadius = true;
                            requestDataPersonal(0, false);
                        }
                        return true;
                    }
                });
                popup.show();

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b1) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (mActivity == null) {
            return;
        }
        if (ab == null) {
            return;
        }
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }


    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(mActivity);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (background.getBackground() != null) {
            background.getBackground().setCallback(null);
        }
        if (image != null) {
            image.setCallback(null);
            //image.getBitmap().recycle();
            image = null;
        }


        target = null;
        /*
        ppUsername = null;
        ppProfilePic = null;
        lpUsername = null;
        lpProfilePic = null;
        liveFeedList = null;
        listOfPostsPersonal = null;
        listOfPostsLocal = null;
        listOfPostsAds = null;

        listOfPostsLocalEmpty = null;
        listOfPostsPersonalEmpty = null;
        */
    }
}

