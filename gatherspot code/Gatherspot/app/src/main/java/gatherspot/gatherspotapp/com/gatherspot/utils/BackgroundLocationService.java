package gatherspot.gatherspotapp.com.gatherspot.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.facebook.AccessToken;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PanoramioImage;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Josip on 5.8.2015..
 */
public class BackgroundLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    ApiInterface api;

    String token;

    OkHttpClient client;

    // update svakih 20 minuta
    public static long UPDATE_LOCATION_INTERVAL = 1000 * 60 * 20;
    public static long FASTEST_UPDATE_INTERVAL = 1000 * 60 * 5;

    public static final String TAG = "Location Service";

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;

    String loginType;
    SharedPreferences myTokenPrefs, loginTypePrefs, gcmPrefs, backgroundImagePrefs;
    SharedPreferences.Editor edtToken, edtImage;

    @Override
    public void onCreate() {
        super.onCreate();

        client = trustcert(getApplicationContext());

        // inicijalizacija za server request
        api = InitHttpClient.getInstance().getApiInterface();

        // dobivanje tokena iz preferencesa
        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        gcmPrefs = getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");
        backgroundImagePrefs = getApplicationContext().getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
        edtImage = backgroundImagePrefs.edit();

        buildGoogleApiClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        } else {
            startLocationUpdates();
        }

//        BusProvider.getInstance().register(this);
        return START_STICKY;
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_LOCATION_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setSmallestDisplacement(20);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    protected void startLocationUpdates() {
        System.out.println("Start location updates");
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        System.out.println("Stop location updates ");
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void handleLocation(final Location location) {
        // slanje lokacije na server
        api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                    @Override
                    public void success(PanoramioImage response, Response response2) {
                        //edtImage.putString("url", response.getUrl());
                        //edtImage.commit();
                    }

                    @Override
                    public void failure(RetrofitError arg0) {
                        Response r = arg0.getResponse();
                        if (r != null && r.getStatus() == 401) {
                            if (loginType.equals("facebook")) {
                                api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                        AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                                                        String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                                                            @Override
                                                            public void success(PanoramioImage response, Response response2) {
                                                                //edtImage.putString("url", response.getUrl());
                                                                //edtImage.commit();
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {

                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });
                            } else {
                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.updateUserLocation(token, String.valueOf(location.getLongitude()),
                                                        String.valueOf(location.getLatitude()), new Callback<PanoramioImage>() {
                                                            @Override
                                                            public void success(PanoramioImage response, Response response2) {
                                                                //edtImage.putString("url", response.getUrl());
                                                                //edtImage.commit();
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {

                                                            }
                                                        });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });
                            }
                        }
                    }
                });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurrentLocation != null) {
            handleLocation(mCurrentLocation);
        }

        // provjera dali je namješteno da budu updejti
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleLocation(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public OkHttpClient trustcert(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient();
        try {
            KeyStore ksTrust = KeyStore.getInstance("BKS");
            InputStream instream = context.getResources().openRawResource(R.raw.mykeystore);
            ksTrust.load(instream, "secret".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ksTrust);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
            okHttpClient.setHostnameVerifier(new NullHostnameVerifier());
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | KeyManagementException e) {
            e.printStackTrace();
        }

        return okHttpClient;
    }

    public static class NullHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
