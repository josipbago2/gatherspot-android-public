package gatherspot.gatherspotapp.com.gatherspot.modules.chat;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterChat;
import gatherspot.gatherspotapp.com.gatherspot.pojo.ChatListItem;

/**
 * Created by Josip on 1.11.2015..
 */
public class FragmentChat extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ObservableScrollViewCallbacks {

    View rootView;
    ObservableListView list;
    Activity mActivity;

    ListAdapterChat listAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public FragmentChat(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.livefeed_layout, null);
        list = (ObservableListView) rootView.findViewById(R.id.list);

        listAdapter = new ListAdapterChat(mActivity, setDataForChatMock());

        setList();

        return rootView;
    }

    private void setList(){
        list.setAdapter(listAdapter);
    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b1) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    public void onRefresh() {

    }

    private ArrayList<ChatListItem> setDataForChatMock() {
        ArrayList<ChatListItem> dataSet = new ArrayList<ChatListItem>();
        ChatListItem data1 = new ChatListItem();
        data1.setUsername("Batman");
        data1.setLastMessage("lalalalal batman");
        data1.setTimeStamp("30min ago");
        dataSet.add(data1);

        ChatListItem data2 = new ChatListItem();
        data1.setUsername("Mami�");
        data1.setLastMessage("Nisam ti ja Joca Amsterdam");
        data1.setTimeStamp("33min ago");
        dataSet.add(data2);

        ChatListItem data3 = new ChatListItem();
        data3.setUsername("Neki user");
        data3.setLastMessage("Nesto nesto nesto tralalalalala");
        data3.setTimeStamp("2h ago");
        dataSet.add(data3);

        return dataSet;
    }
}
