package gatherspot.gatherspotapp.com.gatherspot.pushnotf;

/**
 * Created by Josip on 1.6.2015..
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
