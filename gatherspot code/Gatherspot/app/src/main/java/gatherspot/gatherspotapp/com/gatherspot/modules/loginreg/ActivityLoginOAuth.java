package gatherspot.gatherspotapp.com.gatherspot.modules.loginreg;

/**
 * Created by Josip on 28.5.2015..
 */

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nineoldandroids.animation.Animator;

import java.io.IOException;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.RegisterItemResponse;
import gatherspot.gatherspotapp.com.gatherspot.pushnotf.QuickstartPreferences;
import gatherspot.gatherspotapp.com.gatherspot.pushnotf.RegistrationIntentService;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityLoginOAuth extends AppCompatActivity {

    String mEmail;
    ImageButton signUp;
    boolean alreadyRegistered = false;

    private YoYo.YoYoString logoFade;

    LoginButton loginButton;

    CallbackManager callbackManager;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    SharedPreferences myTokenPrefs, loginPrefs, loginTypePrefs;
    SharedPreferences.Editor edt, edtLoginPrefs, edtType;

    RelativeLayout relativeLayout;

    String gcmToken = "";

    private long Interval = 400;
    ImageView logo1, logo2, logo3, logo4, title1, background;

    private static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";

    ListView list;
    ArrayAdapter<String> adapter;

    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    SharedPreferences gcmPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.auth_startactivity);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        myTokenPrefs = getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edt = myTokenPrefs.edit();
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        edtType = loginTypePrefs.edit();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    gcmPrefs = getApplicationContext().getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
                    gcmToken = gcmPrefs.getString("gcmToken", "");
                } else {

                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        callbackManager = CallbackManager.Factory.create();

        title1 = (ImageView) findViewById(R.id.title);
        logo1 = (ImageView) findViewById(R.id.logopart1);
        logo2 = (ImageView) findViewById(R.id.logopart2);
        logo3 = (ImageView) findViewById(R.id.logopart3);
        logo4 = (ImageView) findViewById(R.id.logopart4);
        relativeLayout = (RelativeLayout) findViewById(R.id.buttonsLogin);
        background = (ImageView) findViewById(R.id.background);

        runAnimation();

        signUp = (ImageButton) findViewById(R.id.signUpEmail);
        loginButton = (LoginButton) findViewById(R.id.login_buttonFb);
        //loginButton.setBackgroundResource(R.drawable.login_fb_button);

        //loginButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        loginButton.setTextSize(11);
        float fbIconScale = 1.45F;
        Drawable drawable = ContextCompat.getDrawable(this,
                com.facebook.R.drawable.com_facebook_button_icon);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * fbIconScale),
                (int) (drawable.getIntrinsicHeight() * fbIconScale));
        loginButton.setCompoundDrawables(drawable, null, null, null);
        loginButton.setCompoundDrawablePadding(this.getResources().
                getDimensionPixelSize(R.dimen.fb_margin_override_textpadding));
        loginButton.setPadding(
                this.getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_lr),
                this.getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_top),
                this.getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_rightmargin),
                this.getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_bottom));

        loginButton.setReadPermissions("public_profile", "user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                relativeLayout.setVisibility(View.INVISIBLE);
                registerAndLogin(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //pickUserAccount();
                Intent intent = new Intent(ActivityLoginOAuth.this, ActivityLogin.class);
                //recycleBitmaps();
                startActivity(intent);

            }
        });


    }

    private void recycleBitmaps() {
        Drawable drawable1 = background.getDrawable();
        if (drawable1 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable1;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable2 = logo1.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable2;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable3 = logo2.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable3;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable4 = logo3.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable4;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable5 = logo4.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable5;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
    }

    private void registerForGcm() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
            }
        };
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    public void registerAndLogin(final String FBtoken) {
        ApiInterface api = InitHttpClient.getInstance().getApiInterface();
        api.RegisterFB(gcmToken, FBtoken, "facebook", new Callback<RegisterItemResponse>() {

            @Override
            public void failure(RetrofitError arg0) {
                arg0.printStackTrace();
                relativeLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void success(RegisterItemResponse arg0, Response arg1) {
                if (arg0.getResponse().equals("Already Registered")) {
                    alreadyRegistered = true;
                }
                loginWithFB(FBtoken);
            }
        });
    }

    public void loginWithFB(String FBtoken) {

        ApiInterface api = InitHttpClient.getInstance().getApiInterface();
        api.LoginFB(gcmToken, FBtoken, "facebook", new Callback<LoginItemToken>() {

            @Override
            public void failure(RetrofitError arg0) {
                // TODO Auto-generated method stub
                arg0.printStackTrace();
                relativeLayout.setVisibility(View.VISIBLE);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 403) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ActivityLoginOAuth.this);
                    alertBuilder.setTitle("Gatherspot");
                    alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                            "and/or comments, you have been banned from Gatherspot");
                    alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertBuilder.create();
                    alertDialog.show();
                }

            }

            @Override
            public void success(LoginItemToken arg0, Response arg1) {
                edtType.putString("type", "facebook");
                edtType.commit();
                edt.putString("mytoken", arg0.getToken());
                edt.putString("myuserid", arg0.getUser_id());
                edt.commit();
                if (alreadyRegistered) {
                    Intent intent = new Intent(ActivityLoginOAuth.this,
                            ActivityTutorial.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //recycleBitmaps();
                    startActivity(intent);
                } else {
                    loginPrefs = getSharedPreferences("login", Context.MODE_PRIVATE);
                    edtLoginPrefs = loginPrefs.edit();
                    edtLoginPrefs.putBoolean("fb", true);
                    edtLoginPrefs.commit();
                    Intent intent = new Intent(ActivityLoginOAuth.this,
                            ActivityRegisterUsername.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //recycleBitmaps();
                    startActivity(intent);
                }


            }

        });
    }


    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                getUsername();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Pick account", Toast.LENGTH_SHORT).show();

            }
        } else if ((requestCode == REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR)
                && resultCode == RESULT_OK) {
            getUsername();
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void getUsername() {
        if (mEmail == null) {
            pickUserAccount();
        } else {
            if (isDeviceOnline()) {
                new getUsernameTask(ActivityLoginOAuth.this, mEmail, SCOPE).execute();
            } else {
                Toast.makeText(this, "Not connected to the internet",
                        Toast.LENGTH_SHORT);
            }
        }
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public class getUsernameTask extends AsyncTask<Void, Void, Void> {

        Activity mActivity;
        String mScope;
        String mEmail;

        public getUsernameTask(Activity activity, String name, String scope) {
            this.mActivity = activity;
            this.mScope = scope;
            this.mEmail = name;
        }

        protected Void doInBackground(Void... params) {
            try {
                String token = fetchToken();
                if (token != null) {
                    edt.putString("mytoken", token);
                    // edt.putString("myuserid", "");
                    edt.commit();
                    Toast.makeText(ActivityLoginOAuth.this, "Registered",
                            Toast.LENGTH_SHORT);
                } else {

                }
            } catch (IOException e) {
                Toast.makeText(ActivityLoginOAuth.this, "Something went wrong",
                        Toast.LENGTH_SHORT);
            }
            return null;
        }

        protected String fetchToken() throws IOException {
            try {
                return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
            } catch (UserRecoverableAuthException userRecoverableAuthException) {
                handleException(userRecoverableAuthException);
            } catch (GoogleAuthException fatalException) {
                Toast.makeText(ActivityLoginOAuth.this,
                        "Something went wrong with authentication",
                        Toast.LENGTH_SHORT);
            }
            return null;
        }

    }

    public void handleException(final Exception e) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (e instanceof GooglePlayServicesAvailabilityException) {
                    int statusCode = ((GooglePlayServicesAvailabilityException) e)
                            .getConnectionStatusCode();
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                            statusCode, ActivityLoginOAuth.this,
                            REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    dialog.show();
                } else if (e instanceof UserRecoverableAuthException) {
                    Intent intent = ((UserRecoverableAuthException) e)
                            .getIntent();
                    startActivityForResult(intent,
                            REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                }
            }

        });
    }


    private void runAnimation() {
        logoFade = YoYo.with(Techniques.FadeIn)
                .duration(Interval)
                .delay(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        logo1.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeIn)
                                .duration(150)
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        title1.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(title1);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.FadeIn)
                                .duration(Interval)
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        logo2.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.FadeIn)
                                                .duration(Interval)
                                                .interpolate(new AccelerateDecelerateInterpolator())
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        logo3.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeIn)
                                                                .duration(Interval)
                                                                .interpolate(new AccelerateDecelerateInterpolator())
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        logo4.setVisibility(View.VISIBLE);

                                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(logo4);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                }).playOn(logo3);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                }).playOn(logo2);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(logo1);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        isDeviceOnline();
        super.onResume();
        //getUserInfo();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("Tag", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}
