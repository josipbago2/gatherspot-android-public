package gatherspot.gatherspotapp.com.gatherspot.utils;

/**
 * Created by Josip on 29.5.2015..
 */

import java.util.ArrayList;

import gatherspot.gatherspotapp.com.gatherspot.pojo.Feed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LocalPost;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.MyLocalPost;
import gatherspot.gatherspotapp.com.gatherspot.pojo.NewComment;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PanoramioImage;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPost;
import gatherspot.gatherspotapp.com.gatherspot.pojo.RegisterItemResponse;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UpDownVoteResponse;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UserInfoItem;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;


public interface ApiInterface {

    // @Headers("feed")
    @GET("/feed/feed.json")
    public void getFeed(Callback<Feed> callback);

    @Multipart
    @POST("/register")
    public void RegisterFB(@Part("gcmToken") String gcmToken, @Part("accessToken") String accessToken, @Part("grant_type") String grant_type, Callback<RegisterItemResponse> cb);

    @Multipart
    @POST("/register")
    public void Register(@Part("email") String username,
                         @Part("password") String password, @Part("grant_type") String grant_type, Callback<RegisterItemResponse> cb);

    @Multipart
    @POST("/login")
    public void LoginFB(@Part("gcmToken") String gcmToken, @Part("accessToken") String accessToken, @Part("grant_type") String grant_type, Callback<LoginItemToken> cb);

    @Multipart
    @POST("/login")
    public void Login(@Part("gcmToken") String gcm, @Part("email") String username,
                      @Part("password") String password, @Part("grant_type") String grant_type, Callback<LoginItemToken> cb);

    @Multipart
    @POST("/user/data")
    public void changeUserData(@Part("gcmToken") String gcmToken, @Header("Authorization") String token,
                               @Part("username") String username, @Part("about") String about,
                               Callback<Response> cb);

    @Multipart
    @POST("/user/beat")
    public void updateUserLocation(@Header("Authorization") String token,
                                   @Part("x") String x, @Part("y") String y, Callback<PanoramioImage> cb);

    @Multipart
    @POST("/post_local/delete")
    public void deleteMyLocalPost(@Header("Authorization") String token,
                                  @Part("id") String id, Callback<String> cb);

    @GET("/post_local/my")
    public void getMyLocalPosts(@Header("Authorization") String token,
                                 @Query("pageSize") String pageSize, @Query("pageNumber") String pageNumber, Callback<ArrayList<MyLocalPost>> cb);

    @GET("/post_personal/my")
    public void getMyPersonalPosts(@Header("Authorization") String token,
                                   Callback<Feed> cb);

    @Multipart
    @POST("/post_local/up_vote")
    public void upVoteLocal(@Header("Authorization") String token, @Part("id") String id,
                            Callback<UpDownVoteResponse> cb);

    @Multipart
    @POST("/post_personal/get")
    public void getPersonalPosts(@Header("Authorization") String token,
                                 @Part("x") String x, @Part("y") String y,
                                 @Part("radius") String radius, @Part("pageSize") String pageSize, @Part("pageNumber") String pageNumber, Callback<Feed> cb);

    @Multipart
    @POST("/post_local/down_vote")
    public void downVoteLocal(@Header("Authorization") String token,
                              @Part("id") String id, Callback<UpDownVoteResponse> cb);

    @Multipart
    @POST("/post_personal/up_vote")
    public void upVotePersonal(@Header("Authorization") String token,
                               @Part("id") String id, Callback<UpDownVoteResponse> cb);

    @Multipart
    @POST("/post_personal/down_vote")
    public void downVotePersonal(@Header("Authorization") String token,
                                 @Part("id") String id, Callback<UpDownVoteResponse> cb);

    @GET("/post_local")
    public void getLocalPosts(@Header("Authorization") String token,
                              @Query("x") String x, @Query("y") String y,
                              @Query("radius") String radius, @Query("pageSize") String pageSize, @Query("pageNumber") String pageNumber, Callback<Feed> callback);

    @Multipart
    @POST("/post_personal/new")
    public void newPersonalPost(@Header("Authorization") String token,
                                @Part("image") TypedFile file, @Part("status") String status,
                                Callback<Response> cb);

    @Multipart
    @POST("/post_personal/new")
    public void newPersonalPostNoImage(@Header("Authorization") String token,
                                       @Part("status") String status, Callback<Response> cb);

    @Multipart
    @POST("/post_personal/delete")
    public void deletePersonalPost(@Header("Authorization") String token,
                                   @Part("post_id") String post_id, Callback<Response> cb);

    @Multipart
    @POST("/post_personal/comment/delete")
    public void deletePersonalComment(@Header("Authorization") String token,
                                      @Part("id") String id, Callback<Response> cb);

    @Multipart
    @POST("/post_local/comments/delete")
    public void deleteLocalComment(@Header("Authorization") String token,
                                   @Part("post_id") String post_id, @Part("id") String id, Callback<Response> cb);

    @Multipart
    @POST("/post_local/new")
    public void newLocalPost(@Header("Authorization") String token,
                             @Part("status") String status, @Part("x") String x,
                             @Part("y") String y, @Part("image") TypedFile file,
                             @Part("name") String name, Callback<Response> cb);

    @Multipart
    @POST("/post_local/new")
    public void newLocalPostNoImage(@Header("Authorization") String token,
                                    @Part("status") String status, @Part("x") String x,
                                    @Part("y") String y, @Part("name") String name,
                                    Callback<Response> cb);

    @Multipart
    @POST("/post_local/report/comment")
    public void reportComment(@Header("Authorization") String token,
                              @Part("id") String id, @Part("reason") String reason, Callback<Response> cb);

    @Multipart
    @POST("/user/profilepic")
    public void setProfilePicture(@Header("Authorization") String token,
                                  @Part("image") TypedFile file, Callback<Response> cb);

    @Multipart
    @POST("/user/profilepic/get")
    public void getUserInfo(@Header("Authorization") String token,
                            @Part("user_id") String user_id, Callback<UserInfoItem> cb);

    @Multipart
    @POST("/post_personal/user")
    public void getUsersPersonal(@Header("Authorization") String token,
                                 @Part("user_id") String user_id, Callback<Feed> cb);

    @Multipart
    @POST("/post_local/comments/get")
    public void getLocalComments(@Header("Authorization") String token,
                                 @Part("post_id") String post_id,
                                 @Part("pageSize") String pageSize, @Part("pageNumber") String pageNumber, Callback<Feed> cb);

    @Multipart
    @POST("/post_personal/comments")
    public void newPersonalComment(@Header("Authorization") String token,
                                   @Part("post_id") String post_id, @Part("status") String status,
                                   Callback<NewComment> cb);

    @Multipart
    @POST("/post_personal/comments/get")
    public void getPersonalComments(@Header("Authorization") String token,
                                    @Part("post_id") String post_id,
                                    @Part("pageSize") String pageSize, @Part("pageNumber") String pageNumber, Callback<Feed> cb);

    @Multipart
    @POST("/post_local/comments")
    public void newLocalComment(@Header("Authorization") String token,
                                @Part("post_id") String post_id, @Part("status") String status,
                                Callback<NewComment> cb);

    @Multipart
    @POST("/post_personal/id")
    public void getPostPersonal(@Header("Authorization") String token, @Part("id") String postId, Callback<PersonalPost> cb);

    @Multipart
    @POST("/post_local/id")
    public void getPostLocal(@Header("Authorization") String token, @Part("id") String postId, Callback<LocalPost> cb);


    @Multipart
    @POST("/user/seen")
    public void seenComments(@Header("Authorization") String token, @Part("id") String idPost, Callback<Response> cb);

    @Multipart
    @POST("/user/feedback")
    public void reportProblem(@Header("Authorization") String token,
                              @Part("text") String text,
                              Callback<Response> cb);

    @Multipart
    @POST("/post_local/report")
    public void reportLocalPost(@Header("Authorization") String token,
                                @Part("post_id") String post_id, @Part("reason") String reason,
                                Callback<Response> cb);

    @Multipart
    @POST("/post_personal/report")
    public void reportPersonalPost(@Header("Authorization") String token,
                                   @Part("post_id") String post_id, @Part("reason") String reason,
                                   Callback<Response> cb);
}
