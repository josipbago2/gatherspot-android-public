package gatherspot.gatherspotapp.com.gatherspot.listadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.ChatListItem;

/**
 * Created by Josip on 1.11.2015..
 */
public class ListAdapterChat extends BaseAdapter {

    List<ChatListItem> listData;
    Context context;

    private LayoutInflater layoutInflater;

    public ListAdapterChat(Context context, List<ChatListItem> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public ChatListItem getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = layoutInflater.inflate(R.layout.feed_item_chatlist, null);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.username.setText(listData.get(position).getUsername());
        holder.lastMessage.setText(listData.get(position).getLastMessage());
        holder.timeStamp.setText(listData.get(position).getTimeStamp());

        return convertView;
    }

    static class ViewHolder {
        TextView username, lastMessage, timeStamp;
    }
}
