package gatherspot.gatherspotapp.com.gatherspot.pushnotf;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import gatherspot.gatherspotapp.com.gatherspot.modules.shared.ActivityComments;
import gatherspot.gatherspotapp.com.gatherspot.R;

/**
 * Created by Josip on 1.6.2015..
 */
public class MyGcmListenerService extends GcmListenerService {
    private static final String TAG = "MyGcmListenerService";

    private static final int PERSONAL = 1;
    private static final int LOCAL = 3;

    SharedPreferences pushNotfOnOff;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        pushNotfOnOff = getSharedPreferences(
                "pushnotfonoff", Context.MODE_PRIVATE);
        String message = data.getString("message");
        String type = data.getString("type"); //local i personal
        String postId = data.getString("id");

        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        if (pushNotfOnOff.getBoolean("onoff", true)) {
            sendNotification(message, postId, type);
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message, String postId, String type) {
        int typeToSend;
        if (type.equals("personal")) {
            typeToSend = PERSONAL;
        } else {
            typeToSend = LOCAL;
        }
        Intent intent = new Intent(this, ActivityComments.class);
        intent.putExtra("notfPostId", postId);
        intent.putExtra("notification", true);
        intent.putExtra("postType", typeToSend);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_stat_logo_final)
                .setContentTitle("Gatherspot")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
