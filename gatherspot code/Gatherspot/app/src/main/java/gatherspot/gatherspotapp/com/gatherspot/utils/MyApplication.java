package gatherspot.gatherspotapp.com.gatherspot.utils;

import android.app.Application;

import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;

/**
 * Created by Josip on 18.8.2015..
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initSingletons();
    }

    protected void initSingletons() {
        InitHttpClient.initInstance(getApplicationContext());
    }
}
