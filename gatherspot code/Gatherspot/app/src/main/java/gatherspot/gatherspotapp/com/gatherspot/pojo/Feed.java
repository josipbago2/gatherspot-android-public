package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 29.5.2015..
 */

import java.util.List;
import java.util.Map;

public class Feed {

    List<LocalPostItem> feed;
    List<PersonalPostItem> posts;
    List<CommentItemResponse> comments;
    Map<String, Map<String, String>> users;

    public List<LocalPostItem> getFeed() {
        return feed;
    }

    public List<CommentItemResponse> getComments() {
        return comments;
    }

    public List<PersonalPostItem> getPersonalPosts() {
        return posts;
    }

    public Map<String, Map<String, String>> getUsers() {
        return users;
    }

    public void setFeed(List<LocalPostItem> feed) {
        this.feed = feed;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((feed == null) ? 0 : feed.hashCode());
        result = prime * result + ((posts == null) ? 0 : posts.hashCode());
        result = prime * result + ((users == null) ? 0 : users.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Feed other = (Feed) obj;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (feed == null) {
            if (other.feed != null)
                return false;
        } else if (!feed.equals(other.feed))
            return false;
        if (posts == null) {
            if (other.posts != null)
                return false;
        } else if (!posts.equals(other.posts))
            return false;
        if (users == null) {
            if (other.users != null)
                return false;
        } else if (!users.equals(other.users))
            return false;
        return true;
    }

}
