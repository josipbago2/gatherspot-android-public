package gatherspot.gatherspotapp.com.gatherspot.modules.settings;

/**
 * Created by Josip on 29.5.2015..
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import java.util.UUID;

import gatherspot.gatherspotapp.com.gatherspot.MainActivity;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.StartActivity;

public class FragmentSettings extends Fragment {

    private Activity mActivity;
    SharedPreferences pushNotfPref, myPersonalExistPrefs, lUpdatesPrefs, myTokenPrefs, firstRun, loginPrefs;
    SharedPreferences.Editor edt, edtlUpdatesPrefs, edtMyTokenPrefs, edtFirstRun, edtLoginPrefs;
    private Switch btnOnOffNotf, btnOnOffLU;
    TextView title1, title2, title3, title4, title5, title6, title7, title8, title10;

    android.support.v7.app.ActionBar ab;

    ListView list;
    Adapter listAdapter;

    View header1, header2, header3, header4, header5, header6, header7, header8, header9, header10;

    private boolean pushOnOff, currentlyTracking;

    public FragmentSettings() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        loginPrefs = mActivity.getSharedPreferences("login", Context.MODE_PRIVATE);
        edtLoginPrefs = loginPrefs.edit();
        firstRun = mActivity.getSharedPreferences("FIRST",
                Context.MODE_PRIVATE);
        edtFirstRun = firstRun.edit();
        myTokenPrefs = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtMyTokenPrefs = myTokenPrefs.edit();
        lUpdatesPrefs = mActivity.getSharedPreferences("lupdates",
                Context.MODE_PRIVATE);
        edtlUpdatesPrefs = lUpdatesPrefs.edit();
        currentlyTracking = lUpdatesPrefs.getBoolean("lonoff", true);
        myPersonalExistPrefs = mActivity.getSharedPreferences(
                "mypersonalexist", Context.MODE_PRIVATE);
        pushNotfPref = mActivity.getApplicationContext().getSharedPreferences(
                "pushnotfonoff", Context.MODE_PRIVATE);
        pushOnOff = pushNotfPref.getBoolean("onoff", true);
        edt = pushNotfPref.edit();

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mActivity = activity;
        ab = ((AppCompatActivity) activity).getSupportActionBar();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.settings_layout, container,
                false);

        /*
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(
                getResources().getDimensionPixelSize(R.dimen.settings_item_leftrighttop_margin), 50,
                getResources().getDimensionPixelSize(R.dimen.settings_item_leftrighttop_margin),
                getResources().getDimensionPixelSize(R.dimen.settings_item_leftrighttop_margin));
                */

        // push
        header1 = inflater.inflate(R.layout.settings_h1, null);
        title1 = (TextView) header1.findViewById(R.id.textView1);
        title1.setText("Turn on/off push notifications");

        btnOnOffNotf = (Switch) header1.findViewById(R.id.btnOnOff);
        if (!pushOnOff) {
            btnOnOffNotf.setChecked(false);
        } else {
            btnOnOffNotf.setChecked(true);
        }

        btnOnOffNotf.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (pushOnOff) {
                    pushOnOff = false;
                    btnOnOffNotf.setChecked(false);
                    edt.putBoolean("onoff", false);
                    edt.commit();
                } else {
                    pushOnOff = true;
                    btnOnOffNotf.setChecked(true);
                    edt.putBoolean("onoff", true);
                    edt.commit();
                }

            }
        });

        // location updates
        header2 = inflater.inflate(R.layout.settings_h1, null);
        title2 = (TextView) header2.findViewById(R.id.textView1);
        title2.setText("Background location updates");

        btnOnOffLU = (Switch) header2.findViewById(R.id.btnOnOff);
        if (!currentlyTracking) {
            btnOnOffLU.setChecked(false);
        } else {
            btnOnOffLU.setChecked(true);
        }

        btnOnOffLU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentlyTracking) {
                    // zaustavi updates
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mActivity);
                    alertBuilder.setTitle("Location updates");
                    alertBuilder.setMessage("By turning off your location updates, you will not " +
                            "broadcast your personal posts while you are not using Gatherspot. Are you sure?");
                    alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) mActivity).stopBackgroundService();
                            edtlUpdatesPrefs.putBoolean("lonoff", false);
                            edtlUpdatesPrefs.putString("sessionID", "");
                            edtlUpdatesPrefs.commit();
                            btnOnOffLU.setChecked(false);
                            currentlyTracking = lUpdatesPrefs.getBoolean("lonoff", true);
                        }
                    });
                    alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            btnOnOffLU.setChecked(true);
                        }
                    });
                    AlertDialog alertDialog = alertBuilder.create();
                    alertDialog.show();

                } else {
                    // zapo�ni updates
                    ((MainActivity) mActivity).startBackgroundService();
                    /*
                    if (myPersonalExistPrefs.getBoolean("exist", false)) {
                        ((MainActivity) mActivity).startAlarmManager();
                    }
                    */
                    edtlUpdatesPrefs.putString("sessionID", UUID.randomUUID().toString());
                    edtlUpdatesPrefs.putBoolean("lonoff", true);
                    edtlUpdatesPrefs.commit();
                    btnOnOffLU.setChecked(true);
                    currentlyTracking = lUpdatesPrefs.getBoolean("lonoff", true);
                }
            }
        });

        // .................

        // Give us feedback
        header3 = inflater.inflate(R.layout.settings_h2, null);
        title3 = (TextView) header3.findViewById(R.id.titleH);
        title3.setText("Give us feedback");
        //layout2 = (RelativeLayout) header3.findViewById(R.id.container);

        header3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityContactUs.class);
                intent.putExtra("id", "feedback");
                mActivity.startActivity(intent);
            }
        });


        // Report a problem
        header4 = inflater.inflate(R.layout.settings_h2, null);
        title4 = (TextView) header4.findViewById(R.id.titleH);
        title4.setText("Report a problem");

        header4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityContactUs.class);
                intent.putExtra("id", "reportaproblem");
                mActivity.startActivity(intent);
            }
        });

        // .................

        // Terms of use
        header5 = inflater.inflate(R.layout.settings_h2, null);
        title5 = (TextView) header5.findViewById(R.id.titleH);
        title5.setText("Terms of use");
        //layout6 = (RelativeLayout) header5.findViewById(R.id.container);

        header5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "Terms of Use");
                mActivity.startActivity(intent);
            }
        });


        // Privacy Policy
        header6 = inflater.inflate(R.layout.settings_h2, null);
        title6 = (TextView) header6.findViewById(R.id.titleH);
        title6.setText("Privacy Policy");

        header6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "Privacy Policy");
                mActivity.startActivity(intent);
            }
        });

        // Like us on Facebook
        header7 = inflater.inflate(R.layout.settings_h3, null);
        title7 = (TextView) header7.findViewById(R.id.titleH);
        title7.setText("Like us on Facebook");

        header7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = newFacebookIntent(
                        mActivity.getPackageManager(), "https://www.facebook.com/pages/Gatherspot/492206880920846");
                startActivity(intent);
            }
        });

        // ..................

        // Log out
        header8 = inflater.inflate(R.layout.settings_h2, null);
        title8 = (TextView) header8.findViewById(R.id.titleH);
        title8.setText("Log out");
        //layout3 = (RelativeLayout) header8.findViewById(R.id.container);

        header8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FacebookSdk.isInitialized()) {
                    FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
                }
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mActivity);
                alertBuilder.setTitle("Log Out");
                alertBuilder.setMessage("Log out now?");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((MainActivity) mActivity).stopBackgroundService();
                        edtlUpdatesPrefs.putBoolean("lonoff", false);
                        edtlUpdatesPrefs.putString("sessionID", "");
                        edtlUpdatesPrefs.commit();


                        edtFirstRun.putBoolean("LIVEFEED", false);
                        edtFirstRun.putBoolean("tutorialpersonal", true);
                        edtFirstRun.putBoolean("tutorialnewpost", true);
                        edtFirstRun.putBoolean("MYPROFILE", false);
                        edtFirstRun.commit();

                        edtMyTokenPrefs.putString("myusername", "");
                        edtMyTokenPrefs.putString("mytoken", "");
                        edtMyTokenPrefs.putString("myuserid", "");
                        edtMyTokenPrefs.commit();

                        LoginManager.getInstance().logOut();

                        Intent intent = new Intent(mActivity, StartActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();

            }
        });

        header9 = inflater.inflate(R.layout.settings_h4, null);

        // Rate us
        header10 = inflater.inflate(R.layout.settings_h2, null);
        title10 = (TextView) header10.findViewById(R.id.titleH);
        title10.setText("Rate us");

        header10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + mActivity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + mActivity.getPackageName())));
                }
            }
        });

        listAdapter = new Adapter();
        list = (ListView) rootView.findViewById(R.id.settingsList);
        list.addHeaderView(header1);
        list.addHeaderView(header2);
        list.addHeaderView(header9);
        list.addHeaderView(header3);
        list.addHeaderView(header4);
        list.addHeaderView(header10);
        list.addHeaderView(header5);
        list.addHeaderView(header6);
        list.addHeaderView(header7);
        list.addHeaderView(header9);
        list.addHeaderView(header8);

        list.setAdapter(listAdapter);

        header9.setVisibility(View.INVISIBLE);

        /*
        layout2.setLayoutParams(params);
        layout6.setLayoutParams(params);
        layout3.setLayoutParams(params);
        */

        return rootView;
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri;
        try {
            pm.getPackageInfo("com.facebook.katana", 0);
            // http://stackoverflow.com/a/24547437/1048340
            uri = Uri.parse("fb://facewebmodal/f?href=" + url);
        } catch (PackageManager.NameNotFoundException e) {
            uri = Uri.parse(url);
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    class Adapter extends BaseAdapter {

        public Adapter() {
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }

}
