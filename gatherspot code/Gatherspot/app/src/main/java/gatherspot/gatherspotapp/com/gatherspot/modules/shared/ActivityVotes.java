package gatherspot.gatherspotapp.com.gatherspot.modules.shared;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UserItem;

/**
 * Created by Josip on 30.9.2015..
 */
public class ActivityVotes extends AppCompatActivity {

    ListView list;
    ProgressBar pb;

    private VotesAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.livefeed_layout);

        list = (ListView) findViewById(R.id.list);
        pb = (ProgressBar) findViewById(R.id.progressBar);
    }

    private class VotesAdapter extends BaseAdapter {

        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPERATOR = 1;
        private static final int TYPE_MAX_COUNT = TYPE_SEPERATOR + 1;

        private List<UserItem> listData;
        private Context context;

        public VotesAdapter(Context context, List<UserItem> listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            return TYPE_MAX_COUNT;
        }


        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }


}
