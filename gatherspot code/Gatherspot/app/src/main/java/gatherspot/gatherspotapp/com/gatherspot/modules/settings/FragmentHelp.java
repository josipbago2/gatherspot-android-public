package gatherspot.gatherspotapp.com.gatherspot.modules.settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import gatherspot.gatherspotapp.com.gatherspot.R;

/**
 * Created by Josip on 23.9.2015..
 */

public class FragmentHelp extends Fragment {

    ListView list;
    Adapter listAdapter;
    Activity mActivity;
    android.support.v7.app.ActionBar ab;

    View header1, header2, item11, item12, item13,
            item21, item22, item23, item24, item25, item26;

    public FragmentHelp() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_layout, container,
                false);

        header1 = inflater.inflate(R.layout.settings_h2, null);
        TextView title1 = (TextView) header1.findViewById(R.id.titleH);
        title1.setText("Talk To Us");
        title1.setTextSize(21.f);
        title1.setTextColor(Color.parseColor("#797979"));
        RelativeLayout r1 = (RelativeLayout) header1.findViewById(R.id.container);
        r1.setBackgroundResource(android.R.color.transparent);

        item11 = inflater.inflate(R.layout.settings_h2, null);
        TextView title2 = (TextView) item11.findViewById(R.id.titleH);
        title2.setText("Ask a question");

        item11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityContactUs.class);
                intent.putExtra("id", "askaquestion");
                mActivity.startActivity(intent);
            }
        });

        item12 = inflater.inflate(R.layout.settings_h2, null);
        TextView title3 = (TextView) item12.findViewById(R.id.titleH);
        title3.setText("Report a problem");

        item12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityContactUs.class);
                intent.putExtra("id", "reportaproblem");
                mActivity.startActivity(intent);
            }
        });

        item13 = inflater.inflate(R.layout.settings_h2, null);
        TextView title4 = (TextView) item13.findViewById(R.id.titleH);
        title4.setText("Report bad behavior");

        item13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityContactUs.class);
                intent.putExtra("id", "reportbb");
                mActivity.startActivity(intent);
            }
        });

        header2 = inflater.inflate(R.layout.settings_h2, null);
        TextView title5 = (TextView) header2.findViewById(R.id.titleH);
        title5.setText("FAQ");
        title5.setTextSize(21.f);
        title5.setTextColor(Color.parseColor("#797979"));
        RelativeLayout r2 = (RelativeLayout) header2.findViewById(R.id.container);
        r2.setBackgroundResource(android.R.color.transparent);

        item21 = inflater.inflate(R.layout.settings_h2, null);
        TextView title6 = (TextView) item21.findViewById(R.id.titleH);
        title6.setText("What is a personal post?");

        item21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq1");
                mActivity.startActivity(intent);
            }
        });

        item22 = inflater.inflate(R.layout.settings_h2, null);
        TextView title7 = (TextView) item22.findViewById(R.id.titleH);
        title7.setText("What is a local post?");

        item22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq2");
                mActivity.startActivity(intent);
            }
        });

        item23 = inflater.inflate(R.layout.settings_h2, null);
        TextView title8 = (TextView) item23.findViewById(R.id.titleH);
        title8.setText("What's the difference between personal and local post?");

        item23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq3");
                mActivity.startActivity(intent);
            }
        });

        item24 = inflater.inflate(R.layout.settings_h2, null);
        TextView title9 = (TextView) item24.findViewById(R.id.titleH);
        title9.setText("What is a local ad?");

        item24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq4");
                mActivity.startActivity(intent);
            }
        });

        item25 = inflater.inflate(R.layout.settings_h2, null);
        TextView title10 = (TextView) item25.findViewById(R.id.titleH);
        title10.setText("What if i don't want to be seen on the network(become invisible)?");

        item25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq5");
                mActivity.startActivity(intent);
            }
        });

        item26 = inflater.inflate(R.layout.settings_h2, null);
        TextView title11 = (TextView) item26.findViewById(R.id.titleH);
        title11.setText("What if i dont like the content shared on the app?");

        item26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityTermsOfUse.class);
                intent.putExtra("id", "faq6");
                mActivity.startActivity(intent);
            }
        });

        listAdapter = new Adapter();
        list = (ListView) rootView.findViewById(R.id.settingsList);
        list.addHeaderView(header1);
        list.addHeaderView(item11);
        list.addHeaderView(item12);
        list.addHeaderView(item13);
        list.addHeaderView(header2);
        list.addHeaderView(item21);
        list.addHeaderView(item22);
        list.addHeaderView(item23);
        list.addHeaderView(item24);
        //list.addHeaderView(item25);
        list.addHeaderView(item26);

        list.setAdapter(listAdapter);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        ab = ((AppCompatActivity) activity).getSupportActionBar();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    class Adapter extends BaseAdapter {

        public Adapter() {
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }
}
