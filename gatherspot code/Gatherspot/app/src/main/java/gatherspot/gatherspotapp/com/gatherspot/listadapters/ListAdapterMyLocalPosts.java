package gatherspot.gatherspotapp.com.gatherspot.listadapters;

/**
 * Created by Josip on 29.5.2015..
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.modules.shared.ActivityComments;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentData;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.MyLocalPost;
import gatherspot.gatherspotapp.com.gatherspot.utils.FullScreenImageActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

public class ListAdapterMyLocalPosts extends BaseAdapter {

    ApiInterface api;

    Picasso picasso;

    private ArrayList<MyLocalPost> listData;
    private String token, myUserId;
    String loginType;
    SharedPreferences resume, resumeComments, myTokenPrefs, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edt, edtToken;
    private LayoutInflater layoutInflater;
    private Context context;

    public ListAdapterMyLocalPosts(Context context, ArrayList<MyLocalPost> listData,
                                   String token) {
        this.token = token;
        this.listData = listData;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        api = InitHttpClient.getInstance().getApiInterface();
        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");
        myTokenPrefs = context.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        myUserId = myTokenPrefs.getString("myuserid", "");

        picasso = InitHttpClient.getInstance().getImageLoader();
    }

    public void addMore(List<MyLocalPost> addList) {
        listData.addAll(addList);
        this.notifyDataSetChanged();
    }

    public void refill(List<MyLocalPost> updateList) {
        listData.clear();
        Collections.reverse(updateList);
        listData.addAll(updateList);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final int postType = 4;

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.feed_item_mlp, null);
            holder.voteValue = (TextView) convertView.findViewById(R.id.voteValue);
            holder.delete = (ImageButton) convertView.findViewById(R.id.btnDelete);
            holder.place = (TextView) convertView.findViewById(R.id.postPlace);
            holder.feedImage = (ImageView) convertView
                    .findViewById(R.id.feedImage1);
            holder.progressBar = (ProgressBar) convertView
                    .findViewById(R.id.progressBar1);
            holder.post = (RelativeLayout) convertView.findViewById(R.id.post);
            holder.status = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);
            holder.comments = (Button) convertView.findViewById(R.id.btnComment);
            holder.timeStamp = (TextView) convertView.findViewById(R.id.timeStamp);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ((listData.get(position)).getImage() == null) {

            holder.progressBar.setVisibility(View.GONE);
            holder.feedImage.setImageDrawable(null);
            holder.feedImage.setVisibility(View.GONE);
        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.feedImage.setVisibility(View.VISIBLE);
            picasso.load((listData.get(position)).getImage())
                    .placeholder(R.drawable.grey_image)
                    .into(holder.feedImage,
                            new com.squareup.picasso.Callback() {

                                @Override
                                public void onError() {

                                }

                                @Override
                                public void onSuccess() {
                                    holder.progressBar.setVisibility(View.GONE);
                                }

                            });
        }

        holder.feedImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                resume = context.getSharedPreferences("resume",
                        Context.MODE_PRIVATE);
                edt = resume.edit();
                edt.putBoolean("yesNo", false);
                edt.commit();
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);
                intent.putExtra("URLimage", (listData.get(position).getImage()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.voteValue.setText((listData.get(position)).getVotes());
        holder.status.setText((listData.get(position)).getStatus());

        holder.comments.setText(""
                + (listData.get(position)).getComments_number() + " Comments");
        holder.place.setText((listData.get(position)).getName());
        holder.timeStamp.setText((listData.get(position)).getTimeStamp() + " ago");
        final String postId = (listData.get(position)).getId();
        holder.delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                alertBuilder.setMessage("Delete this post?");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        api.deleteMyLocalPost(token, postId, new Callback<String>() {

                            @Override
                            public void failure(RetrofitError arg0) {
                                handleBan(arg0);
                                Response r = arg0.getResponse();
                                if (r != null && r.getStatus() == 401) {
                                    if (loginType.equals("facebook")) {
                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.deleteMyLocalPost(token, postId, new Callback<String>() {
                                                            @Override
                                                            public void success(String s, Response response) {

                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    } else {
                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.deleteMyLocalPost(token, postId, new Callback<String>() {
                                                            @Override
                                                            public void success(String s, Response response) {

                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    }
                                }

                            }

                            @Override
                            public void success(String arg0, Response arg1) {

                            }

                        });
                        listData.remove(position);
                        notifyDataSetChanged();
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();


            }
        });
        holder.post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeComments = context.getSharedPreferences("MYTOKEN",
                        Context.MODE_PRIVATE);
                edt = resumeComments.edit();
                edt.putBoolean("yesNoComments", false);
                edt.commit();

                CommentData commentData = new CommentData();
                commentData.setUserId(myUserId);
                commentData.setVoteValueSend(listData.get(position).getVotes());
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setPlace(listData.get(position).getName());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setPostId(listData.get(position).getId());
                if ((listData.get(position)).getImage() == null) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }

                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", postType);
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resumeComments = context.getSharedPreferences("MYTOKEN",
                        Context.MODE_PRIVATE);
                edt = resumeComments.edit();
                edt.putBoolean("yesNoComments", false);
                edt.commit();

                CommentData commentData = new CommentData();
                commentData.setUserId(myUserId);
                commentData.setVoteValueSend(listData.get(position).getVotes());
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setPlace(listData.get(position).getName());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setPostId(listData.get(position).getId());
                if ((listData.get(position)).getImage() == null) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }

                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", postType);
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        // ubaciti još sliku!!!!

        return convertView;
    }

    static class ViewHolder {
        RelativeLayout post;
        ImageView feedImage;
        ImageButton delete;
        ProgressBar progressBar;
        Button comments;
        TextView voteValue, place, timeStamp;
        ExpandableTextView status;

    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(context);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

}
