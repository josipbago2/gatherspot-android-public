package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 28.8.2015..
 */
public class PanoramioImage {
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
