package gatherspot.gatherspotapp.com.gatherspot.modules.loginreg;

/**
 * Created by Josip on 29.5.2015..
 */

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import gatherspot.gatherspotapp.com.gatherspot.modules.shared.ActivitySetProfilePic;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityRegisterUsername extends AppCompatActivity {

    Intent intent;
    Button done;
    EditText usernameInput;
    String userEmail;

    Context context;
    InputFilter filter1, filter2;
    String blockedCharacters = "#!$%&()='*+? \"\\@~:;,/";

    RestAdapter adapter;
    ApiInterface api;


    SharedPreferences myTokenPrefs, lUpdatesPrefs, myPersonalExistPrefs, loginPrefs, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edt1, edt2, edtToken;
    String token, user_id, loginType;

    ProgressBar pb;

    public static final String ENDPOINT = "https://service.gatherspot.me";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setupuser_login);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        context = ActivityRegisterUsername.this;

        // razlikuje fb login i email login
        loginPrefs = getSharedPreferences("login", Context.MODE_PRIVATE);

        // startAlarmManager();
        lUpdatesPrefs = getSharedPreferences("lupdates", Context.MODE_PRIVATE);
        edt1 = lUpdatesPrefs.edit();
        edt1.putString("sessionID", UUID.randomUUID().toString());
        edt1.putBoolean("lonoff", true);
        edt1.commit();

        myTokenPrefs = getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");
        edtToken = myTokenPrefs.edit();
        user_id = myTokenPrefs.getString("myuserid", "");
        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        myPersonalExistPrefs = getSharedPreferences("mypersonalexist",
                Context.MODE_PRIVATE);
        edt2 = myPersonalExistPrefs.edit();
        edt2.putBoolean("exist", false);
        edt2.commit();

        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");

        iFilter();
        userEmail = getUsername();
        if (userEmail != null) {
            for (char c : userEmail.toCharArray()) {
                if (blockedCharacters.contains(String.valueOf(c))) {
                    userEmail = null;
                }
            }
        }
        pb = (ProgressBar) findViewById(R.id.progressBar);
        usernameInput = (EditText) findViewById(R.id.usernameInput);
        usernameInput.setFilters(new InputFilter[]{filter1, filter2});
        if (userEmail != null) {
            usernameInput.setText(userEmail);
        }
        done = (Button) findViewById(R.id.btnDone);

        api = InitHttpClient.getInstance().getApiInterface();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(usernameInput.getText().toString());
            }
        });


    }


    private String getUsername() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();
        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }
        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");

            if (parts.length > 1)
                return parts[0];
        }
        return null;
    }

    private void iFilter() {
        filter2 = new InputFilter.LengthFilter(40);
        filter1 = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) {
                        sb.append(c);                   // put your condition here
                    } else {
                        keepOriginal = false;
                        Toast.makeText(context, "Character not allowed", Toast.LENGTH_SHORT).show();
                    }
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                if (Character.isLetterOrDigit(c)) {
                    return true;
                } else if (blockedCharacters.contains(String.valueOf(c))) {
                    return false;
                } else {
                    return true;
                }
            }
        };
    }

    private void setUserData(final String username) {
        if (username.equals("")) {
            Toast.makeText(ActivityRegisterUsername.this, "Please enter username", Toast.LENGTH_SHORT).show();
        } else {
            pb.setVisibility(View.VISIBLE);
            done.setClickable(false);
            api.changeUserData(gcmPrefs.getString("gcmToken", ""), token, username, "", new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    edtToken.putString("myusername", username);
                    edtToken.commit();
                    if (loginPrefs.getBoolean("fb", false)) {
                        intent = new Intent(
                                ActivityRegisterUsername.this,
                                ActivityTutorial.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        intent = new Intent(
                                ActivityRegisterUsername.this,
                                ActivitySetProfilePic.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }

                @Override
                public void failure(RetrofitError arg0) {
                    handleBan(arg0);
                    Response r = arg0.getResponse();
                    if (r != null && r.getStatus() == 401) {
                        if (loginType.equals("facebook")) {
                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                        @Override
                                        public void success(LoginItemToken arg0, Response response) {
                                            edtToken.putString("mytoken", arg0.getToken());
                                            edtToken.putString("myuserid", arg0.getUser_id());
                                            edtToken.commit();
                                            token = arg0.getToken();
                                            api.changeUserData(gcmPrefs.getString("gcmToken", ""),
                                                    token, username, "", new Callback<Response>() {
                                                        @Override
                                                        public void success(Response response, Response response2) {
                                                            edtToken.putString("myusername", username);
                                                            edtToken.commit();
                                                            if (loginPrefs.getBoolean("fb", false)) {
                                                                intent = new Intent(
                                                                        ActivityRegisterUsername.this,
                                                                        ActivityTutorial.class);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            } else {
                                                                intent = new Intent(
                                                                        ActivityRegisterUsername.this,
                                                                        ActivitySetProfilePic.class);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            }
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            pb.setVisibility(View.INVISIBLE);
                                                            done.setClickable(true);
                                                            handleBan(error);
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            pb.setVisibility(View.INVISIBLE);
                                            done.setClickable(true);
                                            handleBan(error);
                                        }
                                    });
                        } else {
                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                        @Override
                                        public void success(LoginItemToken arg0, Response response) {
                                            edtToken.putString("mytoken", arg0.getToken());
                                            edtToken.putString("myuserid", arg0.getUser_id());
                                            edtToken.commit();
                                            token = arg0.getToken();
                                            api.changeUserData(gcmPrefs.getString("gcmToken", ""),
                                                    token, username, "", new Callback<Response>() {
                                                        @Override
                                                        public void success(Response response, Response response2) {
                                                            edtToken.putString("myusername", username);
                                                            edtToken.commit();
                                                            if (loginPrefs.getBoolean("fb", false)) {
                                                                intent = new Intent(
                                                                        ActivityRegisterUsername.this,
                                                                        ActivityTutorial.class);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            } else {
                                                                intent = new Intent(
                                                                        ActivityRegisterUsername.this,
                                                                        ActivitySetProfilePic.class);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            }
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            pb.setVisibility(View.INVISIBLE);
                                                            done.setClickable(true);
                                                            handleBan(error);
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            pb.setVisibility(View.INVISIBLE);
                                            done.setClickable(true);
                                            handleBan(error);
                                        }
                                    });
                        }
                    }
                }
            });
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivityRegisterUsername.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }


}