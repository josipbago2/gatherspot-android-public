package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 29.5.2015..
 */

public class PersonalPostItem {

    private String id, image, status, user_id, timeStamp, up_votes, upvoted,
            downvoted, comments_number;

    public String getDownvoted() {
        return downvoted;
    }

    public void setDownvoted(String downvoted) {
        this.downvoted = downvoted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUp_votes() {
        return up_votes;
    }

    public void setUp_votes(String up_votes) {
        this.up_votes = up_votes;
    }

    public String getUpvoted() {
        return upvoted;
    }

    public void setUpvoted(String upvoted) {
        this.upvoted = upvoted;
    }

    public String getComments_number() {
        return comments_number;
    }

    public void setComments_number(String comments_number) {
        this.comments_number = comments_number;
    }

}
