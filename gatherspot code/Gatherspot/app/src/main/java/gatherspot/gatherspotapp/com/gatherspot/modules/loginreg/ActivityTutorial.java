package gatherspot.gatherspotapp.com.gatherspot.modules.loginreg;

/**
 * Created by Josip on 29.5.2015..
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import gatherspot.gatherspotapp.com.gatherspot.MainActivity;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ViewPagerAdapter;

public class ActivityTutorial extends Activity {

    ViewPager viewPager;
    PagerAdapter adapter;
    Button btnContinue;
    int[] flag;
    LinearLayout llDots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_layout);
        System.gc();

        flag = new int[]{R.drawable.tutorial1, R.drawable.tutorial2,
                R.drawable.tutorial3, R.drawable.tutorial4};

        btnContinue = (Button) findViewById(R.id.btnContinue);
        llDots = (LinearLayout) findViewById(R.id.llDots);
        viewPager = (ViewPager) findViewById(R.id.pager);

        btnContinue.setVisibility(View.INVISIBLE);
        btnContinue.setClickable(false);

        adapter = new ViewPagerAdapter(this, flag);
        viewPager.setAdapter(adapter);


        for (int i = 0; i < adapter.getCount(); i++) {
            ImageButton imgDot = new ImageButton(this);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.dot_selector);
            imgDot.setBackgroundResource(0);
            imgDot.setPadding(5, 5, 5, 20);
            LayoutParams params = new LayoutParams(20, 35);
            imgDot.setLayoutParams(params);
            if (i == 0)
                imgDot.setSelected(true);

            llDots.addView(imgDot);

            viewPager.addOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int pos) {
                    if (pos == 3) {
                        btnContinue.setVisibility(View.VISIBLE);
                        btnContinue.setClickable(true);
                    }
                    for (int i = 0; i < adapter.getCount(); i++) {
                        if (i != pos) {
                            ((ImageView) llDots.findViewWithTag(i))
                                    .setSelected(false);
                        }
                    }
                    ((ImageView) llDots.findViewWithTag(pos)).setSelected(true);
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {

                }
            });
        }
        btnContinue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityTutorial.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
    }

}
