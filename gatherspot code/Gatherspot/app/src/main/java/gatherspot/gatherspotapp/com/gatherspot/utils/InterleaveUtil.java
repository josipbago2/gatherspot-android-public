package gatherspot.gatherspotapp.com.gatherspot.utils;

/**
 * Created by Josip on 29.5.2015..
 */

import static java.util.Collections.reverse;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InterleaveUtil {

    public static <T> List<T> interleave(List<T> first, List<T> second,
                                         List<T> third) {
        int i = 0;
        final int totalLength = first.size() + second.size() + third.size();

        final List<T> result = new ArrayList<T>(totalLength);

        boolean firstEmpty = false, secondEmpty = false, thirdEmpty = false;

        final Stack<T> firstStack = new Stack<T>();
        final Stack<T> secondStack = new Stack<T>();
        final Stack<T> thirdStack = new Stack<T>();

        firstStack.addAll(first);
        secondStack.addAll(second);
        thirdStack.addAll(third);

        reverse(firstStack);
        reverse(secondStack);
        reverse(thirdStack);

        if (firstStack.isEmpty()) {
            firstEmpty = true;
        }
        if (secondStack.isEmpty()) {
            secondEmpty = true;
        }
        if (thirdStack.isEmpty()) {
            thirdEmpty = true;
        }

        while (!firstEmpty || !secondEmpty || !thirdEmpty) {
            switch (i % 3) {
                case 0:
                    if (firstEmpty == false) {
                        result.add((firstStack.pop()));
                    } else {
                        /*
                        if (secondEmpty == false) {
                            result.add(secondStack.pop());
                        } else {
                            result.add(thirdStack.pop());
                        }
                        */
                    }
                    break;
                case 1:
                    if (secondEmpty == false) {
                        result.add(secondStack.pop());
                    } else {
                        /*
                        if (thirdEmpty == false) {
                            result.add(thirdStack.pop());
                        } else {
                            result.add(firstStack.pop());
                        }
                        */
                    }

                    break;
                case 2:
                    if (thirdEmpty == false) {
                        result.add(thirdStack.pop());
                    } else {
                        /*
                        if (firstEmpty == false) {
                            result.add(firstStack.pop());
                        } else {
                            result.add(secondStack.pop());
                        }
                        */
                    }

                    break;
            }

            if (firstStack.isEmpty()) {
                firstEmpty = true;
            }
            if (secondStack.isEmpty()) {
                secondEmpty = true;
            }
            if (thirdStack.isEmpty()) {
                thirdEmpty = true;
            }
            i++;
        }




		/*
        boolean selected = false;
		for (int i = 0; i < totalLength; ++i) {
			if (!selected && !first.isEmpty()) {
				result.add(firstStack.pop());
			} else {
				result.add(secondStack.pop());
			}
			if (firstStack.isEmpty()) {
				selected = true;
			} else if (secondStack.isEmpty()) {
				selected = false;
			} else {
				selected = !selected;
			}
		}
		*/
        return result;
    }
}
