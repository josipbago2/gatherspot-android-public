package gatherspot.gatherspotapp.com.gatherspot.modules.settings;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;

import java.util.LinkedList;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Josip on 14.8.2015..
 */
public class ActivityContactUs extends AppCompatActivity {

    EditText email, message;
    String prefix = "";
    String responseMessage = "";
    ApiInterface api;
    String token;
    String loginType;
    SharedPreferences myTokenPrefs, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edtToken;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactus_layout);

        Intent intent = getIntent();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        api = InitHttpClient.getInstance().getApiInterface();
        myTokenPrefs = getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");

        gcmPrefs = getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        email = (EditText) findViewById(R.id.emailInput);
        message = (EditText) findViewById(R.id.message);

        if (intent.getStringExtra("id").equals("feedback")) {
            prefix = "feedback";
            responseMessage = "Thank you for your help in making Gatherspot better. " +
                    "We will revise your feedback, and if required get back to you";
            getSupportActionBar().setTitle("Give us feedback");
            message.setHint("Briefly explain what you love, or what could improve.");
        } else if (intent.getStringExtra("id").equals("reportaproblem")) {
            prefix = "problem";
            responseMessage = "Thank you for your help in making Gatherspot better. " +
                    "We will revise your feedback, and if required get back to you";
            getSupportActionBar().setTitle("Report a problem");
            message.setHint("Briefly explain what happened.");
        } else if (intent.getStringExtra("id").equals("reportbb")) {
            prefix = "bad behaviour";
            responseMessage = "Thank you for your help in making Gatherspot better. " +
                    "We will revise your report and solve the issue as soon as possible, and if required get back to you";
            getSupportActionBar().setTitle("Report bad behaviour");
            message.setHint("Briefly explain what happened and who is involved.");
        } else if (intent.getStringExtra("id").equals("askaquestion")) {
            prefix = "question";
            responseMessage = "Thank you for your question. " +
                    "We will try to answer you as soon as possible";
            getSupportActionBar().setTitle("Ask a question");
            message.setHint("Ask us a question.");
        }

        if (getEmail() != null) {
            email.setText(getEmail());
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(getApplicationContext());
            }
        }
    }

    private String getEmail() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();
        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }
        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            return email;
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contactus_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Send:
                final String text = email.getText().toString() + "\n" + prefix + "\n" + message.getText().toString();
                api.reportProblem(token, text, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ActivityContactUs.this);
                        alertBuilder.setTitle("Gatherspot");
                        alertBuilder.setMessage(responseMessage);
                        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }

                    @Override
                    public void failure(RetrofitError arg0) {
                        handleBan(arg0);
                        Response r = arg0.getResponse();
                        if (r != null && r.getStatus() == 401) {
                            if (loginType.equals("facebook")) {
                                api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                        AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.reportProblem(token, text, new Callback<Response>() {
                                                    @Override
                                                    public void success(Response response, Response response2) {
                                                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ActivityContactUs.this);
                                                        alertBuilder.setTitle("Gatherspot");
                                                        alertBuilder.setMessage(responseMessage);
                                                        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                finish();
                                                            }
                                                        });
                                                        AlertDialog alertDialog = alertBuilder.create();
                                                        alertDialog.show();
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            } else {
                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                            @Override
                                            public void success(LoginItemToken arg0, Response response) {
                                                edtToken.putString("mytoken", arg0.getToken());
                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                edtToken.commit();
                                                token = arg0.getToken();
                                                api.reportProblem(token, text, new Callback<Response>() {
                                                    @Override
                                                    public void success(Response response, Response response2) {
                                                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ActivityContactUs.this);
                                                        alertBuilder.setTitle("Gatherspot");
                                                        alertBuilder.setMessage(responseMessage);
                                                        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                finish();
                                                            }
                                                        });
                                                        AlertDialog alertDialog = alertBuilder.create();
                                                        alertDialog.show();
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                            }
                        }
                    }
                });
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivityContactUs.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

}
