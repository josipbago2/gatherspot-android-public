package gatherspot.gatherspotapp.com.gatherspot.pojo;

/**
 * Created by Josip on 30.9.2015..
 */
public class UserItem {

    private String username, profilePic;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }
}
