package gatherspot.gatherspotapp.com.gatherspot.modules.shared;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dd.CircularProgressButton;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterComments;
import gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile.ActivityUserProfile;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentData;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentItemFill;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentItemResponse;
import gatherspot.gatherspotapp.com.gatherspot.pojo.Feed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LocalPost;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.NewComment;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPost;
import gatherspot.gatherspotapp.com.gatherspot.utils.FullScreenImageActivity;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Josip on 16.6.2015..
 */
public class ActivityComments extends AppCompatActivity {

    ListView lv1;
    EditText newComment;
    CircularProgressButton sendComment;

    boolean thisMyLocalPost = false;

    int postType;
    String postId, userId, myUserId;
    CommentData commentData;

    ProgressBar circle;

    Context context;

    // za prepoznavanje dali je intent iz my profile ili user profile u vezi dropmenua
    boolean identifier = false;

    public int counterNewComments = 0;

    BitmapDrawable image;

    View header;
    ImageView profilePic, feedImage, iconTime;
    Button username;
    ProgressBar progressBar;
    TextView distance, location, timeStamp, voteValue;
    ExpandableTextView status;
    Button loadMore, comment;
    ImageButton dropMenu, upVote, downVote, delete;

    private static final int PERSONAL = 1;
    private static final int ADS = 2;
    private static final int LOCAL = 3;
    private static final int MY_PROFILE = 4;
    private static final int MY_LOCAL = 5;

    int pagingCounter = -1;
    ListAdapterComments listAdapter;
    List<CommentItemFill> listOfComments;
    private static final int PAGING_SIZE = 5;

    ApiInterface api;

    RelativeLayout background;

    InputMethodManager imm;

    String token, loginType;
    boolean resume;
    SharedPreferences.Editor edtToken;
    SharedPreferences myTokenPrefs, resumeHandler, loginTypePrefs, gcmPrefs, backgroundImagePrefs;

    private void sharedPreferencesInitializer() {
        myTokenPrefs = getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        myUserId = myTokenPrefs.getString("myuserid", "");
        resumeHandler = getSharedPreferences("RESUMEHANDLER",
                Context.MODE_PRIVATE);
        resume = resumeHandler.getBoolean("resume?comments", true);
        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");
        backgroundImagePrefs = getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));
        getSupportActionBar().setTitle("Comments");

        imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);

        context = ActivityComments.this;

        sharedPreferencesInitializer();
        api = InitHttpClient.getInstance().getApiInterface();

        background = (RelativeLayout) findViewById(R.id.background);
        if (BackgroundImageHandler.fileExistance(this)) {
            /*
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.55f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(getResources(),
                    BackgroundImageHandler.loadImageFromStorage(this));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
            */
            setBackground();
        }

        circle = (ProgressBar) findViewById(R.id.progressBarComments);
        newComment = (EditText) findViewById(R.id.etnewComment);
        sendComment = (CircularProgressButton) findViewById(R.id.newComment);
        sendComment.setIndeterminateProgressMode(true);

        Intent intent = getIntent();

        // provjera otkud se pristupa komentarima (notf ili aplikacija)
        if (intent.getBooleanExtra("notification", false)) {
            // ako se pristupa iz push notifikacije
            postId = intent.getStringExtra("notfPostId");
            postType = intent.getIntExtra("postType", 0);
            switch (postType) {
                case PERSONAL:
                    getPersonalPost();
                    break;
                case LOCAL:
                    getLocalPost();
                    break;
                default:
                    break;
            }
        } else {
            // ako se pristupa od bilo kud drugdje
            // tip posta i komentara + 1 zbog numeracija getviewtypeova u adapteru

            postType = intent.getIntExtra("postType", 0) + 1;

            // lokalno dohva�anje podataka o postu �iji su komentari
            commentData = (CommentData) intent.getSerializableExtra("commentData");
            // provjera vrste posta
            switch (postType) {
                case PERSONAL:
                    initializePersonalHeader();

                    break;
                case LOCAL:
                    initializeLocalHeader();

                    break;
                case MY_PROFILE:
                    initializeMyProfileHeader();

                    break;
                case ADS:
                    // naknadno, zanemari
                    header = getLayoutInflater().inflate(
                            R.layout.feed_item_lf_localads, null);
                    break;
                default:
                    initializeDefaultHeader();

                    break;
            }
            initializeEqual();
            setView();
        }


    }

    private void setBackground() {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0.45f);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        image = new BitmapDrawable(context.getResources(),
                BackgroundImageHandler.loadImageFromStorage(context));
        if (!image.getBitmap().isRecycled()) {
            image.setColorFilter(filter);
            if (Build.VERSION.SDK_INT > 15) {
                getWindow().setBackgroundDrawable(image);
            } else {
                getWindow().setBackgroundDrawable(image);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(getApplicationContext());
            }
        }
        // za push notifikacije seen

        api.seenComments(token, postId, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }
        });

    }


    @Override
    protected void onStop() {
        super.onStop();
        api.seenComments(token, postId, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }
        });
    }


    private void updateNewCommentandSetView(String commentId) {
        counterNewComments++;
        CommentItemFill commentItemFill = new CommentItemFill();
        commentItemFill.setUser_id(myUserId);
        commentItemFill.setId(commentId);
        commentItemFill.setStatus(newComment.getText().toString());
        commentItemFill.setProfile_pic_url(myTokenPrefs.getString("myprofilepicurl", ""));
        commentItemFill.setTimestamp("Just now");
        commentItemFill.setUsername(myTokenPrefs.getString("myusername", ""));
        newComment.setText("");
        hideKeyboard();
        ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                .getWrappedAdapter()).refillNewComment(commentItemFill);
        sendComment.setProgress(0);
        /*
        lv1.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                lv1.setSelection(lv1.getCount() - 1);
            }
        });
        */
    }

    private void getComments() {
        pagingCounter++;
        listOfComments = new ArrayList<CommentItemFill>();
        if (postType == LOCAL || postType == MY_LOCAL) {
            api.getLocalComments(token, postId, String.valueOf(PAGING_SIZE),
                    String.valueOf(pagingCounter), new Callback<Feed>() {
                        @Override
                        public void success(Feed arg0, Response response) {
                            for (CommentItemResponse item : arg0.getComments()) {
                                CommentItemFill list = new CommentItemFill();
                                list.setId(item.getId());
                                list.setStatus(item.getStatus());
                                list.setTimestamp(item.getTimestamp());
                                list.setUser_id((item.getUser_id()).getUser_id());
                                list.setUsername((item.getUser_id()).getUsername());
                                if ((item.getUser_id()).getProfile_pic_url() == null) {
                                    list.setProfile_pic_url("");
                                } else {
                                    list.setProfile_pic_url((item.getUser_id())
                                            .getProfile_pic_url());
                                }
                                if (listOfComments != null) {
                                    listOfComments.add(list);
                                }
                            }
                            if (listOfComments != null) {
                                if (listOfComments.size() < PAGING_SIZE) {
                                    loadMore.setVisibility(View.GONE);
                                }
                                if (pagingCounter == 0) {
                                    circle.setVisibility(View.GONE);
                                    listAdapter = new ListAdapterComments(context,
                                            listOfComments, postType, identifier, postId);
                                    lv1.setAdapter(listAdapter);
                                } else {
                                    ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                            .getWrappedAdapter()).pagingRefill(listOfComments);
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError arg0) {
                            handleBan(arg0);
                            Response r = arg0.getResponse();
                            if (r != null && r.getStatus() == 401) {
                                if (loginType.equals("facebook")) {
                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                @Override
                                                public void success(LoginItemToken arg0, Response response) {
                                                    edtToken.putString("mytoken", arg0.getToken());
                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                    edtToken.commit();
                                                    token = arg0.getToken();
                                                    api.getLocalComments(token, postId, String.valueOf(PAGING_SIZE),
                                                            String.valueOf(pagingCounter), new Callback<Feed>() {
                                                                @Override
                                                                public void success(Feed arg0, Response response) {
                                                                    for (CommentItemResponse item : arg0.getComments()) {
                                                                        CommentItemFill list = new CommentItemFill();
                                                                        list.setId(item.getId());
                                                                        list.setStatus(item.getStatus());
                                                                        list.setTimestamp(item.getTimestamp());
                                                                        list.setUser_id((item.getUser_id()).getUser_id());
                                                                        list.setUsername((item.getUser_id()).getUsername());
                                                                        if ((item.getUser_id()).getProfile_pic_url() == null) {
                                                                            list.setProfile_pic_url("");
                                                                        } else {
                                                                            list.setProfile_pic_url((item.getUser_id())
                                                                                    .getProfile_pic_url());
                                                                        }
                                                                        listOfComments.add(list);
                                                                    }
                                                                    if (listOfComments.size() < PAGING_SIZE) {
                                                                        loadMore.setVisibility(View.GONE);
                                                                    }
                                                                    if (pagingCounter == 0) {
                                                                        circle.setVisibility(View.GONE);
                                                                        listAdapter = new ListAdapterComments(context,
                                                                                listOfComments, postType, identifier, postId);
                                                                        lv1.setAdapter(listAdapter);
                                                                    } else {
                                                                        ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                                                                .getWrappedAdapter()).pagingRefill(listOfComments);
                                                                    }
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {
                                                                    handleBan(error);
                                                                }
                                                            });
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    handleBan(error);
                                                }
                                            });
                                } else {
                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                @Override
                                                public void success(LoginItemToken arg0, Response response) {
                                                    edtToken.putString("mytoken", arg0.getToken());
                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                    edtToken.commit();
                                                    token = arg0.getToken();
                                                    api.getLocalComments(token, postId, String.valueOf(PAGING_SIZE),
                                                            String.valueOf(pagingCounter), new Callback<Feed>() {
                                                                @Override
                                                                public void success(Feed arg0, Response response) {
                                                                    for (CommentItemResponse item : arg0.getComments()) {
                                                                        CommentItemFill list = new CommentItemFill();
                                                                        list.setId(item.getId());
                                                                        list.setStatus(item.getStatus());
                                                                        list.setTimestamp(item.getTimestamp());
                                                                        list.setUser_id((item.getUser_id()).getUser_id());
                                                                        list.setUsername((item.getUser_id()).getUsername());
                                                                        if ((item.getUser_id()).getProfile_pic_url() == null) {
                                                                            list.setProfile_pic_url("");
                                                                        } else {
                                                                            list.setProfile_pic_url((item.getUser_id())
                                                                                    .getProfile_pic_url());
                                                                        }
                                                                        listOfComments.add(list);
                                                                    }
                                                                    if (listOfComments.size() < PAGING_SIZE) {
                                                                        loadMore.setVisibility(View.GONE);
                                                                    }
                                                                    if (pagingCounter == 0) {
                                                                        circle.setVisibility(View.GONE);
                                                                        listAdapter = new ListAdapterComments(context,
                                                                                listOfComments, postType, identifier, postId);
                                                                        lv1.setAdapter(listAdapter);
                                                                    } else {
                                                                        ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                                                                .getWrappedAdapter()).pagingRefill(listOfComments);
                                                                    }
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {
                                                                    handleBan(error);
                                                                }
                                                            });
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    handleBan(error);
                                                }
                                            });
                                }
                            }
                        }
                    });
        } else {
            api.getPersonalComments(token, postId, String.valueOf(PAGING_SIZE),
                    String.valueOf(pagingCounter), new Callback<Feed>() {
                        @Override
                        public void success(Feed arg0, Response response) {
                            for (CommentItemResponse item : arg0.getComments()) {
                                CommentItemFill list = new CommentItemFill();
                                list.setId(item.getId());
                                list.setStatus(item.getStatus());
                                list.setTimestamp(item.getTimestamp());
                                list.setUser_id((item.getUser_id()).getUser_id());
                                list.setUsername((item.getUser_id()).getUsername());
                                if ((item.getUser_id()).getProfile_pic_url() == null) {
                                    list.setProfile_pic_url("");
                                } else {
                                    list.setProfile_pic_url((item.getUser_id())
                                            .getProfile_pic_url());
                                }
                                listOfComments.add(list);
                            }
                            if (listOfComments.size() < PAGING_SIZE) {
                                loadMore.setVisibility(View.GONE);
                            }
                            if (pagingCounter == 0) {
                                circle.setVisibility(View.GONE);
                                listAdapter = new ListAdapterComments(context,
                                        listOfComments, postType, identifier, postId);
                                lv1.setAdapter(listAdapter);
                            } else {
                                ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                        .getWrappedAdapter()).pagingRefill(listOfComments);
                            }
                        }

                        @Override
                        public void failure(RetrofitError arg0) {
                            handleBan(arg0);
                            Response r = arg0.getResponse();
                            if (r != null && r.getStatus() == 401) {
                                if (loginType.equals("facebook")) {
                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                @Override
                                                public void success(LoginItemToken arg0, Response response) {
                                                    edtToken.putString("mytoken", arg0.getToken());
                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                    edtToken.commit();
                                                    token = arg0.getToken();
                                                    api.getPersonalComments(token, postId, String.valueOf(PAGING_SIZE),
                                                            String.valueOf(pagingCounter), new Callback<Feed>() {
                                                                @Override
                                                                public void success(Feed arg0, Response response) {
                                                                    for (CommentItemResponse item : arg0.getComments()) {
                                                                        CommentItemFill list = new CommentItemFill();
                                                                        list.setId(item.getId());
                                                                        list.setStatus(item.getStatus());
                                                                        list.setTimestamp(item.getTimestamp());
                                                                        list.setUser_id((item.getUser_id()).getUser_id());
                                                                        list.setUsername((item.getUser_id()).getUsername());
                                                                        if ((item.getUser_id()).getProfile_pic_url() == null) {
                                                                            list.setProfile_pic_url("");
                                                                        } else {
                                                                            list.setProfile_pic_url((item.getUser_id())
                                                                                    .getProfile_pic_url());
                                                                        }
                                                                        listOfComments.add(list);
                                                                    }
                                                                    if (listOfComments.size() < PAGING_SIZE) {
                                                                        loadMore.setVisibility(View.GONE);
                                                                    }
                                                                    if (pagingCounter == 0) {
                                                                        circle.setVisibility(View.GONE);
                                                                        listAdapter = new ListAdapterComments(context,
                                                                                listOfComments, postType, identifier, postId);
                                                                        lv1.setAdapter(listAdapter);
                                                                    } else {
                                                                        ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                                                                .getWrappedAdapter()).pagingRefill(listOfComments);
                                                                    }
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {
                                                                    handleBan(error);
                                                                }
                                                            });
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    handleBan(error);
                                                }
                                            });
                                } else {
                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                @Override
                                                public void success(LoginItemToken arg0, Response response) {
                                                    edtToken.putString("mytoken", arg0.getToken());
                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                    edtToken.commit();
                                                    token = arg0.getToken();
                                                    api.getPersonalComments(token, postId, String.valueOf(PAGING_SIZE),
                                                            String.valueOf(pagingCounter), new Callback<Feed>() {
                                                                @Override
                                                                public void success(Feed arg0, Response response) {
                                                                    for (CommentItemResponse item : arg0.getComments()) {
                                                                        CommentItemFill list = new CommentItemFill();
                                                                        list.setId(item.getId());
                                                                        list.setStatus(item.getStatus());
                                                                        list.setTimestamp(item.getTimestamp());
                                                                        list.setUser_id((item.getUser_id()).getUser_id());
                                                                        list.setUsername((item.getUser_id()).getUsername());
                                                                        if ((item.getUser_id()).getProfile_pic_url() == null) {
                                                                            list.setProfile_pic_url("");
                                                                        } else {
                                                                            list.setProfile_pic_url((item.getUser_id())
                                                                                    .getProfile_pic_url());
                                                                        }
                                                                        listOfComments.add(list);
                                                                    }
                                                                    if (listOfComments.size() < PAGING_SIZE) {
                                                                        loadMore.setVisibility(View.GONE);
                                                                    }
                                                                    if (pagingCounter == 0) {
                                                                        circle.setVisibility(View.GONE);
                                                                        listAdapter = new ListAdapterComments(context,
                                                                                listOfComments, postType, identifier, postId);
                                                                        lv1.setAdapter(listAdapter);
                                                                    } else {
                                                                        ((ListAdapterComments) ((HeaderViewListAdapter) lv1.getAdapter())
                                                                                .getWrappedAdapter()).pagingRefill(listOfComments);
                                                                    }
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {
                                                                    handleBan(error);
                                                                }
                                                            });
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    handleBan(error);
                                                }
                                            });
                                }
                            }
                        }
                    });
        }
    }


    private void initializeEqual() {
        postId = commentData.getPostId();
        userId = commentData.getUserId();
        if (userId.equals(myUserId)) {
            identifier = true;
        }
        loadMore = (Button) header.findViewById(R.id.btnMoreComments);
        status = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
        timeStamp = (TextView) header.findViewById(R.id.timeStamp);
        voteValue = (TextView) header.findViewById(R.id.voteValue);
        feedImage = (ImageView) header.findViewById(R.id.feedImage1);
        progressBar = (ProgressBar) header.findViewById(R.id.progressBar1);
        comment = (Button) header.findViewById(R.id.btnComment);
        comment.setVisibility(View.INVISIBLE);

        timeStamp.setText(commentData.getTimeStampSend() + " ago");
        // handlanje timestampa desne margine za my local post
        if (thisMyLocalPost) {
            iconTime = (ImageView) header.findViewById(R.id.icontime);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) iconTime.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            if (timeStamp.getText().toString().contains("min")) {
                params.setMargins(0, 0, 110, 0);
            } else {
                params.setMargins(0, 0, 90, 0);
            }
            iconTime.setLayoutParams(params);
        }

        status.setText(commentData.getStatusSend());
        voteValue.setText(commentData.getVoteValueSend());

        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getComments();
            }
        });
        feedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);
                intent.putExtra("URLimage", commentData.getImageNumberFeedImage());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        if (commentData.getImageNumberFeedImage().isEmpty()) {
            feedImage.setImageDrawable(null);
            feedImage.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            Picasso picasso = InitHttpClient.getInstance().getImageLoader();
            picasso.load(commentData.getImageNumberFeedImage())
                    .placeholder(R.drawable.grey_image)
                    .into(feedImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    private void initializePersonalHeader() {

        header = getLayoutInflater().inflate(
                R.layout.feed_item_header_comments_1, null);
        userId = commentData.getUserId();
        profilePic = (ImageView) header.findViewById(R.id.profilePic);
        username = (Button) header.findViewById(R.id.btnUsername);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);
        upVote = (ImageButton) header.findViewById(R.id.btnUpVote);
        downVote = (ImageButton) header.findViewById(R.id.btnDownVote);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);

        username.setText(commentData.getUsernameSend());
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userid", userId);
                context.startActivity(intent);
            }
        });
        if (userId.equals(myUserId)) {
            username.setClickable(false);
        }


        upVote.setVisibility(View.GONE);
        downVote.setVisibility(View.GONE);

        dropMenu.setVisibility(View.GONE);

        if (commentData.getImageNumberProfile().isEmpty()) {
            profilePic.setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (commentData.getImageNumberProfile().contains("gatherspot")) {
                Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                picasso.load(commentData.getImageNumberProfile())
                        .into(profilePic);
            } else {
                Picasso.with(context).load(commentData.getImageNumberProfile())
                        .into(profilePic);
            }

        }

    }

    private void initializeLocalHeader() {
        userId = commentData.getUserId();
        header = getLayoutInflater().inflate(R.layout.feed_item_header_comments_2,
                null);
        profilePic = (ImageView) header.findViewById(R.id.profilePic);
        username = (Button) header.findViewById(R.id.btnUsername);
        distance = (TextView) header.findViewById(R.id.distance);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);
        upVote = (ImageButton) header.findViewById(R.id.btnUpVote);
        downVote = (ImageButton) header.findViewById(R.id.btnDownVote);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);

        upVote.setVisibility(View.GONE);
        downVote.setVisibility(View.GONE);
        dropMenu.setVisibility(View.GONE);
        username.setText(commentData.getUsernameSend());
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userid", userId);
                context.startActivity(intent);
            }
        });
        if (userId.equals(myUserId)) {
            username.setClickable(false);
        }
        if (distance != null) {
            String[] arrDist = commentData.getDistanceSend().split("\\.");
            distance.setText(arrDist[0] + "m");
        }

        if (commentData.getImageNumberProfile().isEmpty()) {
            profilePic.setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (commentData.getImageNumberProfile().contains("gatherspot")) {
                Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                picasso.load(commentData.getImageNumberProfile())
                        .into(profilePic);
            } else {
                Picasso.with(context).load(commentData.getImageNumberProfile())
                        .into(profilePic);
            }
        }

    }

    private void initializeMyProfileHeader() {
        header = getLayoutInflater().inflate(R.layout.feed_item_header_comments_3,
                null);
        delete = (ImageButton) header.findViewById(R.id.btnDeletePost);
        delete.setVisibility(View.INVISIBLE);

        upVote = (ImageButton) header.findViewById(R.id.btnUpVote);
        downVote = (ImageButton) header.findViewById(R.id.btnDownVote);

        upVote.setVisibility(View.GONE);
        downVote.setVisibility(View.GONE);

    }

    private void initializeDefaultHeader() {
        // za my local posts
        header = getLayoutInflater().inflate(R.layout.feed_item_header_comments_4,
                null);
        thisMyLocalPost = true;
        location = (TextView) header.findViewById(R.id.postPlace);
        delete = (ImageButton) header.findViewById(R.id.btnDelete);
        delete.setVisibility(View.GONE);
        location.setText(commentData.getPlace());
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void getPersonalPost() {
        final PersonalPost pp = new PersonalPost();
        api.getPostPersonal(token, postId, new Callback<PersonalPost>() {
            @Override
            public void success(PersonalPost personalPost, Response response) {
                pp.setId(personalPost.getId());
                pp.setUser_id(personalPost.getUser_id());
                pp.setComments_number(personalPost.getComments_number());
                pp.setImage(personalPost.getImage());
                pp.setStatus(personalPost.getStatus());
                pp.setUsername(personalPost.getUsername());
                pp.setTimeStamp(personalPost.getTimeStamp());
                pp.setUp_votes(personalPost.getUp_votes());
                pp.setProfile_pic_url(personalPost.getProfile_pic_url());

                handlePersonalNotf(pp);
            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getPostPersonal(token, postId, new Callback<PersonalPost>() {
                                            @Override
                                            public void success(PersonalPost personalPost, Response response) {
                                                pp.setId(personalPost.getId());
                                                pp.setUser_id(personalPost.getUser_id());
                                                pp.setComments_number(personalPost.getComments_number());
                                                pp.setImage(personalPost.getImage());
                                                pp.setStatus(personalPost.getStatus());
                                                pp.setUsername(personalPost.getUsername());
                                                pp.setTimeStamp(personalPost.getTimeStamp());
                                                pp.setUp_votes(personalPost.getUp_votes());
                                                pp.setProfile_pic_url(personalPost.getProfile_pic_url());

                                                handlePersonalNotf(pp);
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getPostPersonal(token, postId, new Callback<PersonalPost>() {
                                            @Override
                                            public void success(PersonalPost personalPost, Response response) {
                                                pp.setId(personalPost.getId());
                                                pp.setUser_id(personalPost.getUser_id());
                                                pp.setComments_number(personalPost.getComments_number());
                                                pp.setImage(personalPost.getImage());
                                                pp.setStatus(personalPost.getStatus());
                                                pp.setUsername(personalPost.getUsername());
                                                pp.setTimeStamp(personalPost.getTimeStamp());
                                                pp.setUp_votes(personalPost.getUp_votes());
                                                pp.setProfile_pic_url(personalPost.getProfile_pic_url());

                                                handlePersonalNotf(pp);
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }
        });
    }

    private void getLocalPost() {
        final LocalPost lp = new LocalPost();
        api.getPostLocal(token, postId, new Callback<LocalPost>() {
            @Override
            public void success(LocalPost localPost, Response response) {
                lp.setId(localPost.getId());
                lp.setImage(localPost.getImage());
                lp.setUsername(localPost.getUsername());
                lp.setUser_id(localPost.getUser_id());
                lp.setComments_number(localPost.getComments_number());
                lp.setStatus(localPost.getStatus());
                lp.setVotes(localPost.getVotes());
                lp.setProfile_pic_url(localPost.getProfile_pic_url());
                lp.setTimestamp(localPost.getTimestamp());

                handleLocalNotf(lp);

            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getPostLocal(token, postId, new Callback<LocalPost>() {
                                            @Override
                                            public void success(LocalPost localPost, Response response) {
                                                lp.setId(localPost.getId());
                                                lp.setImage(localPost.getImage());
                                                lp.setUsername(localPost.getUsername());
                                                lp.setUser_id(localPost.getUser_id());
                                                lp.setComments_number(localPost.getComments_number());
                                                lp.setStatus(localPost.getStatus());
                                                lp.setVotes(localPost.getVotes());
                                                lp.setProfile_pic_url(localPost.getProfile_pic_url());
                                                lp.setTimestamp(localPost.getTimestamp());

                                                handleLocalNotf(lp);
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getPostLocal(token, postId, new Callback<LocalPost>() {
                                            @Override
                                            public void success(LocalPost localPost, Response response) {
                                                lp.setId(localPost.getId());
                                                lp.setImage(localPost.getImage());
                                                lp.setUsername(localPost.getUsername());
                                                lp.setUser_id(localPost.getUser_id());
                                                lp.setComments_number(localPost.getComments_number());
                                                lp.setStatus(localPost.getStatus());
                                                lp.setVotes(localPost.getVotes());
                                                lp.setProfile_pic_url(localPost.getProfile_pic_url());
                                                lp.setTimestamp(localPost.getTimestamp());

                                                handleLocalNotf(lp);
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }
        });
    }

    private void handlePersonalNotf(final PersonalPost post) {
        header = getLayoutInflater().inflate(
                R.layout.feed_item_header_comments_1, null);

        userId = post.getUser_id();
        if (myUserId.equals(userId)) {
            // za handle drop menu na my persona posts
            identifier = true;
        }
        seenCommentsFromPush(userId);
        profilePic = (ImageView) header.findViewById(R.id.profilePic);
        username = (Button) header.findViewById(R.id.btnUsername);
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userid", userId);
                context.startActivity(intent);
            }
        });
        if (post.getUser_id().equals(myUserId)) {
            username.setClickable(false);
        }
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);
        upVote = (ImageButton) header.findViewById(R.id.btnUpVote);
        downVote = (ImageButton) header.findViewById(R.id.btnDownVote);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);

        username.setText(post.getUsername());
        upVote.setVisibility(View.GONE);
        downVote.setVisibility(View.GONE);
        dropMenu.setVisibility(View.GONE);

        if (post.getProfile_pic_url() == null) {
            profilePic.setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (post.getProfile_pic_url().contains("gatherspot")) {
                Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                picasso.load(post.getProfile_pic_url())
                        .into(profilePic);
            } else {
                Picasso.with(context).load(post.getProfile_pic_url())
                        .into(profilePic);
            }

        }

        loadMore = (Button) header.findViewById(R.id.btnMoreComments);
        status = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
        timeStamp = (TextView) header.findViewById(R.id.timeStamp);
        voteValue = (TextView) header.findViewById(R.id.voteValue);
        feedImage = (ImageView) header.findViewById(R.id.feedImage1);
        progressBar = (ProgressBar) header.findViewById(R.id.progressBar1);
        comment = (Button) header.findViewById(R.id.btnComment);
        comment.setVisibility(View.INVISIBLE);

        status.setText(post.getStatus());
        timeStamp.setText(post.getTimeStamp() + " ago");
        voteValue.setText(post.getUp_votes());

        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getComments();
            }
        });
        feedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);

                // notifikacije

                intent.putExtra("URLimage", post.getImage());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        if (post.getImage() == null) {
            feedImage.setImageDrawable(null);
            feedImage.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            Picasso picasso = InitHttpClient.getInstance().getImageLoader();
            picasso.load(post.getImage())
                    .placeholder(R.drawable.grey_image)
                    .into(feedImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        setView();
    }

    private void seenCommentsFromPush(String userId) {
        api.seenComments(token, postId, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.seenComments(token, postId, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }
        });
    }


    private void handleLocalNotf(final LocalPost post) {
        header = getLayoutInflater().inflate(R.layout.feed_item_header_comments_2,
                null);

        userId = post.getUser_id();
        seenCommentsFromPush(userId);
        profilePic = (ImageView) header.findViewById(R.id.profilePic);
        username = (Button) header.findViewById(R.id.btnUsername);
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userid", userId);
                context.startActivity(intent);
            }
        });
        if (post.getUser_id().equals(myUserId)) {
            username.setClickable(false);
        }
        distance = (TextView) header.findViewById(R.id.distance);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);
        upVote = (ImageButton) header.findViewById(R.id.btnUpVote);
        downVote = (ImageButton) header.findViewById(R.id.btnDownVote);
        dropMenu = (ImageButton) header.findViewById(R.id.dropMenu);

        ImageView iconLoc = (ImageView) header.findViewById(R.id.iconlocation);
        iconLoc.setVisibility(View.INVISIBLE);

        upVote.setVisibility(View.GONE);
        downVote.setVisibility(View.GONE);
        dropMenu.setVisibility(View.GONE);
        username.setText(post.getUsername());
        distance.setVisibility(View.INVISIBLE);
        if (post.getProfile_pic_url() == null) {
            profilePic.setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (post.getProfile_pic_url().contains("gatherspot")) {
                Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                picasso.load(post.getProfile_pic_url())
                        .into(profilePic);
            } else {
                Picasso.with(context).load(post.getProfile_pic_url())
                        .into(profilePic);
            }
        }
        loadMore = (Button) header.findViewById(R.id.btnMoreComments);
        status = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
        timeStamp = (TextView) header.findViewById(R.id.timeStamp);
        voteValue = (TextView) header.findViewById(R.id.voteValue);
        feedImage = (ImageView) header.findViewById(R.id.feedImage1);
        progressBar = (ProgressBar) header.findViewById(R.id.progressBar1);
        comment = (Button) header.findViewById(R.id.btnComment);
        comment.setVisibility(View.INVISIBLE);

        status.setText(post.getStatus());
        timeStamp.setText(post.getTimestamp() + " ago");
        voteValue.setText(post.getVotes());

        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getComments();
            }
        });
        feedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);
                intent.putExtra("URLimage", post.getImage());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        if (post.getImage() == null) {
            feedImage.setImageDrawable(null);
            feedImage.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            Picasso picasso = InitHttpClient.getInstance().getImageLoader();
            picasso.load(post.getImage())
                    .placeholder(R.drawable.grey_image)
                    .into(feedImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        setView();
    }

    private void setView() {
        lv1 = (ListView) findViewById(R.id.list_comments);
        lv1.addHeaderView(header);

        getComments();

        sendComment.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (sendComment.getProgress() == 0) {
                            sendComment.setProgress(50);
                        } else if (sendComment.getProgress() == 100) {
                            sendComment.setProgress(0);
                        } else {
                            sendComment.setProgress(100);
                        }
                        if (postType == LOCAL || postType == MY_LOCAL) {
                            if (!(newComment.getText().toString()).equals("")) {
                                sendComment.setClickable(false);
                                api.newLocalComment(token, postId,
                                        newComment.getText().toString(), new Callback<NewComment>() {

                                            @Override
                                            public void success(NewComment arg0, Response response) {
                                                sendComment.setClickable(true);
                                                updateNewCommentandSetView(arg0.getId());
                                            }

                                            @Override
                                            public void failure(RetrofitError arg0) {
                                                handleBan(arg0);
                                                Response r = arg0.getResponse();
                                                if (r != null && r.getStatus() == 401) {
                                                    if (loginType.equals("facebook")) {
                                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.newLocalComment(token, postId,
                                                                                newComment.getText().toString(), new Callback<NewComment>() {
                                                                                    @Override
                                                                                    public void success(NewComment arg0, Response response) {
                                                                                        sendComment.setClickable(true);
                                                                                        updateNewCommentandSetView(arg0.getId());
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        sendComment.setClickable(true);
                                                                                        sendComment.setProgress(0);
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        sendComment.setClickable(true);
                                                                        sendComment.setProgress(0);
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.newLocalComment(token, postId,
                                                                                newComment.getText().toString(), new Callback<NewComment>() {
                                                                                    @Override
                                                                                    public void success(NewComment arg0, Response response) {
                                                                                        sendComment.setClickable(true);
                                                                                        updateNewCommentandSetView(arg0.getId());
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        sendComment.setClickable(true);
                                                                                        sendComment.setProgress(0);
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        sendComment.setClickable(true);
                                                                        sendComment.setProgress(0);
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }
                                                } else {
                                                    sendComment.setClickable(true);
                                                    sendComment.setProgress(0);
                                                }
                                            }
                                        });
                            } else {
                                sendComment.setProgress(0);
                            }

                        } else {
                            if (!(newComment.getText().toString()).equals("")) {
                                sendComment.setClickable(false);
                                api.newPersonalComment(token, postId,
                                        newComment.getText().toString(), new Callback<NewComment>() {
                                            @Override
                                            public void success(NewComment arg0, Response response) {
                                                sendComment.setClickable(true);
                                                updateNewCommentandSetView(arg0.getId());
                                            }

                                            @Override
                                            public void failure(RetrofitError arg0) {
                                                handleBan(arg0);
                                                Response r = arg0.getResponse();
                                                if (r != null && r.getStatus() == 401) {
                                                    if (loginType.equals("facebook")) {
                                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.newPersonalComment(token, postId,
                                                                                newComment.getText().toString(), new Callback<NewComment>() {
                                                                                    @Override
                                                                                    public void success(NewComment arg0, Response response) {
                                                                                        sendComment.setClickable(true);
                                                                                        updateNewCommentandSetView(arg0.getId());
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        sendComment.setClickable(true);
                                                                                        sendComment.setProgress(0);
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        sendComment.setClickable(true);
                                                                        sendComment.setProgress(0);
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.newPersonalComment(token, postId,
                                                                                newComment.getText().toString(), new Callback<NewComment>() {
                                                                                    @Override
                                                                                    public void success(NewComment arg0, Response response) {
                                                                                        sendComment.setClickable(true);
                                                                                        updateNewCommentandSetView(arg0.getId());
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        sendComment.setClickable(true);
                                                                                        sendComment.setProgress(0);
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        sendComment.setClickable(true);
                                                                        sendComment.setProgress(0);
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }
                                                } else {
                                                    sendComment.setClickable(true);
                                                    sendComment.setProgress(0);
                                                }
                                            }
                                        });
                            } else {
                                sendComment.setProgress(0);
                            }

                        }
                    }
                }

        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comments_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.refresh:
                super.recreate();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivityComments.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (!(event.getAction() == MotionEvent.ACTION_UP) && !(event.getAction() == MotionEvent.ACTION_DOWN) && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {

                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //listOfComments = null;
        if (background.getBackground() != null) {
            background.getBackground().setCallback(null);
        }
        commentData = null;
        if (image != null) {
            image.setCallback(null);
            //image.getBitmap().recycle();
            image = null;
        }
        System.gc();

    }
}
