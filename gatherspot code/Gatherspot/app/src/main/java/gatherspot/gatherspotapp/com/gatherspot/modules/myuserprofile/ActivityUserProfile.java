package gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile;

/**
 * Created by Josip on 29.5.2015..
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterUserProfile;
import gatherspot.gatherspotapp.com.gatherspot.pojo.Feed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UserInfoItem;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityUserProfile extends AppCompatActivity {

    String user_id, token;
    ListView lv1;
    View headerView;
    ProgressBar circle;
    ImageView headerPic;
    TextView username;
    ExpandableTextView mpInfo1;
    String loginType;
    SharedPreferences myTokenPrefs, resume, loginTypePrefs, gcmPrefs, backgroundImagePrefs;
    SharedPreferences.Editor edtToken;
    boolean yesNoResume, yesNoResumeComments;
    List<PersonalPostItem> listPersonal;
    SharedPreferences.Editor edt;

    BitmapDrawable image;

    RelativeLayout background;

    Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.myprofile_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));

        Intent intent = getIntent();
        user_id = intent.getStringExtra("userid");

        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");
        edtToken = myTokenPrefs.edit();
        backgroundImagePrefs = getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
        gcmPrefs = getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        lv1 = (ListView) findViewById(R.id.list);
        circle = (ProgressBar) findViewById(R.id.progressBarMyProfile);

        headerView = getLayoutInflater().from(this).inflate(R.layout.feed_item_header_mp,
                null);

        headerPic = (ImageView) headerView.findViewById(R.id.profilePic);
        username = (TextView) headerView.findViewById(R.id.username);
        mpInfo1 = (ExpandableTextView) headerView.findViewById(R.id.expand_text_view);
        background = (RelativeLayout) findViewById(R.id.background);

        picasso = InitHttpClient.getInstance().getImageLoader();

        if (BackgroundImageHandler.fileExistance(this)) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.45f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(getResources(),
                    BackgroundImageHandler.loadImageFromStorage(this));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
        }
    }


    private void requestUserInfo() {

        final ApiInterface api = InitHttpClient.getInstance().getApiInterface();

        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                if (arg0.getProfilePic() == null) {

                                                    headerPic
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                } else {
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        picasso.load(arg0.getProfilePic()).into(headerPic);
                                                    } else {
                                                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());
                                                mpInfo1.setText(arg0.getAbout());
                                                // lv1.removeViewInLayout(headerView);
                                                if (lv1.getAdapter() == null) {
                                                    lv1.addHeaderView(headerView);
                                                } else {

                                                }
                                                requestData();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                if (arg0.getProfilePic() == null) {

                                                    headerPic
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                } else {
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        picasso.load(arg0.getProfilePic()).into(headerPic);
                                                    } else {
                                                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());
                                                mpInfo1.setText(arg0.getAbout());
                                                // lv1.removeViewInLayout(headerView);
                                                if (lv1.getAdapter() == null) {
                                                    lv1.addHeaderView(headerView);
                                                } else {

                                                }
                                                requestData();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }

            @Override
            public void success(UserInfoItem arg0, Response arg1) {

                if (arg0.getProfilePic() == null) {

                    headerPic
                            .setImageResource(R.mipmap.ic_action_icon_person);
                } else {
                    if (arg0.getProfilePic().contains("gatherspot")) {
                        picasso.load(arg0.getProfilePic()).into(headerPic);
                    } else {
                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                .into(headerPic);
                    }
                }
                username.setText(arg0.getUsername());
                mpInfo1.setText(arg0.getAbout());
                // lv1.removeViewInLayout(headerView);
                if (lv1.getAdapter() == null) {
                    lv1.addHeaderView(headerView);
                } else {

                }
                requestData();

            }
        });
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        resume = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        yesNoResume = resume.getBoolean("yesNo", true);
        if (yesNoResume) {
            requestUserInfo();
        } else {
            edt = resume.edit();
            edt.putBoolean("yesNo", true);
            edt.commit();
        }
    }

    private void requestData() {

        final ApiInterface api = InitHttpClient.getInstance().getApiInterface();

        api.getUsersPersonal(token, user_id, new Callback<Feed>() {

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUsersPersonal(token, user_id, new Callback<Feed>() {
                                            @Override
                                            public void success(Feed arg0, Response response) {
                                                listPersonal = new ArrayList<PersonalPostItem>();
                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                    PersonalPostItem personalPost = new PersonalPostItem();
                                                    personalPost.setStatus(item.getStatus());
                                                    personalPost.setTimeStamp(item.getTimeStamp());
                                                    personalPost.setComments_number(item.getComments_number());
                                                    personalPost.setUp_votes(item.getUp_votes());
                                                    personalPost.setUpvoted(item.getUpvoted());
                                                    personalPost.setDownvoted(item.getDownvoted());
                                                    personalPost.setUser_id(item.getUser_id());
                                                    personalPost.setId(item.getId());
                                                    if (item.getImage() == null) {
                                                        personalPost.setImage("");
                                                    } else {
                                                        personalPost.setImage(item.getImage());
                                                    }
                                                    listPersonal.add(personalPost);
                                                }
                                                setList();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUsersPersonal(token, user_id, new Callback<Feed>() {
                                            @Override
                                            public void success(Feed arg0, Response response) {
                                                listPersonal = new ArrayList<PersonalPostItem>();
                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                    PersonalPostItem personalPost = new PersonalPostItem();
                                                    personalPost.setStatus(item.getStatus());
                                                    personalPost.setTimeStamp(item.getTimeStamp());
                                                    personalPost.setComments_number(item.getComments_number());
                                                    personalPost.setUp_votes(item.getUp_votes());
                                                    personalPost.setUpvoted(item.getUpvoted());
                                                    personalPost.setDownvoted(item.getDownvoted());
                                                    personalPost.setUser_id(item.getUser_id());
                                                    personalPost.setId(item.getId());
                                                    if (item.getImage() == null) {
                                                        personalPost.setImage("");
                                                    } else {
                                                        personalPost.setImage(item.getImage());
                                                    }
                                                    listPersonal.add(personalPost);
                                                }
                                                setList();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }

            }

            @Override
            public void success(Feed arg0, Response arg1) {
                listPersonal = new ArrayList<PersonalPostItem>();
                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                    PersonalPostItem personalPost = new PersonalPostItem();
                    personalPost.setStatus(item.getStatus());
                    personalPost.setTimeStamp(item.getTimeStamp());
                    personalPost.setComments_number(item.getComments_number());
                    personalPost.setUp_votes(item.getUp_votes());
                    personalPost.setUpvoted(item.getUpvoted());
                    personalPost.setDownvoted(item.getDownvoted());
                    personalPost.setUser_id(item.getUser_id());
                    personalPost.setId(item.getId());
                    if (item.getImage() == null) {
                        personalPost.setImage("");
                    } else {
                        personalPost.setImage(item.getImage());
                    }
                    listPersonal.add(personalPost);
                }
                setList();

            }
        });
    }

    private void setList() {

        yesNoResumeComments = resume.getBoolean("yesNoComments", true);
        if (yesNoResumeComments == false) {
            // refill
            if (lv1.getAdapter() == null) {
                ListAdapterUserProfile listAdapter = new ListAdapterUserProfile(this, listPersonal,
                        token);
                lv1.setAdapter(listAdapter);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            } else {
                ((ListAdapterUserProfile) ((HeaderViewListAdapter) lv1.getAdapter())
                        .getWrappedAdapter()).refill(listPersonal);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            }
        } else {
            ListAdapterUserProfile listAdapter = new ListAdapterUserProfile(this, listPersonal, token);
            lv1.setAdapter(listAdapter);
        }
        circle.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.mp_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivityUserProfile.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listPersonal = null;
        if (background.getBackground() != null) {
            background.getBackground().setCallback(null);
        }
        if (image != null) {
            image.setCallback(null);
            //image.getBitmap().recycle();
            image = null;
        }
        System.gc();

    }
}
