package gatherspot.gatherspotapp.com.gatherspot.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Josip on 28.8.2015..
 */
public class BackgroundImageHandler {

    public static Bitmap blur(Bitmap bitmap, Context context) {
        if (Build.VERSION.SDK_INT >= 17) {
            Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            RenderScript rs = RenderScript.create(context);
            android.renderscript.ScriptIntrinsicBlur intrinsicBlur =
                    android.renderscript.ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, bitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outBitmap);
            intrinsicBlur.setRadius(1.5f);
            intrinsicBlur.setInput(tmpIn);
            intrinsicBlur.forEach(tmpOut);
            tmpOut.copyTo(outBitmap);
            rs.destroy();
            outBitmap = changeBitmapContrastBrightness(outBitmap, 0.5f, 110.f);
            return outBitmap;

        } else {
            android.support.v8.renderscript.RenderScript rs = android.support.v8.renderscript.RenderScript.create(context);
            final android.support.v8.renderscript.Allocation input = android.support.v8.renderscript.Allocation.createFromBitmap(rs, bitmap,
                    android.support.v8.renderscript.Allocation.MipmapControl.MIPMAP_NONE, android.support.v8.renderscript.Allocation.USAGE_SCRIPT);
            final android.support.v8.renderscript.Allocation output =
                    android.support.v8.renderscript.Allocation.createTyped(rs, input.getType());
            final android.support.v8.renderscript.ScriptIntrinsicBlur script =
                    android.support.v8.renderscript.ScriptIntrinsicBlur.create(rs, android.support.v8.renderscript.Element.U8_4(rs));
            script.setRadius(1.5f);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(bitmap);
            rs.destroy();
            bitmap = changeBitmapContrastBrightness(bitmap, 0.5f, 110.f);
            return bitmap;
        }

    }

    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }

    public static void setBackground(RelativeLayout background,
                                     Context context) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0.45f);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        BitmapDrawable image = new BitmapDrawable(context.getResources(),
                BackgroundImageHandler.loadImageFromStorage(context));
        image.setColorFilter(filter);
        if (Build.VERSION.SDK_INT > 15) {
            background.setBackground(image);
        } else {
            background.setBackgroundDrawable(image);
        }
    }

    public static void saveToInternalStorage(Bitmap bitmapImage, Context context) {

        /*
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        */
        File mypath = new File(context.getFilesDir(), "bi.jpg");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(Context context) {
        SharedPreferences backgroundImagePrefs =
                context.getSharedPreferences("IMAGE", Context.MODE_PRIVATE);
        File mypath = new File(context.getFilesDir(), "bi.jpg");

    }

    public static Bitmap loadImageFromStorage(Context context) {

        try {
            File f = new File(context.getFilesDir(), "bi.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean fileExistance(Context context) {
        File file = new File(context.getApplicationContext().getFilesDir(), "bi.jpg");
        return file.exists();
    }

}
