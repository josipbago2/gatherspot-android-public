package gatherspot.gatherspotapp.com.gatherspot.utils;

import android.content.Context;
import android.net.Uri;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import gatherspot.gatherspotapp.com.gatherspot.R;
import retrofit.RestAdapter;

/**
 * Created by Josip on 18.8.2015..
 */
public class InitHttpClient {

    private static InitHttpClient instanceHttpClient;

    private static Picasso imageLoader;
    private static Picasso imageLoaderNoCert;
    private static ApiInterface api;

    public static final String ENDPOINT = "https://nodejs-mamic.rhcloud.com";

    public static void initInstance(Context context) {
        if (instanceHttpClient == null) {
            instanceHttpClient = new InitHttpClient();
            instanceHttpClient.initialize(context);
        }
    }

    private InitHttpClient() {

    }

    public ApiInterface getApiInterface() {
        return this.api;
    }

    public Picasso getImageLoader() {
        return this.imageLoader;
    }

    public Picasso getImageLoaderNoCert() {
        return this.imageLoaderNoCert;
    }

    public static InitHttpClient getInstance() {
        return instanceHttpClient;
    }


    private void initialize(Context context) {
        OkHttpClient client = trustcert(context);
        this.api = setRestAdapter(client);
        this.imageLoader = new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(client))
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        exception.printStackTrace();
                    }
                }).build();
        this.imageLoaderNoCert = new Picasso.Builder(context)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        exception.printStackTrace();
                    }
                }).build();
    }


    public static ApiInterface setRestAdapter(OkHttpClient client) {
        ApiInterface apiInterface;
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ENDPOINT).build();
        apiInterface = adapter.create(ApiInterface.class);

        return apiInterface;
    }

    public static OkHttpClient trustcert(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient();
        try {
            KeyStore ksTrust = KeyStore.getInstance("BKS");
            InputStream instream = context.getResources().openRawResource(R.raw.mykeystore);
            ksTrust.load(instream, "secret".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ksTrust);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
            okHttpClient.setHostnameVerifier(new NullHostnameVerifier());
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | KeyManagementException e) {
            e.printStackTrace();
        }

        return okHttpClient;
    }


    public static class NullHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
