package gatherspot.gatherspotapp.com.gatherspot.listadapters;

/**
 * Created by Josip on 29.5.2015..
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.modules.shared.ActivityComments;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile.ActivityUserProfile;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentData;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UpDownVoteResponse;
import gatherspot.gatherspotapp.com.gatherspot.utils.FullScreenImageActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListAdapterLiveFeed extends BaseAdapter implements OnClickListener {

    private List<PostItem> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    String loginType;
    SharedPreferences myTokenPrefs, resume, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edt, edtToken;

    private final SparseBooleanArray mCollapsedStatus;

    private static final int PERSONAL = 0;
    private static final int ADS = 1;
    private static final int LOCAL = 2;
    private static final int CIRCLE = 3;

    private String token, user_id;

    ImageView imageToFB;
    TextView statusToFB;

    Picasso picasso;

    ApiInterface api;

    public ListAdapterLiveFeed(Context context, List<PostItem> liveFeedList) {
        this.listData = liveFeedList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

        mCollapsedStatus = new SparseBooleanArray();

        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        resume = context.getSharedPreferences("RESUMEHANDLER", Context.MODE_PRIVATE);
        myTokenPrefs = context.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        user_id = myTokenPrefs.getString("myuserid", "");

        api = InitHttpClient.getInstance().getApiInterface();

        picasso = InitHttpClient.getInstance().getImageLoader();

    }

    public void removeAll() {
        listData.clear();
        this.notifyDataSetChanged();
    }

    public void addMore(List<PostItem> addList) {
        listData.addAll(addList);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listData.size();
    }

    @Override
    public PostItem getItem(int position) {
        // TODO Auto-generated method stub
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        // return super.getItemViewType(position);
        String type = (getItem(position)).getType();
        if (type.equalsIgnoreCase("personal")) {
            return PERSONAL;
        } else if (type.equalsIgnoreCase("local")) {
            return LOCAL;
        } else if (type.equalsIgnoreCase("circle")) {
            return CIRCLE;
        } else {
            return ADS;
        }
    }

    @Override
    public int getViewTypeCount() {

        if (getCount() != 0)
            return getCount();

        return 1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        String postType = null;
        final ViewHolder holder;
        boolean clickPreventer = false;

        if (layoutInflater == null)
            layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            holder = new ViewHolder();

            switch (getItemViewType(position)) {
                case PERSONAL:
                    convertView = layoutInflater.inflate(
                            R.layout.feed_item_lf_personal, null);
                    postType = "personal";

                    break;
                case ADS:
                    convertView = layoutInflater.inflate(
                            R.layout.feed_item_lf_localads, null);
                    postType = "ads";
                    holder.distance = (TextView) convertView
                            .findViewById(R.id.distance);
                    holder.parentLayout = (LinearLayout) convertView
                            .findViewById(R.id.parentLayout);

                    break;
                case LOCAL:
                    convertView = layoutInflater.inflate(
                            R.layout.feed_item_lf_local, null);
                    postType = "local";
                    holder.distance = (TextView) convertView
                            .findViewById(R.id.distance);

                    break;
                case CIRCLE:
                    convertView = layoutInflater.inflate(R.layout.feed_item_lf_circlescroll, null);
                    postType = "circle";
                default:
                    postType = "";
            }
            holder.feedImage = (ImageView) convertView
                    .findViewById(R.id.feedImage1);
            holder.profilePic = (ImageView) convertView
                    .findViewById(R.id.profilePic);
            holder.post = (RelativeLayout) convertView.findViewById(R.id.post);
            holder.status = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);
            holder.comments = (Button) convertView.findViewById(R.id.btnComment);
            holder.username = (Button) convertView.findViewById(R.id.btnUsername);

            holder.progressBar = (ProgressBar) convertView
                    .findViewById(R.id.progressBar1);
            holder.downVote = (ImageButton) convertView
                    .findViewById(R.id.btnDownVote);
            holder.upVote = (ImageButton) convertView.findViewById(R.id.btnUpVote);
            holder.timeStamp = (TextView) convertView.findViewById(R.id.timeStamp);
            holder.voteValue = (TextView) convertView.findViewById(R.id.voteValue);
            holder.dropMenu = (ImageButton) convertView.findViewById(R.id.dropMenu);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }
        if (getItemViewType(position) == CIRCLE) {
            return convertView;
        }

        holder.status.setText((listData.get(position)).getStatus(), mCollapsedStatus, position);

        final String idPost = (listData.get(position)).getId();

        if (getItemViewType(position) == ADS
                || getItemViewType(position) == LOCAL) {
            String distance = (listData.get(position)).getRadius();
            if (distance != null) {
                String[] arrDist = distance.split("\\.");
                if (myTokenPrefs.getBoolean("imperialunits", true)) {
                    holder.distance.setText(arrDist[0] + "yd");
                } else {
                    holder.distance.setText(arrDist[0] + "m");
                }
            }
        }

        // LOCAL ADS MANUAL!!!! MANUAL!!!

        if ((listData.get(position)).getImage().isEmpty()) {

            holder.progressBar.setVisibility(View.GONE);
            holder.feedImage.setImageDrawable(null);
            holder.feedImage.setVisibility(View.GONE);
        } else if ((listData.get(position)).getImage().equals("adsimage1")) {
            clickPreventer = true;
            //holder.feedImage.setImageResource(R.drawable.localadmock3);
        } else if ((listData.get(position)).getImage().equals("emptypersonal")) {
            // usput i micanje footera sa komentarima
            RelativeLayout footer = (RelativeLayout) convertView
                    .findViewById(R.id.footer);
            clickPreventer = true;
            footer.setVisibility(View.GONE);
            holder.feedImage.setPadding(0, 0, 0, 30);

            holder.feedImage.setImageResource(R.drawable.personalmock3);

        } else if ((listData.get(position)).getImage().equals("emptylocal")) {
            // usput i micanje footera sa komentarima
            RelativeLayout footer = (RelativeLayout) convertView
                    .findViewById(R.id.footer);
            clickPreventer = true;
            footer.setVisibility(View.GONE);
            holder.feedImage.setPadding(0, 0, 0, 30);

            holder.feedImage.setImageResource(R.drawable.localmock3);

        } else if ((listData.get(position)).getImage().equals("adsimage2")) {
            //holder.feedImage.setImageResource(R.drawable.college_bar);
            clickPreventer = true;
        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.feedImage.setVisibility(View.VISIBLE);
            picasso.load((listData.get(position)).getImage())
                    .placeholder(R.drawable.grey_image)
                    .into(holder.feedImage,
                            new com.squareup.picasso.Callback() {

                                @Override
                                public void onError() {

                                }

                                @Override
                                public void onSuccess() {
                                    // TODO Auto-generated method stub
                                    holder.progressBar.setVisibility(View.GONE);
                                }

                            });
        }


        holder.feedImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                edt = resume.edit();
                edt.putBoolean("resume?", true);
                edt.commit();
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);
                intent.putExtra("URLimage", (listData.get(position).getImage()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        // LOCAL ADS MANUAL!!!!

        if ((listData.get(position)).getProfilePic().isEmpty()
                || listData.get(position).getProfilePic() == null) {
            holder.profilePic
                    .setImageResource(R.mipmap.ic_action_icon_person);
        } else if ((listData.get(position)).getProfilePic()
                .equals("adsprofile")) {
            holder.profilePic.setImageResource(R.mipmap.logo_gatherspot);
        } else {
            if (listData.get(position).getProfilePic().contains("gatherspot")) {
                picasso.load((listData.get(position)).getProfilePic())
                        .into(holder.profilePic);
            } else {
                InitHttpClient.getInstance().getImageLoaderNoCert()
                        .load(listData.get(position).getProfilePic())
                        .into(holder.profilePic);
            }

        }
        holder.profilePic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                edt = resume.edit();
                edt.putBoolean("resume?", true);
                edt.commit();

                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.putExtra("userid", (listData.get(position)).getUser_id());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.timeStamp.setText((listData.get(position)).getTimeStamp()
                + " ago");

        holder.downVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (getItemViewType(position) == LOCAL) {
                    api.downVoteLocal(token, idPost,
                            new Callback<UpDownVoteResponse>() {

                                @Override
                                public void failure(RetrofitError arg0) {
                                    handleBan(arg0);
                                    Response r = arg0.getResponse();
                                    if (r != null && r.getStatus() == 401) {
                                        if (loginType.equals("facebook")) {
                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.downVoteLocal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "down")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.upVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.upVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        } else {
                                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.downVoteLocal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "down")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.upVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.upVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        }

                                    }

                                }

                                @Override
                                public void success(UpDownVoteResponse arg0,
                                                    Response arg1) {
                                    if (arg0.getResponse().equalsIgnoreCase(
                                            "down")) {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes--;
                                        (listData.get(position))
                                                .setVotes(String.valueOf(votes));
                                        (listData.get(position))
                                                .setDownvoted("true");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.upVote.setColorFilter(Color
                                                .parseColor("#80000000"));
                                        holder.upVote.setClickable(false);
                                    } else {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes++;
                                        (listData.get(position))
                                                .setVotes(String.valueOf(votes));
                                        (listData.get(position))
                                                .setDownvoted("false");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.upVote.setColorFilter(Color
                                                .parseColor("#00000000"));
                                        holder.upVote.setClickable(true);
                                    }
                                }

                            });
                } else {
                    api.downVotePersonal(token, idPost,
                            new Callback<UpDownVoteResponse>() {

                                @Override
                                public void failure(RetrofitError arg0) {
                                    handleBan(arg0);
                                    Response r = arg0.getResponse();
                                    if (r != null && r.getStatus() == 401) {
                                        if (loginType.equals("facebook")) {
                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.downVotePersonal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "down")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.upVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.upVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        } else {
                                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.downVotePersonal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "down")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.upVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setDownvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.upVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.upVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        }
                                    }

                                }

                                @Override
                                public void success(UpDownVoteResponse arg0,
                                                    Response arg1) {
                                    if (arg0.getResponse().equalsIgnoreCase(
                                            "down")) {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes--;
                                        (listData.get(position))
                                                .setUp_votes(String
                                                        .valueOf(votes));
                                        (listData.get(position))
                                                .setDownvoted("true");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.upVote.setColorFilter(Color
                                                .parseColor("#80000000"));
                                        holder.upVote.setClickable(false);
                                    } else {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes++;
                                        (listData.get(position))
                                                .setUp_votes(String
                                                        .valueOf(votes));
                                        (listData.get(position))
                                                .setDownvoted("false");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.upVote.setColorFilter(Color
                                                .parseColor("#00000000"));
                                        holder.upVote.setClickable(true);
                                    }
                                }

                            });
                }

            }
        });

        holder.upVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (getItemViewType(position) == LOCAL) {
                    api.upVoteLocal(token, idPost,
                            new Callback<UpDownVoteResponse>() {

                                @Override
                                public void failure(RetrofitError arg0) {
                                    handleBan(arg0);
                                    Response r = arg0.getResponse();
                                    if (r != null && r.getStatus() == 401) {
                                        if (loginType.equals("facebook")) {
                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.upVoteLocal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "up")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.downVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.downVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        } else {
                                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.upVoteLocal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "up")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.downVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setVotes(String.valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.downVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        }
                                    }
                                }

                                @Override
                                public void success(UpDownVoteResponse arg0,
                                                    Response arg1) {
                                    if (arg0.getResponse().equalsIgnoreCase(
                                            "up")) {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes++;
                                        (listData.get(position))
                                                .setVotes(String.valueOf(votes));
                                        (listData.get(position))
                                                .setUpvoted("true");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.downVote.setColorFilter(Color
                                                .parseColor("#80000000"));
                                        holder.downVote.setClickable(false);
                                    } else {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes--;
                                        (listData.get(position))
                                                .setVotes(String.valueOf(votes));
                                        (listData.get(position))
                                                .setUpvoted("false");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.downVote.setColorFilter(Color
                                                .parseColor("#00000000"));
                                        holder.downVote.setClickable(true);
                                    }
                                }

                            });

                } else {
                    api.upVotePersonal(token, idPost,
                            new Callback<UpDownVoteResponse>() {

                                @Override
                                public void failure(RetrofitError arg0) {
                                    handleBan(arg0);
                                    Response r = arg0.getResponse();
                                    if (r != null && r.getStatus() == 401) {
                                        if (loginType.equals("facebook")) {
                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.upVotePersonal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "up")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.downVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.downVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        } else {
                                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                        @Override
                                                        public void success(LoginItemToken arg0, Response response) {
                                                            edtToken.putString("mytoken", arg0.getToken());
                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                            edtToken.commit();
                                                            token = arg0.getToken();
                                                            api.upVotePersonal(token, idPost,
                                                                    new Callback<UpDownVoteResponse>() {
                                                                        @Override
                                                                        public void success(UpDownVoteResponse arg0, Response response) {
                                                                            if (arg0.getResponse().equalsIgnoreCase(
                                                                                    "up")) {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes++;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("true");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#80000000"));
                                                                                holder.downVote.setClickable(false);
                                                                            } else {
                                                                                int votes = Integer
                                                                                        .parseInt(holder.voteValue
                                                                                                .getText().toString());
                                                                                votes--;
                                                                                (listData.get(position))
                                                                                        .setUp_votes(String
                                                                                                .valueOf(votes));
                                                                                (listData.get(position))
                                                                                        .setUpvoted("false");
                                                                                holder.voteValue.setText(String
                                                                                        .valueOf(votes));
                                                                                holder.downVote.setColorFilter(Color
                                                                                        .parseColor("#00000000"));
                                                                                holder.downVote.setClickable(true);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError error) {
                                                            handleBan(error);
                                                        }
                                                    });
                                        }
                                    }

                                }

                                @Override
                                public void success(UpDownVoteResponse arg0,
                                                    Response arg1) {
                                    if (arg0.getResponse().equalsIgnoreCase(
                                            "up")) {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes++;
                                        (listData.get(position))
                                                .setUp_votes(String
                                                        .valueOf(votes));
                                        (listData.get(position))
                                                .setUpvoted("true");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.downVote.setColorFilter(Color
                                                .parseColor("#80000000"));
                                        holder.downVote.setClickable(false);
                                    } else {
                                        int votes = Integer
                                                .parseInt(holder.voteValue
                                                        .getText().toString());
                                        votes--;
                                        (listData.get(position))
                                                .setUp_votes(String
                                                        .valueOf(votes));
                                        (listData.get(position))
                                                .setUpvoted("false");
                                        holder.voteValue.setText(String
                                                .valueOf(votes));
                                        holder.downVote.setColorFilter(Color
                                                .parseColor("#00000000"));
                                        holder.downVote.setClickable(true);
                                    }
                                }
                            });
                }
            }

        });

        if (getItemViewType(position) == LOCAL) {
            holder.voteValue.setText((listData.get(position)).getVotes());
        } else if (getItemViewType(position) == PERSONAL) {
            holder.voteValue.setText((listData.get(position)).getUp_votes());
        } else {
            holder.voteValue.setText("ads votes");
        }

        holder.upvoted = (listData.get(position)).getUpvoted();
        holder.downvoted = (listData.get(position)).getDownvoted();

        if (holder.upvoted.equalsIgnoreCase("true")) {
            holder.downVote.setClickable(false);
            holder.downVote.setColorFilter(Color.parseColor("#80000000"));
        } else {
            holder.downVote.setClickable(true);
            holder.downVote.setColorFilter(Color.parseColor("#00000000"));
        }
        if (holder.downvoted.equalsIgnoreCase("true")) {
            holder.upVote.setClickable(false);
            holder.upVote.setColorFilter(Color.parseColor("#80000000"));
        } else {
            holder.upVote.setClickable(true);
            holder.upVote.setColorFilter(Color.parseColor("#00000000"));
        }

        holder.username.setText((listData.get(position)).getUsername());
        holder.username.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                edt = resume.edit();
                edt.putBoolean("resume?", true);
                edt.commit();

                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.putExtra("userid", (listData.get(position)).getUser_id());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
        if (((listData.get(position)).getUser_id()).equals(user_id)) {
            holder.profilePic.setClickable(false);
            holder.username.setClickable(false);
        }

        holder.post.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                edt = resume.edit();
                edt.putBoolean("resume?", true);
                edt.commit();
                CommentData commentData = new CommentData();
                commentData.setUserId(listData.get(position).getUser_id());
                commentData.setPostId((listData.get(position)).getId());
                commentData.setDistanceSend((listData.get(position))
                        .getRadius());
                commentData.setNumCommentsSend(listData.get(position)
                        .getComments_number());
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setUsernameSend(listData.get(position)
                        .getUsername());
                commentData.setVoteValueSend(holder.voteValue.getText()
                        .toString());
                if (listData.get(position).getProfilePic().isEmpty()) {
                    commentData.setImageNumberProfile("");
                } else {
                    commentData.setImageNumberProfile(listData.get(position)
                            .getProfilePic());
                }
                if ((listData.get(position)).getImage().isEmpty()) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }
                if (holder.upVote.isClickable()
                        && holder.downVote.isClickable()) {
                    commentData.setUpvoted("");
                    commentData.setDownvoted("");
                } else {
                    if (holder.upVote.isClickable()) {
                        commentData.setUpvoted("true");
                    } else {
                        commentData.setUpvoted("");
                    }
                    if (holder.downVote.isClickable()) {
                        commentData.setDownvoted("true");
                    } else {
                        commentData.setDownvoted("");
                    }
                }
                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", getItemViewType(position));
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.comments.setText((listData.get(position)).getComments_number()
                + " Comments");

        holder.comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                edt = resume.edit();
                edt.putBoolean("resume?", true);
                edt.commit();
                CommentData commentData = new CommentData();
                commentData.setUserId(listData.get(position).getUser_id());
                commentData.setPostId((listData.get(position)).getId());
                commentData.setDistanceSend((listData.get(position))
                        .getRadius());
                commentData.setNumCommentsSend(listData.get(position)
                        .getComments_number());
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setUsernameSend(listData.get(position)
                        .getUsername());
                commentData.setVoteValueSend(holder.voteValue.getText()
                        .toString());
                if (listData.get(position).getProfilePic().isEmpty()) {
                    commentData.setImageNumberProfile("");
                } else {
                    commentData.setImageNumberProfile(listData.get(position)
                            .getProfilePic());
                }
                if ((listData.get(position)).getImage().isEmpty()) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }
                if (holder.upVote.isClickable()
                        && holder.downVote.isClickable()) {
                    commentData.setUpvoted("");
                    commentData.setDownvoted("");
                } else {
                    if (holder.upVote.isClickable()) {
                        commentData.setUpvoted("true");
                    } else {
                        commentData.setUpvoted("");
                    }
                    if (holder.downVote.isClickable()) {
                        commentData.setDownvoted("true");
                    } else {
                        commentData.setDownvoted("");
                    }
                }
                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", getItemViewType(position));
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        if (clickPreventer == true) {
            holder.username.setClickable(false);
            holder.username.setOnClickListener(null);
            holder.feedImage.setClickable(false);
            holder.feedImage.setOnClickListener(null);
        }


        holder.dropMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                View drop = holder.dropMenu;
                final ViewGroup root = (ViewGroup) v.getParent();
                PopupMenu popup = new PopupMenu(context, drop);
                if (listData.get(position).getImage().equals("adsimage1")) {
                    popup.getMenuInflater().inflate(R.menu.localad_menu,
                            popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            edtToken.putBoolean("hidelocalad", true);
                            edtToken.commit();
                            holder.parentLayout.setVisibility(View.GONE);
                            return true;
                        }
                    });
                } else {
                    popup.getMenuInflater().inflate(R.menu.dropmenu,
                            popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getTitle().equals("Report this post")) {
                                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                                alertBuilder.setTitle("Report");
                                alertBuilder.setMessage("Please, write us a reason for your report on this post. " +
                                        "If required, we'll do necessary actions as soon as possible.");
                                final EditText message = new EditText(context);
                                alertBuilder.setView(message);
                                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                alertBuilder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (getItemViewType(position) == LOCAL) {
                                            api.reportLocalPost(token, listData.get(position).getId(),
                                                    message.getText().toString(), new Callback<Response>() {
                                                        @Override
                                                        public void success(Response response, Response response2) {
                                                            Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError arg0) {
                                                            handleBan(arg0);
                                                            Response r = arg0.getResponse();
                                                            if (r != null && r.getStatus() == 401) {
                                                                if (loginType.equals("facebook")) {
                                                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                                @Override
                                                                                public void success(LoginItemToken arg0, Response response) {
                                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                                    edtToken.commit();
                                                                                    token = arg0.getToken();
                                                                                    api.reportLocalPost(token, listData.get(position).getId(),
                                                                                            message.getText().toString(), new Callback<Response>() {
                                                                                                @Override
                                                                                                public void success(Response response, Response response2) {
                                                                                                    Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                                                                }

                                                                                                @Override
                                                                                                public void failure(RetrofitError error) {
                                                                                                    handleBan(error);
                                                                                                }
                                                                                            });
                                                                                }

                                                                                @Override
                                                                                public void failure(RetrofitError error) {
                                                                                    handleBan(error);
                                                                                }
                                                                            });
                                                                } else {
                                                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                                @Override
                                                                                public void success(LoginItemToken arg0, Response response) {
                                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                                    edtToken.commit();
                                                                                    token = arg0.getToken();
                                                                                    api.reportLocalPost(token, listData.get(position).getId(),
                                                                                            message.getText().toString(), new Callback<Response>() {
                                                                                                @Override
                                                                                                public void success(Response response, Response response2) {
                                                                                                    Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                                                                }

                                                                                                @Override
                                                                                                public void failure(RetrofitError error) {
                                                                                                    handleBan(error);
                                                                                                }
                                                                                            });
                                                                                }

                                                                                @Override
                                                                                public void failure(RetrofitError error) {
                                                                                    handleBan(error);
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                            //Toast.makeText(context, "Already reported", Toast.LENGTH_SHORT).show();

                                                        }
                                                    });
                                        } else {
                                            api.reportPersonalPost(token, listData.get(position).getId(),
                                                    message.getText().toString(), new Callback<Response>() {
                                                        @Override
                                                        public void success(Response response, Response response2) {
                                                            Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                        }

                                                        @Override
                                                        public void failure(RetrofitError arg0) {
                                                            handleBan(arg0);
                                                            Response r = arg0.getResponse();
                                                            if (r != null && r.getStatus() == 401) {
                                                                if (loginType.equals("facebook")) {
                                                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                                @Override
                                                                                public void success(LoginItemToken arg0, Response response) {
                                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                                    edtToken.commit();
                                                                                    token = arg0.getToken();
                                                                                    api.reportPersonalPost(token, listData.get(position).getId(),
                                                                                            message.getText().toString(), new Callback<Response>() {
                                                                                                @Override
                                                                                                public void success(Response response, Response response2) {
                                                                                                    Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                                                                }

                                                                                                @Override
                                                                                                public void failure(RetrofitError error) {
                                                                                                    handleBan(error);
                                                                                                }
                                                                                            });
                                                                                }

                                                                                @Override
                                                                                public void failure(RetrofitError error) {
                                                                                    handleBan(error);
                                                                                }
                                                                            });
                                                                } else {
                                                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                                @Override
                                                                                public void success(LoginItemToken arg0, Response response) {
                                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                                    edtToken.commit();
                                                                                    token = arg0.getToken();
                                                                                    api.reportPersonalPost(token, listData.get(position).getId(),
                                                                                            message.getText().toString(), new Callback<Response>() {
                                                                                                @Override
                                                                                                public void success(Response response, Response response2) {
                                                                                                    Toast.makeText(context, "Post reported", Toast.LENGTH_SHORT).show();
                                                                                                }

                                                                                                @Override
                                                                                                public void failure(RetrofitError error) {
                                                                                                    handleBan(error);
                                                                                                }
                                                                                            });
                                                                                }

                                                                                @Override
                                                                                public void failure(RetrofitError error) {
                                                                                    handleBan(error);
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                            //Toast.makeText(context, "Already reported", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                        }
                                    }
                                });
                                AlertDialog alertDialog = alertBuilder.create();
                                alertDialog.show();
                            } else {
                            /*
                            LayoutInflater mInflater = (LayoutInflater)
                                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            RelativeLayout rl = new RelativeLayout(context);
                            mInflater.inflate(R.layout.aatoshareonfb, rl, true);
                            TextView trl = (TextView) rl.findViewById(R.id.statusFB);
                            ImageView irl = (ImageView) rl.findViewById(R.id.imageFB);
                            trl.setText(listData.get(position).getStatus());

                            rl.setLayoutParams(new RelativeLayout.LayoutParams(700,
                                    850));
                            String postColour = (getItem(position)).getType();
                            if (postColour.equalsIgnoreCase("personal")) {
                                rl.setBackgroundColor(Color.parseColor("#233E81"));
                            } else if (postColour.equalsIgnoreCase("local")) {
                                rl.setBackgroundColor(Color.parseColor("#500037"));
                            } else {
                                rl.setBackgroundColor(Color.parseColor("#D8712F"));
                            }

                            rl.measure(RelativeLayout.MeasureSpec.makeMeasureSpec(rl.getLayoutParams().width,
                                            RelativeLayout.MeasureSpec.EXACTLY),
                                    RelativeLayout.MeasureSpec.makeMeasureSpec(rl.getLayoutParams().height,
                                            RelativeLayout.MeasureSpec.EXACTLY));

                            rl.layout(0, 0, rl.getMeasuredWidth(), rl.getMeasuredHeight());

                            Bitmap bitmap = Bitmap.createBitmap(rl.getMeasuredWidth(),
                                    rl.getMeasuredHeight(),
                                    Bitmap.Config.ARGB_8888);
                            Canvas c = new Canvas(bitmap);
                            rl.draw(c);
                            ((MainActivity) context).shareOnFBHandler(bitmap);
                            */
                            }
                            return true;
                        }
                    });
                }

                popup.show();
            }
        });

        // za fiksno postavljene postove maknuti dropmenu
        if (listData.get(position).getImage().equals("emptylocal") ||
                listData.get(position).getImage().equals("emptypersonal")) {
            holder.profilePic.setClickable(false);
            holder.post.setClickable(false);
            holder.dropMenu.setVisibility(View.INVISIBLE);
            holder.dropMenu.setClickable(false);
        }

        // sakrij local ad
        if (listData.get(position).getImage().equals("adsimage1")) {
            holder.profilePic.setClickable(false);
            holder.post.setClickable(false);
            if (myTokenPrefs.getBoolean("hidelocalad", false)) {
                holder.parentLayout.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    static class ViewHolder {
        LinearLayout parentLayout;
        RelativeLayout post;
        String id, upvoted, downvoted;
        ProgressBar progressBar;
        ExpandableTextView status;
        ImageView feedImage, profilePic;
        TextView timeStamp, voteValue, distance;
        Button username, comments;
        ImageButton upVote, downVote, dropMenu;
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

}
