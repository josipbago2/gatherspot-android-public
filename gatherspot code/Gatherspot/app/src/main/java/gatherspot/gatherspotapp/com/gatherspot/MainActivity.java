package gatherspot.gatherspotapp.com.gatherspot;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundLocationService;
import gatherspot.gatherspotapp.com.gatherspot.modules.chat.FragmentChat;
import gatherspot.gatherspotapp.com.gatherspot.modules.livefeed.LiveFeedFragment;
import gatherspot.gatherspotapp.com.gatherspot.modules.mylocalposts.FragmentMyLocalPosts;
import gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile.FragmentMyProfile;
import gatherspot.gatherspotapp.com.gatherspot.modules.settings.FragmentHelp;
import gatherspot.gatherspotapp.com.gatherspot.modules.settings.FragmentSettings;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UserInfoItem;
import gatherspot.gatherspotapp.com.gatherspot.pushnotf.QuickstartPreferences;
import gatherspot.gatherspotapp.com.gatherspot.pushnotf.RegistrationIntentService;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Josip on 3.6.2015..
 */
public class MainActivity extends AppCompatActivity {

    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    RestAdapter adapter;
    ApiInterface api;

    String fragmentName;

    ImageView picHeader;
    TextView username;
    public static final String ENDPOINT = "https://service.gatherspot.me";

    String token, user_id, loginType;
    SharedPreferences myTokenPrefs, gcmPrefs, loginTypePrefs;
    SharedPreferences.Editor edt;

    String uname, about;

    Context context;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    public void shareOnFBHandler(Bitmap caption) {
        /*
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(caption)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareDialog.show(this, content);
        */
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        if (resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setTitle("Update Google Play Services");
            alertBuilder.setMessage("You have an outdated version of " +
                    "Google Play Services and the app may not work properly (e.g. you may not get a location fix). Please update your Google Play Services.");
            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(dialogIntent);
                }
            });
            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
        return true;
    }

    public void startBackgroundService() {
        Intent intent = new Intent(MainActivity.this, BackgroundLocationService.class);
        startService(intent);
    }

    public void stopBackgroundService() {
        Intent intent = new Intent(MainActivity.this, BackgroundLocationService.class);
        stopService(intent);
    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast.makeText(this, "You are not connected to the internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    protected void onResume() {
        isDeviceOnline();
        super.onResume();
        //getUserInfo();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    private void handleGcmToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aanewmain); // TODO: A�URIRATI

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));


        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.sdkInitialize(getApplicationContext());
        }

        // za share on FB
        /*
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        */

        api = InitHttpClient.getInstance().getApiInterface();

        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        edt = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        user_id = myTokenPrefs.getString("myuserid", "a");

        gcmPrefs = getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        // ponovo prika�i local ad i upozorenja
        edt.putBoolean("hidelocalad", false);
        edt.putBoolean("showwarning1", true);
        edt.putBoolean("showwarning2", true);
        edt.commit();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    sendGcmTokenToServer();
                } else {

                }
            }
        };

        getUserInfo();

        context = getApplicationContext();

        picHeader = (ImageView) findViewById(R.id.imageView1);
        username = (TextView) findViewById(R.id.textView1);

        mNavItems.add(new NavItem("Live Feed", R.mipmap.feed2));
        mNavItems.add(new NavItem("My Profile", R.mipmap.ic_action_icon_person));
        mNavItems.add(new NavItem("My Local Posts", R.mipmap.ic_action_icon_location));
        mNavItems.add(new NavItem("Chat", R.mipmap.ic_action_chat));
        mNavItems.add(new NavItem("Options", R.mipmap.ic_action_icon_settings2));
        mNavItems.add(new NavItem("Help", R.mipmap.ic_action_faq));

        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);
        // selectItemFromDrawer(3);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        getFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        Fragment f = getFragmentManager().findFragmentById(R.id.mainContent);
                        if (f != null) {
                            String fragClassName = f.getClass().getName();
                            if (fragClassName.equals(LiveFeedFragment.class.getName())) {
                                setTitle("Live Feed");
                                mDrawerList.setItemChecked(0, true);
                            } else if (fragClassName.equals(FragmentMyProfile.class.getName())) {
                                setTitle("My Profile");
                                mDrawerList.setItemChecked(1, true);
                            } else if (fragClassName.equals(FragmentMyLocalPosts.class.getName())) {
                                setTitle("My Local Posts");
                                mDrawerList.setItemChecked(2, true);
                            } else if (fragClassName.equals(FragmentChat.class.getName())) {
                                setTitle("Chat");
                                mDrawerList.setItemChecked(3, true);
                            } else if (fragClassName.equals(FragmentSettings.class.getName())) {
                                setTitle("Options");
                                mDrawerList.setItemChecked(4, true);
                            } else {
                                setTitle("Help");
                                mDrawerList.setItemChecked(5, true);
                            }
                        }
                    }
                });
        /*
        mDrawerList.performItemClick(
                mDrawerList.getAdapter().getView(0, null, null), 0, 0);
                */
        selectItemFromDrawer(0);

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void setHeaderInfo(String image) {
        if (image.isEmpty()) {
            picHeader.setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (image.contains("gatherspot")) {
                Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                picasso.load(image).into(picHeader);
            } else {
                Picasso.with(this).load(image).into(picHeader);
            }
        }
    }

    public void setHeaderUsername(String uname) {
        username.setText(uname);
    }

    private void getUserInfo() {
        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {

            @Override
            public void success(UserInfoItem arg0, Response arg1) {
                edt.putString("myusername", arg0.getUsername());
                edt.putString("myprofilepicurl", arg0.getProfilePic());
                edt.commit();
                if (arg0.getProfilePic() == null) {
                    picHeader
                            .setImageResource(R.mipmap.ic_action_icon_person);
                } else {
                    if (arg0.getProfilePic().contains("gatherspot")) {
                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                        picasso.load(arg0.getProfilePic())
                                .into(picHeader);
                    } else {
                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                .into(picHeader);
                    }
                }
                username.setText(arg0.getUsername());

                // trebam zato kaj je ludi halambek nabacio gcm u tu rutu ...
                uname = arg0.getUsername();
                about = arg0.getAbout();
                //handleGcmToken();
            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edt.putString("mytoken", arg0.getToken());
                                        edt.putString("myuserid", arg0.getUser_id());
                                        edt.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                edt.putString("myusername", arg0.getUsername());
                                                edt.putString("myprofilepicurl", arg0.getProfilePic());
                                                edt.commit();
                                                if (arg0.getProfilePic() == null) {
                                                    picHeader
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                } else {
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                                                        picasso.load(arg0.getProfilePic())
                                                                .into(picHeader);
                                                    } else {
                                                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                                                .into(picHeader);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());

                                                // trebam zato kaj je ludi halambek nabacio gcm u tu rutu ...
                                                uname = arg0.getUsername();
                                                about = arg0.getAbout();
                                                //handleGcmToken();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edt.putString("mytoken", arg0.getToken());
                                        edt.putString("myuserid", arg0.getUser_id());
                                        edt.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                edt.putString("myusername", arg0.getUsername());
                                                edt.putString("myprofilepicurl", arg0.getProfilePic());
                                                edt.commit();
                                                if (arg0.getProfilePic() == null) {
                                                    picHeader
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                } else {
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                                                        picasso.load(arg0.getProfilePic())
                                                                .into(picHeader);
                                                    } else {
                                                        Picasso.with(getBaseContext()).load(arg0.getProfilePic())
                                                                .into(picHeader);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());

                                                // trebam zato kaj je ludi halambek nabacio gcm u tu rutu ...
                                                uname = arg0.getUsername();
                                                about = arg0.getAbout();
                                                //handleGcmToken();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }

            }
        });

    }

    private void sendGcmTokenToServer() {
        gcmPrefs = getApplicationContext().getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        api.changeUserData(gcmPrefs.getString("gcmToken", ""), token, uname, about, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edt.putString("mytoken", arg0.getToken());
                                        edt.putString("myuserid", arg0.getUser_id());
                                        edt.commit();
                                        token = arg0.getToken();
                                        api.changeUserData(gcmPrefs.getString("gcmToken", ""), token, uname, about, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edt.putString("mytoken", arg0.getToken());
                                        edt.putString("myuserid", arg0.getUser_id());
                                        edt.commit();
                                        token = arg0.getToken();
                                        api.changeUserData(gcmPrefs.getString("gcmToken", ""), token, uname, about, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }

        });
    }


    private void selectItemFromDrawer(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new LiveFeedFragment();
                fragmentName = "LiveFeedFragment";
                break;
            case 1:
                fragment = new FragmentMyProfile();
                fragmentName = "FragmentMyProfile";
                break;
            case 2:
                fragment = new FragmentMyLocalPosts();
                fragmentName = "FragmentMyLocalPosts";
                break;
            case 3:
                fragment = new FragmentChat();
                fragmentName = "FragmentChat";
                break;
            case 4:
                fragment = new FragmentSettings();
                fragmentName = "FragmentSettings";
                break;
            case 5:
                fragment = new FragmentHelp();
                fragmentName = "FragmentHelp";
                break;
            default:
                break;
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.mainContent, fragment, fragmentName).commit();

        mDrawerList.setItemChecked(position, true);
        setTitle(mNavItems.get(position).mTitle);

        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    class NavItem {
        int mIcon;
        String mTitle;

        public NavItem(String title, int icon) {
            mTitle = title;
            mIcon = icon;
        }

    }

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.aadrawerlistitem, null);
            } else {
                view = convertView;
            }

            TextView title = (TextView) view.findViewById(R.id.title);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);

            title.setText(mNavItems.get(position).mTitle);
            icon.setImageResource(mNavItems.get(position).mIcon);

            return view;
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }
}
