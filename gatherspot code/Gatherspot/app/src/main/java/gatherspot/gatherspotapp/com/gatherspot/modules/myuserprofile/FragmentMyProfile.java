package gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile;

/**
 * Created by Josip on 29.5.2015..
 */


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.modules.newpost.ActivityNewPersonalPost;
import gatherspot.gatherspotapp.com.gatherspot.MainActivity;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterMyProfile;
import gatherspot.gatherspotapp.com.gatherspot.pojo.Feed;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UserInfoItem;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FragmentMyProfile extends Fragment {

    SharedPreferences myTokenPrefs;
    private String token, user_id = "a";
    ImageView headerPic;
    ListView lv1;
    ProgressBar progressCircle;
    boolean first;
    String loginType;
    SharedPreferences resume, myPersonalExistPrefs, lUpdatesPrefs, firstRun,
            loginTypePrefs, gcmPrefs, backgroundImagePrefs;
    SharedPreferences.Editor edt, edtLocationActive, edtFirstRun, edtToken;
    boolean yesNoResume, yesNoResumeComments;
    View rootView, headerView, hintView;
    RelativeLayout hintBackground;
    Button hintAddPersonalPost;
    List<PersonalPostItem> listOfMyPersonal;
    public static final String ENDPOINT = "https://service.gatherspot.me";
    TextView username;
    ExpandableTextView mpInfo1;

    // za handlanje hintova kod prvog paljenja
    int hintCounter = 0;

    BitmapDrawable image;

    RelativeLayout background;

    android.support.v7.app.ActionBar ab;

    private Activity mActivity;

    public FragmentMyProfile() {
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mActivity = activity;
        ab = ((AppCompatActivity) activity).getSupportActionBar();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        firstRun = mActivity.getSharedPreferences("FIRST",
                Context.MODE_PRIVATE);
        edtFirstRun = firstRun.edit();
        backgroundImagePrefs = mActivity.getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
        first = firstRun.getBoolean("MYPROFILE", false);
        myTokenPrefs = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        user_id = myTokenPrefs.getString("myuserid", "a");

        gcmPrefs = mActivity.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = mActivity.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
            }
        }
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
        resume = mActivity
                .getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        yesNoResume = resume.getBoolean("yesNo", true);
        if (yesNoResume) {
            requestUserInfo();
        } else {
            edt = resume.edit();
            edt.putBoolean("yesNo", true);
            edt.commit();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater
                .inflate(R.layout.myprofile_layout, container, false);

        background = (RelativeLayout) rootView.findViewById(R.id.background);

        if (BackgroundImageHandler.fileExistance(mActivity)) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.45f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(mActivity.getResources(),
                    BackgroundImageHandler.loadImageFromStorage(mActivity));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
        }

        progressCircle = (ProgressBar) rootView.findViewById(R.id.progressBarMyProfile);

        headerView = inflater
                .inflate(R.layout.feed_item_header_mp, null, false);
        headerPic = (ImageView) headerView.findViewById(R.id.profilePic);
        username = (TextView) headerView.findViewById(R.id.username);
        mpInfo1 = (ExpandableTextView) headerView.findViewById(R.id.expand_text_view);
        lv1 = (ListView) rootView.findViewById(R.id.list);

        if (first == false) {
            first = true;
            edtFirstRun.putBoolean("MYPROFILE", true);
            edtFirstRun.commit();
        }

        return rootView;
    }


    private void requestUserInfo() {

        final ApiInterface api = InitHttpClient.getInstance().getApiInterface();

        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                if (arg0.getProfilePic() == null) {
                                                    ((MainActivity) mActivity).setHeaderInfo("");
                                                    headerPic
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                                                } else {
                                                    // slika u sliding drawer
                                                    ((MainActivity) mActivity).setHeaderInfo(arg0
                                                            .getProfilePic());
                                                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                                                        picasso.load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    } else {
                                                        Picasso.with(mActivity).load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());
                                                mpInfo1.setText(arg0.getAbout());
                                                // lv1.removeViewInLayout(headerView);
                                                if (lv1.getAdapter() == null) {
                                                    lv1.addHeaderView(headerView);
                                                }
                                                requestData();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getUserInfo(token, user_id, new Callback<UserInfoItem>() {
                                            @Override
                                            public void success(UserInfoItem arg0, Response response) {
                                                if (arg0.getProfilePic() == null) {
                                                    ((MainActivity) mActivity).setHeaderInfo("");
                                                    headerPic
                                                            .setImageResource(R.mipmap.ic_action_icon_person);
                                                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                                                } else {
                                                    // slika u sliding drawer
                                                    ((MainActivity) mActivity).setHeaderInfo(arg0
                                                            .getProfilePic());
                                                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                                                    if (arg0.getProfilePic().contains("gatherspot")) {
                                                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                                                        picasso.load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    } else {
                                                        Picasso.with(mActivity).load(arg0.getProfilePic())
                                                                .into(headerPic);
                                                    }
                                                }
                                                username.setText(arg0.getUsername());
                                                mpInfo1.setText(arg0.getAbout());
                                                // lv1.removeViewInLayout(headerView);
                                                if (lv1.getAdapter() == null) {
                                                    lv1.addHeaderView(headerView);
                                                }
                                                requestData();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }

            @Override
            public void success(UserInfoItem arg0, Response arg1) {

                if (arg0.getProfilePic() == null) {
                    ((MainActivity) mActivity).setHeaderInfo("");
                    headerPic
                            .setImageResource(R.mipmap.ic_action_icon_person);
                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                } else {
                    // slika u sliding drawer
                    ((MainActivity) mActivity).setHeaderInfo(arg0
                            .getProfilePic());
                    ((MainActivity) mActivity).setHeaderUsername(arg0.getUsername());
                    if (arg0.getProfilePic().contains("gatherspot")) {
                        Picasso picasso = InitHttpClient.getInstance().getImageLoader();
                        picasso.load(arg0.getProfilePic())
                                .into(headerPic);
                    } else {
                        Picasso.with(mActivity).load(arg0.getProfilePic())
                                .into(headerPic);
                    }
                }
                username.setText(arg0.getUsername());
                mpInfo1.setText(arg0.getAbout());
                // lv1.removeViewInLayout(headerView);
                if (lv1.getAdapter() == null) {
                    lv1.addHeaderView(headerView);
                }
                requestData();

            }
        });
    }

    private void requestData() {

        final ApiInterface api = InitHttpClient.getInstance().getApiInterface();

        api.getMyPersonalPosts(token, new Callback<Feed>() {

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getMyPersonalPosts(token, new Callback<Feed>() {
                                            @Override
                                            public void success(Feed arg0, Response response) {
                                                listOfMyPersonal = new ArrayList<PersonalPostItem>();
                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                    PersonalPostItem myPersonal = new PersonalPostItem();
                                                    myPersonal.setComments_number(item.getComments_number());
                                                    myPersonal.setId(item.getId());
                                                    if (item.getImage() == null) {
                                                        myPersonal.setImage("");
                                                    } else {
                                                        myPersonal.setImage(item.getImage());
                                                    }
                                                    myPersonal.setStatus(item.getStatus());
                                                    myPersonal.setTimeStamp(item.getTimeStamp());
                                                    myPersonal.setUp_votes(item.getUp_votes());
                                                    myPersonal.setUpvoted(item.getUpvoted());
                                                    myPersonal.setDownvoted(item.getDownvoted());
                                                    listOfMyPersonal.add(myPersonal);
                                                }
                                                myPersonalExistPrefs = mActivity.getSharedPreferences("mypersonalexist",
                                                        Context.MODE_PRIVATE);
                                                boolean exist = myPersonalExistPrefs.getBoolean("mypersonalexist", false);


                                                lUpdatesPrefs = mActivity.getSharedPreferences("lupdates",
                                                        Context.MODE_PRIVATE);
                                                edtLocationActive = lUpdatesPrefs.edit();
                                                boolean currentlyTracking = lUpdatesPrefs.getBoolean("lonoff",
                                                        false);

                                                setList();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getMyPersonalPosts(token, new Callback<Feed>() {
                                            @Override
                                            public void success(Feed arg0, Response response) {
                                                listOfMyPersonal = new ArrayList<PersonalPostItem>();
                                                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                                                    PersonalPostItem myPersonal = new PersonalPostItem();
                                                    myPersonal.setComments_number(item.getComments_number());
                                                    myPersonal.setId(item.getId());
                                                    if (item.getImage() == null) {
                                                        myPersonal.setImage("");
                                                    } else {
                                                        myPersonal.setImage(item.getImage());
                                                    }
                                                    myPersonal.setStatus(item.getStatus());
                                                    myPersonal.setTimeStamp(item.getTimeStamp());
                                                    myPersonal.setUp_votes(item.getUp_votes());
                                                    myPersonal.setUpvoted(item.getUpvoted());
                                                    myPersonal.setDownvoted(item.getDownvoted());
                                                    listOfMyPersonal.add(myPersonal);
                                                }
                                                myPersonalExistPrefs = mActivity.getSharedPreferences("mypersonalexist",
                                                        Context.MODE_PRIVATE);
                                                boolean exist = myPersonalExistPrefs.getBoolean("mypersonalexist", false);


                                                lUpdatesPrefs = mActivity.getSharedPreferences("lupdates",
                                                        Context.MODE_PRIVATE);
                                                edtLocationActive = lUpdatesPrefs.edit();
                                                boolean currentlyTracking = lUpdatesPrefs.getBoolean("lonoff",
                                                        false);

                                                setList();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }

            }

            @Override
            public void success(Feed arg0, Response arg1) {
                listOfMyPersonal = new ArrayList<PersonalPostItem>();
                for (PersonalPostItem item : arg0.getPersonalPosts()) {
                    PersonalPostItem myPersonal = new PersonalPostItem();
                    myPersonal.setComments_number(item.getComments_number());
                    myPersonal.setId(item.getId());
                    if (item.getImage() == null) {
                        myPersonal.setImage("");
                    } else {
                        myPersonal.setImage(item.getImage());
                    }
                    myPersonal.setStatus(item.getStatus());
                    myPersonal.setTimeStamp(item.getTimeStamp());
                    myPersonal.setUp_votes(item.getUp_votes());
                    myPersonal.setUpvoted(item.getUpvoted());
                    myPersonal.setDownvoted(item.getDownvoted());
                    listOfMyPersonal.add(myPersonal);
                }
                myPersonalExistPrefs = mActivity.getSharedPreferences("mypersonalexist",
                        Context.MODE_PRIVATE);
                boolean exist = myPersonalExistPrefs.getBoolean("mypersonalexist", false);


                lUpdatesPrefs = mActivity.getSharedPreferences("lupdates",
                        Context.MODE_PRIVATE);
                edtLocationActive = lUpdatesPrefs.edit();
                boolean currentlyTracking = lUpdatesPrefs.getBoolean("lonoff",
                        false);

                setList();

            }

        });
    }

    private void handleHint() {
        /*
        if (firstRun.getBoolean("tutorialpersonal", true)) {
        */
        if (lv1.getAdapter() == null) {
            addHeaderHint();
        }
        if (listOfMyPersonal.isEmpty()) {
            hintBackground.setVisibility(View.VISIBLE);
        } else {
            // edtFirstRun.putBoolean("tutorialpersonal", false);
            // edtFirstRun.commit();
            hintBackground.setVisibility(View.GONE);
        }
        /*
        }
        */

    }

    private void addHeaderHint() {
        hintView = mActivity.getLayoutInflater().inflate(R.layout.feed_item_hint, null, false);
        hintBackground = (RelativeLayout) hintView.findViewById(R.id.backgroundHint);
        hintAddPersonalPost = (Button) hintView.findViewById(R.id.newPost);
        hintAddPersonalPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPost = new Intent(mActivity, ActivityNewPersonalPost.class);
                startActivity(intentPost);
            }
        });
        lv1.addHeaderView(hintView);

    }

    private void setList() {
        yesNoResumeComments = resume.getBoolean("yesNoComments", true);
        handleHint();
        if (yesNoResumeComments == false) {
            // refill
            if (lv1.getAdapter() == null) {
                ListAdapterMyProfile listAdapter = new ListAdapterMyProfile(mActivity,
                        listOfMyPersonal, token);
                lv1.setAdapter(listAdapter);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            } else {
                ((ListAdapterMyProfile) ((HeaderViewListAdapter) lv1.getAdapter())
                        .getWrappedAdapter()).refill(listOfMyPersonal);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            }
        } else {
            ListAdapterMyProfile listAdapter = new ListAdapterMyProfile(mActivity,
                    listOfMyPersonal, token);
            lv1.setAdapter(listAdapter);
        }
        progressCircle.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        myTokenPrefs = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.mp_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.newPersonalPost:
                Intent intentPost = new Intent(mActivity, ActivityNewPersonalPost.class);
                startActivity(intentPost);
                /*
                if (lv1.getAdapter() != null) {
                    if (lv1.getAdapter().getCount() > 25) {
                        Toast.makeText(mActivity,
                                "You can broadcast a maximum of 25 personal posts", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intentPost = new Intent(mActivity, ActivityNewPersonalPost.class);
                        startActivity(intentPost);
                    }
                }
                */

                break;
            case R.id.settingsProfile:
                Intent intentProfileSettings = new Intent(mActivity,
                        ActivityMyProfile.class);
                startActivity(intentProfileSettings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(mActivity);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //listOfMyPersonal = null;
        if (background.getBackground() != null) {
            background.getBackground().setCallback(null);
        }
        if (image != null) {
            image.setCallback(null);
            //image.getBitmap().recycle();
            image = null;
        }
    }
}