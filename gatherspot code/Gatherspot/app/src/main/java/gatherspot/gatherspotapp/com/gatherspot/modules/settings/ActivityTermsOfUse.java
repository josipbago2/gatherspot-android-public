package gatherspot.gatherspotapp.com.gatherspot.modules.settings;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import gatherspot.gatherspotapp.com.gatherspot.R;

/**
 * Created by Josip on 11.8.2015..
 */
public class ActivityTermsOfUse extends AppCompatActivity {

    ImageView post;
    String idPpTerms;
    TextView title, text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_pp_terms);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        post = (ImageView) findViewById(R.id.showPost);
        title = (TextView) findViewById(R.id.title);
        text = (TextView) findViewById(R.id.text);
        text.setMovementMethod(new ScrollingMovementMethod());

        Intent intent = getIntent();
        idPpTerms = intent.getStringExtra("id");

        if (idPpTerms.equals("Terms of Use")) {
            getSupportActionBar().setTitle("Terms of Use");
            title.setText("Terms Of Use");
            text.setText("Gatherspot and its related sites or software products (�Site� or �Service� or mobile apps for iOS and Android named �Gatherspot�) are provided to all users (\"User\" or \"Users\") subject to these Terms of Service (\"Terms\"). Any use of the Service binds a User to these Terms. If a User objects to any portion of these Terms, a User must immediately discontinue usage of the Site. Gatherspot reserves the right to enforce these terms via any available lawful means, including but not limited to, civil litigation and referral to applicable authorities for criminal prosecution.\n\n" +
                    "THIS IS A LEGAL AGREEMENT BETWEEN YOU AND GATHERSPOT. ALL USERS OF GATHERSPOT SHOULD CAREFULLY REVIEW THESE TERMS, AS THEY CONTAIN IMPORTANT INFORMATION ABOUT USERS' LEGAL RIGHTS AND RESPONSIBILITIES.\n" +
                    "These Terms may be supplemented by additional legal policies or procedures, which are hereby incorporated by reference. In circumstances where there are conflicts between these Terms and the additional policies above, these Terms shall be deemed superior to the terms of any other policy.\n\n" +
                    "Definitions:\n" +
                    "a.)\t�Content�: is information which includes (but is not limited to) text, pictures, web links, locations, and designs. \n" +
                    "b.)\t\"User\": refers to any Internet visitor or user of Gatherspot, namely a person that interacts with Gatherspot software products.\n" +
                    "c.)\t\"Visitor\" refers to any individual Internet browser that may view any portion of the Site, particularly public portions of the site that do not require registration.\n" +
                    "\n" +
                    "Registration, User Data and User Accounts\n\n" +
                    "1.\tGatherspot Registration Eligibility. By using the Service or the Site, You represent and warrant that you are thirteen (13) years of age or older, and that you agree to abide by these Terms. By registering with the Site, a User affirms that the User's registration is in compliance with all laws, rules and regulations applicable to the User. Gatherspot reserves the right to prevent or cancel a User's registration for any reason. Membership on Gatherspot is void where prohibited by law.\n" +
                    "\n" +
                    "2.\tUser Account Identity. User account identity on Gatherspot is driven by Google account number for Android devices and by Apple ID number for iPhones. By registration and creating User Account on Gatherspot, User allows Gatherspot to use this information as  identity information. \n" +
                    "\n" +
                    "3.\tUser Account Registration Data. Information provided to the Site by Users do not have to be legally accurate, however impersonation of third parties is forbidden. Users shall update information promptly. Users agree to maintain the security of their respective usernames, and other personally identifiable information on the site. Users are responsible for any activities and actions undertaken via the User's account on the site.\n" +
                    "\n" +
                    "4.\tThe owner of a User Account is solely responsible for all activities occurring on the Site under the applicable User Account. \n\n" +
                    "a.) User is responsible for all activities that occur under the applicable User account, including, but not limited to, any third party use of an applicable User Account.\n\n" +
                    "b.) User will not permit any unauthorized third party from accessing the site via the User's account\n\n" +
                    "c.) User agrees that Gatherspot is not responsible for User loss that may occur as a result of any unauthorized use of a User account.\n\n" +
                    "d.) User shall not employ the Site for any purpose that is unlawful, fraudulent or contrary to these Terms.\n\n" +
                    "e.) User agrees to cooperate with any Gatherspot investigation into fraudulent or unlawful activity\n\n" +
                    "f.) User agrees not to create obscene, defamatory or hateful User names.\n\n" +
                    "g.) User agrees not to create content which is in any way insulting, hateful and/or offensive in any way.\n\n" +
                    "\n" +
                    "\n" +
                    "5.\t User Account Suspension, Cancellation and Termination. Gatherpot reserves the right, in its sole discretion, to suspend or terminate the account of any User for any reason. The Site may similarly prohibit a User from accessing the Site or any portion of the Service at its sole discretion. Gatherspot reserves the right to cancel or disable accounts.\n" +
                    "\n" +
                    "\n" +
                    "6.\tUser Acceptance of Privacy Policy.  Gatherspot maintains a separate Privacy Policy applicable to the Service, incorporated by reference, which explains the Site's collection and use of information gathered on the Site. Personally-identifiable information that you submit to Gatherspot for purposes of receiving products or services will be handled in accordance with the Gatherspot Privacy Policy or other applicable provisions. Any User accessing the Site acknowledges and agrees to the Gatherspot Privacy Policy.\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Content and Intellectual Property\n\n" +
                    "7.\tSite Ownership. The Service and Site are owned by Gatherspot. Most aspects of the Service and Site, including, but not limited to, all methods, processes, content, formats, domain names, and extensions are the exclusive property of Gatherspot. Other rights, including, but not limited to, all inventions, trade secrets, patents, copyrights, trademarks and other intellectual property rights are the exclusive property of Gatherspot. Gatherspot does not confer any rights to any User, either expressly or by implication, except as otherwise provided herein.\n" +
                    "\n" +
                    "8.\tUser Content Ownership. Content provided by Users for use or display on the Service shall continue to be owned by the applicable User or any other applicable third party rights holder. By providing Content on the Service, Users grant Gatherspot a limited license to host, transmit and display User Content or third party Content to which User has rights. The limited license provided by User does not grant Gatherspot any right, title or interest in the User Content or any third party content or service.\n" +
                    "\n" +
                    "9.\tProhibition on Infringement. No User shall use the Site to infringe upon any copy written works, trademarks, trade secrets, or other intellectual property rights of any third party.\n" +
                    "\n" +
                    "10.\tUser Communications. Users acknowledge that communications with Gatherspot, including, but not limited to, comments, suggestions, questions, and other information or communications about the Service are not confidential. Such communications shall become the property of Gatherspot. In sending the Site any information or material, you grant Gatherspot an unrestricted, irrevocable license to copy, reproduce, publish, upload, post, transmit, distribute, publicly display, perform, modify, create derivative works from, and otherwise freely use, such materials or information without User compensation or acknowledgment.\n" +
                    "\n" +
                    "11.\tLimited License to Access Content. The content appearing on Gatherspot may be subject to copyright protection. Applicable copyrights are the proprietary property of Gatherspot, its Users, its licensors, or other third parties with all rights reserved.\n" +
                    "\n" +
                    "a.)\tContent on Gatherspot may not be copied, reproduced, republished, uploaded, posted,           transmitted, distributed, or otherwise used for the creation of derivative works without the express written consent of Gatherspot.\n" +
                    "\n" +
                    "b.)\tUsers are granted a non-exclusive, non-transferable, limited permission to access and display the content appearing on Gatherspot on the User's client for purposes of personal, non-commercial use of the Service subject to these Terms. Users must retain all copyright, trademarks, and other intellectual property notices contained in content.\n" +
                    "\n" +
                    "c.)\tSome Content appearing on Gatherspot, including, but not limited to, third party content, software and related resources, may be subject to their own terms, conditions, licenses and notices. Such Content shall be governed by their own terms and conditions as may or may not be provided.\n" +
                    " \n" +
                    "d.)\tAll licenses provided to Users by Gatherspot are subject to revocation by the Service at any time without notice and with or without cause. Any use of the Service other than specifically provided in these Terms or as expressly granted by Gatherspot is strictly prohibited and shall immediately terminate without notice any license granted by these Terms.\n" +
                    "\n" +
                    "\n" +
                    "12.\tTrademarks, Service Marks and Other Intellectual Property. Content appearing on Gatherspot may contain trademarks, service marks, trade names and trade dress owned by Gatherspot. These marks and names are subject to applicable trademark laws in the United States and internationally, and may not be used in connection with any service or product that is likely to violate applicable trademark laws unless such use is expressly granted by Gatherspot. Unless explicitly stated, nothing in these Terms shall be construed as a grant of intellectual property rights under any legal theory.\n" +
                    "\n" +
                    "13.\tThird Party Content and Services. The Content available via the Service may include access to third party sites, services and content provided by other Users that is completely independent of Gatherspot.\n" +
                    "\n" +
                    "a.)\tThese third party resources may include, but are not limited to, third party web sites, software, products, services, networks and similar resources.\n" +
                    "\n" +
                    "b.)\tInclusion of third party resources on Gatherspot does not imply endorsement or approval of any particular resource. Gatherspot does not investigate, review, monitor or otherwise regulate third party resources that may appear on the Site, and the Service does not warrant the accuracy, completeness or appropriateness of third party resources appearing on the Site. Any User choosing to interact with a third party resource appearing on Gatherspot is subject to the terms and conditions of that particular third party resources\n" +
                    "\n" +
                    "c.)\tUsers access third party resources found on the Site at their own risk. Gatherspot urges Users to review the applicable terms and policies, including privacy and data gathering practices, of any third party resource accessed from Gatherspot. All Users agree that Gatherspot shall not be responsible for any loss or damage of any sort incurred as the result of any dealings or transactions between Users or between a User and a third party.\n" +
                    "\n" +
                    "\n" +
                    "14.\t Linking to the Site. Gatherspot generally licenses third parties to provide links to the Site.\n" +
                    "\n" +
                    "a.)\tLinks to the Site must not alter the visual appearance or content of the site, misrepresent a relationship with Gatherspot, imply that the link is an endorsement or approval of the linking site or its related products and services by Gatherspot, present false information about Gatherspot or otherwise damage the reputation and goodwill of Gatherspot.\n" +
                    "\n" +
                    "b.)\t All links to Gatherspot are subject to the Site's continuing permission to maintain such links. Gatherspot reserves the right to terminate permission to link to the Site at any time and third parties that link to the Site are obligated to immediately remove all links to the Site upon the request of Gatherspot.\n" +
                    "\n" +
                    "15.\tClaims of Copyright Infringement. Gatherspot prohibits copyright infringement on its Site. Pursuant to the Digital Millennium Copyright Act (\"DMCA\"), Gatherspot provides the following contact information for receipt of infringement notices: josipbago2@gmail.com\n" +
                    "\n" +
                    "a.)\tAny notice of claimed infringement must be a written communication that includes the following under 17 U.S.C. �512(c)(3):\n" +
                    "\n" +
                    "i. A physical or electronic signature of the person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.\n" +
                    "ii. Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works.\n" +
                    "iii. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material.\n" +
                    "iv. Information reasonably sufficient to permit the service provider to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.\n" +
                    "v. A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.\n" +
                    "vi. A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.\n" +
                    "\n" +
                    "b.)\tUpon receipt of a notice of claimed infringement, Gatherspot will remove or disable access to the allegedly infringing material and promptly notify the alleged infringer of the claim. Subsequent proceedings after initial notification are governed by the DMCA. Pursuant to the DMCA and other applicable law, Gatherspot reserves the right to terminate any infringer, particularly repeat infringers, for any reason in its sole discretion.\n" +
                    "\n" +
                    "16.\tClaims of Trademark Infringement. Gatherspot prohibits trademark infringement on its Site. Gatherspot provides the following contact information for receipt of infringement notices: josipbago2@gmail.com\n" +
                    "\n" +
                    "a.)\tAny notice of claimed infringement must be a written communication that includes the following:\n" +
                    "i. A physical or electronic signature of the person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.\n" +
                    "ii. Identification of the trademark claimed to have been infringed, complete with registration number and issuing government authority.\n" +
                    "iii. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material.\n" +
                    "iv. Information reasonably sufficient to permit the service provider to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.\n" +
                    "\n" +
                    "b.)\tUpon receipt of a notice of claimed infringement, Gatherspot will investigate the claim. Gatherspot reserves the right to terminate any infringer, particularly repeat infringers, for any reason in its sole discretion.\n" +
                    "\n" +
                    "Site and Service\n\n" +
                    "17.\tLimitations of Service. Gatherspot acts solely as a technological intermediary between Users. Gatherspot does not produce, provide or control User Content. Gatherspot does not evaluate or control information shared between Users. Gatherspot does not provide any content, product or service other than the technical services that allow Users to connect via voice conversation. Gatherspot maintains no editorial or other control concerning Content provided by Users.\n" +
                    "\n" +
                    "18.\tIndependent Users. Gatherspot does not evaluate, provide, produce or control information or exchanges between Users, in any manner.\n" +
                    "\n" +
                    "a.)\tGatherspot does not verify, guarantee or make any representations regarding the identity of any User.\n" +
                    "\n" +
                    "b.)\tGatherspot does not make editorial or managerial decisions concerning User content, and assumes no responsibility for any User's compliance with laws or regulations concerning the content of listings or profiles.\n" +
                    "\n" +
                    "c.)\tGatherspot does not control and is not responsible for the truth, accuracy, completeness, safety, timeliness, quality, appropriateness, legality or applicability of anything said or written by any User.\n" +
                    "\n" +
                    "d.)\tUser information found on the Site does not imply that Gatherspot endorses, recommends, verifies or evaluates any User. No content on the Site shall be considered a referral, endorsement, recommendation or guarantee of any User.\n" +
                    "\n" +
                    "19.\tCompliance with Terms. Gatherspot reserves the right to monitor any communications between Users for the purposes of enforcing compliance with these Terms.\n" +
                    "\n\nUser's Consumption of Services on the Site\n\n" +
                    "20.\t Service Restrictions. User agrees to the following restrictions as a condition of employing the Service.\n" +
                    "\n" +
                    "a.)\tNo User shall attempt to circumvent the Site or its communications process by any means.\n" +
                    "\n" +
                    "b.)\tNo User shall engage in any activity that would be considered harassing to any other User.\n" +
                    "\n" +
                    "c.)\tNo User shall make any effort to obtain unauthorized access to any portion of the Site that is not intended for the User.\n" +
                    "\n" +
                    "d.)\tNo User shall make any communication to other Users related to any third-party economic interest, including, but not limited to, promotions of other websites, businesses or interests.\n" +
                    "\n" +
                    "e.)\tMembers who initiate and receive services via Gatherspot do so entirely at their own risk.\n\n\n" +
                    "Data Provision and Battery Lifespan\n\n" +
                    "21.\tData Provision and Battery Lifespan. Gatherspot relies on certain data to operate on a mobile device, including, but not limited to, User location information and User generated Content. By default, data used for running Gatherspot is automatically fetched or pushed via a data connection, without any limitation. The more frequently your mobile device pushes or fetches data, the quicker your device battery may drain. Users are advised that they may adjust location settings via their mobile device's operating system software. Gatherspot may also provide certain settings within the Service that limit or reduce automatic data transfer, as Gatherspot may determine from time to time.\n" +
                    "\n\nDisclaimer of Warranties and Limitation of Liability\n" +
                    "\n" +
                    "22.\tDisclaimers. Gatherspot attempts to ensure accuracy of information on the Service, but cannot guarantee that any content is entirely correct, current or complete. The Site may contain technical inaccuracies or typographical errors. The Site may occasionally be temporarily unavailable for maintenance and related reasons. USE OF THIS SITE IS AT A USER'S SOLE RISK. ALL CONTENT AND SERVICES ARE PROVIDED \"AS IS,\" WITH NO WARRANTIES OR GUARANTEES WHATSOEVER. GATHERSPOT EXPRESSLY DISCLAIMS TO THE FULLEST EXTENT PERMITTED BY LAW ALL EXPRESS, IMPLIED, STATUTORY, AND OTHER WARRANTIES, GUARANTEES, OR REPRESENTATIONS, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY AND INTELLECTUAL PROPERTY RIGHTS. WITHOUT LIMITATION, GATHERSPOT MAKES NO WARRANTY OR GUARANTEE THAT THIS SITE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE. YOU UNDERSTAND AND AGREE THAT ACCESSING GATHERSPOT IS AT THE USER'S OWN RISK AND DISCRETION AND THAT THE USER IS SOLELY RESPONSIBLE FOR ANY DAMAGES THAT MAY RESULT, INCLUDING LOSS OF DATA OR DAMAGE TO YOUR COMPUTER SYSTEM. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF WARRANTIES, SO THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.\n" +
                    "\n" +
                    "23.\tLimitation of Liability. IN NO EVENT WILL GATHERSPOT OR ITS DIRECTORS, EMPLOYEES OR AGENTS BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING FOR ANY LOST PROFITS OR LOST DATA ARISING FROM YOUR USE OF THE SITE. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, THE SITE'S LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER. AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO THE AMOUNT PAID, IF ANY, BY YOU TO THE SITE FOR THE SERVICE, BUT IN NO CASE WILL THE SITE'S LIABILITY TO YOU EXCEED $100. YOU ACKNOWLEDGE THAT IF NO FEES ARE PAID TO GATHERSPOT FOR THE SERVICE, YOU SHALL BE LIMITED TO INJUNCTIVE RELIEF ONLY, UNLESS OTHERWISE PERMITTED BY LAW, AND SHALL NOT BE ENTITLED TO DAMAGES OF ANY KIND FROM GATHERSPOT, REGARDLESS OF THE CAUSE OF ACTION.\n" +
                    "\n" +
                    "24.\tIndemnity. YOU FURTHER AGREE TO HOLD HARMLESS, DEFEND AND INDEMNIFY GATHERSPOT, AND ITS EMPLOYEES, SUBSIDIARIES, AGENTS AND REPRESENTATIVES, FROM AND AGAINST ANY LIABILITY ARISING FROM OR IN ANY WAY RELATED TO YOUR USE OF THE SITE OR PROVISION OF OPERATOR SERVICES, INCLUDING ANY LIABILITY OR EXPENSE ARISING FROM ALL CLAIMS, LOSSES, DAMAGES (ACTUAL AND CONSEQUENTIAL), SUITS, JUDGMENTS, LITIGATION COSTS AND ATTORNEYS' FEES, OF EVERY KIND AND NATURE, KNOWN AND UNKNOWN, FORESEEABLE AND UNFORSEEABLE, DISCLOSED AND UNDISCLOSED.\n" +
                    "\n\n" +
                    "Additional Terms\n\n" +
                    "25.\tNotices. Any notice or other communication regarding these Terms shall be written. Notices sent to Gatherspot shall be sent to josipbago2@gmail.com\n" +
                    "\n" +
                    "26.\tAssignment. Except as expressly provided herein, no User shall have any right or ability to assign, transfer, or sublicense any obligations or benefit under these Terms without the written consent of Gatherspot\n" +
                    "\n" +
                    "27.\tSeverability. If for any reason a court of competent jurisdiction finds any provision or portion of these Terms to be unenforceable, that provision of these Terms will be enforced to the maximum extent permissible so as to affect the intent of the parties, and the remainder of these Terms will continue in full force and effect.\n" +
                    "\n28.\tModifications. Gatherspot retains the right to modify these Terms at any time, and changes shall become effective immediately upon publication. While Gatherspot may notify Users of changes, the User is obligated to review these Terms on a regular basis.\n" +
                    "\n29.\t Entire Agreement. These Terms, including additional policies incorporated by reference, constitute the entire agreement between Gatherspot and a User\n" +
                    "\n30.\tWaiver. Gatherspot failure to act with respect to a breach of these terms by a User does not waive the Site's right to act with respect to subsequent or similar breaches.\n" +
                    "\n");
        } else if (idPpTerms.equals("Privacy Policy")) {
            getSupportActionBar().setTitle("Privacy Policy");
            title.setText("Privacy Policy");
            text.setText("Gatherspot (\"Gatherspot\" or \"Site\" or \"Service\") has created this Privacy Policy (\"Policy\") in order to demonstrate its firm commitment to privacy and explain how the Site uses information collected from visitors. In adopting this Policy, the intent is to balance the Site's legitimate business interests in collecting and using personally identifiable information and visitors' reasonable expectations of privacy. The following Privacy Policy discloses the information gathering and dissemination practices for Gatherspot.\n" +
                    "ALL USERS OF GATHERSPOT SHOULD CAREFULLY REVIEW THIS PRIVACY POLICY, AS IT CONTAINS IMPORTANT INFORMATION ABOUT USERS' LEGAL RIGHTS AND RESPONSIBILIITES.\n\n" +
                    "Definitions\n\n" +
                    "a.)\t�Content�: is information which includes (but is not limited to) text, pictures, videos, sounds, web links, locations, and designs. \n\n" +
                    "b.)\t\"User\": refers to any Internet visitor or user of Gatherspot, namely a person that interacts with Gatherspot software products.\n\n" +
                    "c.)\t\"Visitor\" refers to any individual Internet browser that may view any portion of the Site, particularly public portions of the site that do not require registration.\n\n" +
                    "\n" +
                    "\n" +
                    "Non-Personally Identifying Information\n" +
                    "1.\tInformation Collected from Visitors. Gatherspot may collect certain information from all Visitors that is not personally identifiable. This information includes, but is not limited to, the User's Internet service provider (ISP), Internet protocol (IP) address, applicable location, along with Internet browser type, version and operating system (OS). Other information that may be collected includes the date and time of a Visitor's exit and entry on the Site, the names and addresses of referral sites, the specific pages a Visitor chooses to visit on the Site and certain search terms that a Visitor may have employed to find the Site.\n" +
                    "\n" +
                    "2.\tUse of Information from Visitors. Information collected from Visitors is used for purposes of internal reviews, including, but not limited to, traffic audits, tailoring of pages to a particular user's technology environment, analysing traffic and search trends, generating aggregate demographic information and general Site administration. This information may be shared in an aggregate form with advertisers and other third parties with a legitimate interest in the data. This aggregated data is anonymous and does not allow third parties to identify any Visitor personally.\n" +
                    "\n" +
                    "Personally Identifying Information\n" +
                    "3.\tRegistration by Children Prohibited. Pursuant to the Gatherspot Terms of Service, only users 13 years and older may register for an account with Gatherspot. Pursuant to the Children's Online Privacy Protection Act of 1998 (COPPA), Gatherspot generally does not knowingly collect personally identifiable information from children under thirteen (13) years of age. Upon notice that Gatherspot has inadvertently collected personally identifiable information from a child under thirteen, Gatherspot will take all reasonable steps to remove such information from its records as quickly as reasonably possible.\n" +
                    "\n" +
                    "4.\tInformation Collected from Users. Gatherspot registration may require Users to provide certain types of personally identifiable Content, such as:\n" +
                    "\n" +
                    "a.)\tPersonal Information, such as, but not limited to, name, e-mail address, social media addresses, birthdate, postal address, country of residence, year of birth, gender, personal interests and similar identifying information.\n" +
                    "b.)\tPersonal Content, such as pictures, video or other intellectual property a User may own. Users understand and agree that Gatherspot shall have an absolute license and right to use, copy, distribute, or otherwise display User provided Content on Gatherspot, as well as third-party sites with whom Gatherspot may have agreements.\n" +
                    "c.)\tLocation Information, using tools like global positioning systems (GPS), Wi-Fi, or cellular tower proximity, for purposes of providing relevant Content based on your physical location.\n" +
                    "\n" +
                    "5.\tLimitation of Liability for Privacy. For any reason Gatherspot is not responsible for any loss of privacy that is not listed here and following consequences. By using Gatherspot, User accepts to share his or her personal information and content with other users of Gatherspot , with Gatherspot as a Site, and third-party sites with whom Gatherspot may have agreements.\n" +
                    "\n" +
                    "6.\tCommunications with Users from Gatherspot. Information collected from Users may be used by Gatherspot for purposes of communicating with Users, including, but not limited to, sending Users e-mail messages from Gatherspot, corresponding with Users via email in response to User inquiries, providing services a User may request and otherwise managing a User's account.\n" +
                    "\n" +
                    "a.)\tUsers may opt-out of future promotional mailings from Gatherspot by employing methods provided in the below section entitled \"Opt-Out and Modification of Provided Information.\"\n" +
                    "b.)\tEven when a User chooses to opt-out from promotional mailings, Users will continue to receive administrative emails related to the usage and functionality of the Site.\n" +
                    "\n" +
                    "7.\tUse of IP Addresses from Users. IP addresses collected with personally identifiable information may be employed by the Site for identity, safety and security purposes. Such uses include, but are not limited to, identifying specific Users or preventing access to the Site based on a User's IP address\n" +
                    "\n" +
                    "8.\tUser Historical Data. Gatherspot maintains a historical record for each User. This historical record may include, but is not limited to, prior transactions, account history, Site activities and related data. This data may be employed to assist the Site in identifying potential liabilities, resolving disputes, troubleshooting problems and enforcing all terms of the Site. These data sets cannot be completely removed for any reason, including User request\n" +
                    "\n" +
                    "Additional Terms\n" +
                    "9.\tSecurity. Gatherspot employs commercially reasonable security methods to secure Users' privacy, including, but not limited to, third-party encryption of payment forms, password protected file systems and similar means of protection, designed to protect the loss, misuse and alteration of information under the control of Gatherspot. While Gatherspot takes commercially reasonable security precautions, the Site is not responsible for data breaches of third parties, such as payment processors, web hosts or advertising providers.\n" +
                    "\n" +
                    "10.\tVoluntary Public Disclosure. The Site may include features, such as, but not limited to, User listings, User Content, discussion forums, blog comment forms, and other related tools that allow Users to publicly share information about themselves and the Site experience. Any information a User discloses in these areas may be publicly available on the Internet. Such information may be read, collected, or used by other Internet users. Gatherspot is not responsible for personally identifiable information a User may choose to publicly disclose.\n" +
                    "\n" +
                    "11.\tThird Party Websites. This Policy applies only to information collected by Gatherspot. The Site may contain links to other third party web sites that are not owned or controlled by Gatherspot. These sites control their own privacy practices and are not bound by this Policy. Gatherspot is not responsible for the information or services provided on third party sites, nor is it responsible for the privacy practices or content of their sites.\n" +
                    "\n" +
                    "12.\tCookies. A cookie is a small text file that is stored on a user's computer for record-keeping purposes. The Site may use both \"session ID cookies\" and \"persistent cookies.\" Gatherspot may use cookies to deliver content specific to your interests, provide convenience features and for other purposes.\n" +
                    "\n" +
                    "13.\tDisclosure. Gatherspot reserves the right to disclose your personally identifiable information as required by legal mandate, including, but not limited to, compliance with a judicial proceeding, court order, or legal process lawfully served on Gatherspot. Disclosure may also occur in order to prevent or report suspected fraud or suspected illegal activities. Gatherspot may also disclose information in other circumstances that Gatherspot believes in good-faith requires disclosure.\n" +
                    "\n" +
                    "14.\tOwnership of Data. Gatherspot may exchange personally identifiable information among other entities owned or operated by Gatherspot. Were Gatherspot to engage in a business transition, such as a merger, acquisition or sale, your personally identifiable information may be among the assets transferred.\n" +
                    "\n" +
                    "\n" +
                    "15.\tPrivacy Policy Modifications. Gatherspot retains the right to modify this Policy at any time, and changes shall become effective immediately upon publication. While Gatherspot may notify Users of changes, each User is obligated to review this Policy on a regular basis.\n" +
                    "\n" +
                    "16.\tContact Information. Any notice or other communication regarding this Policy shall be written. Notices sent to Gatherspot shall be sent to josipbago2@gmail.com\n");
        } else if (idPpTerms.equals("faq1")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What is a personal post?");
            post.setVisibility(View.VISIBLE);
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("With personal posts you can broadcast your content to the world around you. They follow your location and everyone " +
                    "around you can see them on their feed anywhere you go. They are also shown on your profile and any visitor can see them. " +
                    "By sharing personal posts you can express yourself, expand your presence and show other people around you more about " +
                    "yourself. \n\n" +
                    "You can add a new personal post on My Profile in the upper-right corner. Additionally, you can add your personal post on Live Feed by choosing to " +
                    "share a personal post. You can manage and delete your personal posts on My Profile.");

        } else if (idPpTerms.equals("faq2")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What is a local post?");
            post.setVisibility(View.VISIBLE);
            post.setImageResource(R.drawable.localizrezan);
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("Local Posts attach to a place they are posted to and stay there for all Gatherspot users near that location to interact with now and in the " +
                    "future.\n\n" +
                    "You can place a new local post on Live Feed by clicking on the action bar button in the upper right corner of the screen. " +
                    "You can manage and delete your local posts on My Local Posts.");

        } else if (idPpTerms.equals("faq3")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What's the difference between personal and local post?");
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("Personal posts follow your location and everyone around you can see them on their feed anywhere you go. Local posts, on " +
                    "the other hand, attach to a place they are posted to and stay there for all Gatherspot users near that location to see " +
                    "now and in the future.\n\n" +
                    "This means that if you post a new Personal post and a new Local post in a certain place and you leave this place and go somewhere else, the " +
                    "local post will stay positioned on the place it is posted to and a personal post will move together with you anywhere you go.");

        } else if (idPpTerms.equals("faq4")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What is a local ad?");
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("Local ads are meant to connect you to " +
                    "shops, events, caffes, restaurants, clubs etc. Local businesses will be able to share information about promotions, discounts or " +
                    "upcoming events with all the users who find themselves nearby. This way you'll never miss another promotion or event at your favorite local caf� again!\n\n" +
                    "Local ads are coming soon to your city/neghbourhood so stay tuned! ");

        } else if (idPpTerms.equals("faq5")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What if i want to hide myself on the network (hide my personal posts)?");
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("The easiest way to make yourself invisible to other users is to hide your own personal posts. By doing so, other Gatherspot users " +
                    "won't be able to see your personal posts regardless of your location and the number of personal posts you share. You can hide your personal posts" +
                    " in My Profile by clicking on My profile settings button in the action bar in the upper right corner of the screen and by choosing to hide yourself.");

        } else if (idPpTerms.equals("faq6")) {
            title.setVisibility(View.GONE);
            getSupportActionBar().setTitle("What if i dont like the content shared on the app?");
            text.setTextColor(Color.parseColor("#000000"));
            text.setText("In case you find any personal or local post offensive or inappropriate, there is an option to report it in the upper left corner " +
                    "of the post. Also, if you'd like so, you can contact us and send us a private message via \"Report bad behaviour\" which " +
                    "is located in the Help section of the app to get our attention to the harmful content. Any post which receives enough reports or " +
                    "is noticed as harmful by our team will be erased and the author of the post might be banned from the network.\n" +
                    "You can also report a comment you find offensive or inappropriate. You can delete any comment you don't like/want on your personal posts");

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
