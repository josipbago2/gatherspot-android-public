package gatherspot.gatherspotapp.com.gatherspot.modules.loginreg;

/**
 * Created by Josip on 29.5.2015..
 */


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityLogin extends Activity {

    String username, password;
    EditText usernameInput, passwordInput;
    Button login, register;
    Intent intent;
    ImageView logo, background;
    SharedPreferences myTokenPrefs, loginPrefs, gcmPrefs, loginTypePrefs;
    SharedPreferences.Editor edt, edtLoginPrefs, edtLoginTypePrefs;
    ProgressBar pb;
    public static final String ENDPOINT = "https://service.gatherspot.me";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        login = (Button) findViewById(R.id.loginButton);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        usernameInput = (EditText) findViewById(R.id.usernameInput);
        passwordInput = (EditText) findViewById(R.id.passwordInput);
        register = (Button) findViewById(R.id.btnRegister);
        background = (ImageView) findViewById(R.id.background);
        loginTypePrefs = getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        edtLoginTypePrefs = loginTypePrefs.edit();
        gcmPrefs = getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);

        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                intent = new Intent(ActivityLogin.this, ActivityRegister.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                username = usernameInput.getText().toString();
                password = passwordInput.getText().toString();
                pb.setVisibility(View.VISIBLE);
                register.setClickable(false);
                login.setClickable(false);

                ApiInterface api = InitHttpClient.getInstance().getApiInterface();

                api.Login(gcmPrefs.getString("gcmToken", ""), username, password, "password", new Callback<LoginItemToken>() {

                    @Override
                    public void failure(RetrofitError arg0) {
                        // TODO Auto-generated method stub
                        Response r = arg0.getResponse();
                        pb.setVisibility(View.INVISIBLE);
                        login.setClickable(true);
                        register.setClickable(true);
                        if (r != null && r.getStatus() == 400) {
                            Toast.makeText(ActivityLogin.this, "Your email and/or password are not correct", Toast.LENGTH_SHORT).show();
                        }
                        handleBan(arg0);
                    }

                    @Override
                    public void success(LoginItemToken arg0, Response arg1) {
                        myTokenPrefs = getSharedPreferences("MYTOKEN",
                                Context.MODE_PRIVATE);
                        edt = myTokenPrefs.edit();
                        edt.putString("mypass", password);
                        edt.putString("myemail", username);
                        edt.putString("mytoken", arg0.getToken());
                        edt.putString("myuserid", arg0.getUser_id());
                        edt.commit();
                        // prepoznavanje potrebne rute za handlanje 401 a relogin
                        edtLoginTypePrefs.putString("type", "email");
                        edtLoginTypePrefs.commit();
                        // if uvjet za prepoznavanje dali je prvi put za taj account login
                        // ako je prvi put za taj account onda treba setati podatke
                        if (arg0.getFirstLogin().equals("true")) {
                            loginPrefs = getSharedPreferences("login", Context.MODE_PRIVATE);
                            edtLoginPrefs = loginPrefs.edit();
                            // ako ide prvi put login
                            // onda da se upali i set profile pic uz username, na facebook login nema profile pic
                            edtLoginPrefs.putBoolean("fb", false);
                            edtLoginPrefs.commit();
                            intent = new Intent(ActivityLogin.this,
                                    ActivityRegisterUsername.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //recycleBitmaps();
                            startActivity(intent);
                        } else {
                            intent = new Intent(ActivityLogin.this,
                                    ActivityTutorial.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //recycleBitmaps();
                            startActivity(intent);
                        }

                    }

                });
            }
        });
    }

    private void recycleBitmaps() {
        Drawable drawable1 = background.getDrawable();
        if (drawable1 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable1;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable2 = logo.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable2;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(ActivityLogin.this);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //recycleBitmaps();
    }
}
