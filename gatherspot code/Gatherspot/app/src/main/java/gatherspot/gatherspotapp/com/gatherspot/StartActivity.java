package gatherspot.gatherspotapp.com.gatherspot;

/**
 * Created by Josip on 28.5.2015..
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.appevents.AppEventsLogger;
import com.crashlytics.android.Crashlytics;

import gatherspot.gatherspotapp.com.gatherspot.modules.loginreg.ActivityLogin;
import gatherspot.gatherspotapp.com.gatherspot.modules.loginreg.ActivityLoginOAuth;
import gatherspot.gatherspotapp.com.gatherspot.modules.loginreg.ActivityRegisterUsername;
import io.fabric.sdk.android.Fabric;


public class StartActivity extends AppCompatActivity {

    SharedPreferences myTokenPrefs;
    String token;
    Intent intent;

    private static final boolean GoogleFBAuth = true;
    // stavi true da preusmjeri na activity za google oauth


    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        myTokenPrefs = getSharedPreferences("MYTOKEN", Context.MODE_PRIVATE);
        token = myTokenPrefs.getString("mytoken", "");
        if (token.isEmpty()) {
            if (GoogleFBAuth) {
                intent = new Intent(StartActivity.this, ActivityLoginOAuth.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                intent = new Intent(StartActivity.this, ActivityLogin.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        } else {
            // ako korisnik stisne back prije uspostave usernamea
            // username mora biti unesen prije korištenja aplikacije nakon logina
            if (myTokenPrefs.getString("myusername", "").equals("")) {
                intent = new Intent(StartActivity.this, ActivityRegisterUsername.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                intent = new Intent(StartActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        }

    }

}
