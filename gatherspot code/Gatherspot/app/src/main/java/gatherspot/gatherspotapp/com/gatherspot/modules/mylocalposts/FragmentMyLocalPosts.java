package gatherspot.gatherspotapp.com.gatherspot.modules.mylocalposts;

/**
 * Created by Josip on 29.5.2015..
 */


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.listadapters.ListAdapterMyLocalPosts;
import gatherspot.gatherspotapp.com.gatherspot.modules.newpost.ActivityNewLocalPost;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.MyLocalPost;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;
import gatherspot.gatherspotapp.com.gatherspot.utils.EndlessScrollListener;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class FragmentMyLocalPosts extends Fragment implements ObservableScrollViewCallbacks {

    SharedPreferences myTokenPrefs, resumeHandler;
    ObservableListView lv1;
    Button hintAddLocalPost;
    View footerView, rootView, hintView;
    RelativeLayout hintBackground;
    String loginType;
    ProgressBar footerCircle;
    SharedPreferences loginTypePrefs, gcmPrefs, backgroundImagePrefs;
    SharedPreferences.Editor edtToken, edtResume;
    public static final String ENDPOINT = "https://service.gatherspot.me";
    ArrayList<MyLocalPost> listOfPosts;
    private String token;

    BitmapDrawable image;

    RelativeLayout background;

    android.support.v7.app.ActionBar ab;

    int pageCounter = 0;
    private static final String PAGE_SIZE = "7";

    ProgressBar circle;

    private Activity mActivity;

    public FragmentMyLocalPosts() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        ab = ((AppCompatActivity) activity).getSupportActionBar();
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        backgroundImagePrefs = mActivity.getSharedPreferences("IMAGE",
                Context.MODE_PRIVATE);
        resumeHandler = mActivity.getSharedPreferences("RESUMEHANDLER", Context.MODE_PRIVATE);
        edtResume = resumeHandler.edit();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.mylocalposts_layout,
                container, false);
        footerView = inflater.inflate(R.layout.feed_item_lf_circlescroll, null, false);
        lv1 = (ObservableListView) rootView.findViewById(R.id.list);
        background = (RelativeLayout) rootView.findViewById(R.id.background);
        footerCircle = (ProgressBar) footerView.findViewById(R.id.progressBarFooter);
        if (BackgroundImageHandler.fileExistance(mActivity)) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.45f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(getResources(),
                    BackgroundImageHandler.loadImageFromStorage(mActivity));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
        }
        lv1.addFooterView(footerView);
        // za actionbar animation
        lv1.setScrollViewCallbacks(this);

        lv1.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (page == 0) {

                } else {
                    requestData(page, true);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        myTokenPrefs = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");

        gcmPrefs = mActivity.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = mActivity.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        circle = (ProgressBar) rootView.findViewById(R.id.progressBarMlp);
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
            }
        }
        requestData(0, false);

    }


    private void requestData(final int pagingCounter, final boolean page) {

        final ApiInterface api = InitHttpClient.getInstance().getApiInterface();

        api.getMyLocalPosts(token, PAGE_SIZE, String.valueOf(pagingCounter), new Callback<ArrayList<MyLocalPost>>() {

            @Override
            public void failure(RetrofitError arg0) {
                handleBan(arg0);
                Response r = arg0.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (loginType.equals("facebook")) {
                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getMyLocalPosts(token, PAGE_SIZE, String.valueOf(pagingCounter), new Callback<ArrayList<MyLocalPost>>() {
                                            @Override
                                            public void success(ArrayList<MyLocalPost> arg0, Response response) {
                                                listOfPosts = arg0;
                                                if (page) {
                                                    setListPaging(listOfPosts);
                                                } else {
                                                    setList(listOfPosts);
                                                }

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    } else {
                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                    @Override
                                    public void success(LoginItemToken arg0, Response response) {
                                        edtToken.putString("mytoken", arg0.getToken());
                                        edtToken.putString("myuserid", arg0.getUser_id());
                                        edtToken.commit();
                                        token = arg0.getToken();
                                        api.getMyLocalPosts(token, PAGE_SIZE, String.valueOf(pagingCounter), new Callback<ArrayList<MyLocalPost>>() {
                                            @Override
                                            public void success(ArrayList<MyLocalPost> arg0, Response response) {
                                                /*
                                                listOfPosts = arg0;
                                                setList();
                                                */
                                                listOfPosts = arg0;
                                                if (page) {
                                                    setListPaging(listOfPosts);
                                                } else {
                                                    setList(listOfPosts);
                                                }
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                handleBan(error);
                                            }
                                        });
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        handleBan(error);
                                    }
                                });
                    }
                }
            }

            @Override
            public void success(ArrayList<MyLocalPost> arg0, Response arg1) {
                /*
                listOfPosts = arg0;
                setList();
                */
                listOfPosts = arg0;
                if (page) {
                    setListPaging(listOfPosts);
                } else {
                    setList(listOfPosts);
                }
            }

        });
    }

    private void setListPaging(ArrayList<MyLocalPost> list) {
        if (lv1.getAdapter() != null) {
            ((ListAdapterMyLocalPosts) ((HeaderViewListAdapter) lv1.getAdapter()).getWrappedAdapter()).addMore(list);
        }
        if (list.size() < Integer.parseInt(PAGE_SIZE)) {
            footerCircle.setVisibility(View.GONE);
        }
    }

    private void handleHint(ArrayList<MyLocalPost> list) {
        /*
        if (firstRun.getBoolean("tutorialpersonal", true)) {
        */
        if (lv1.getAdapter() == null) {
            addHeaderHint();
        }
        if (list.isEmpty()) {
            hintBackground.setVisibility(View.VISIBLE);
        } else {
            // edtFirstRun.putBoolean("tutorialpersonal", false);
            // edtFirstRun.commit();
            hintBackground.setVisibility(View.GONE);
        }
        /*
        }
        */

    }

    private void addHeaderHint() {
        hintView = mActivity.getLayoutInflater().inflate(R.layout.feed_item_hint, null, false);
        hintBackground = (RelativeLayout) hintView.findViewById(R.id.backgroundHint);
        hintAddLocalPost = (Button) hintView.findViewById(R.id.newPost);
        hintAddLocalPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPost = new Intent(mActivity, ActivityNewLocalPost.class);
                intentPost.putExtra("mylocalposts", "mylocalposts");
                startActivity(intentPost);
            }
        });
        TextView message1 = (TextView) hintView.findViewById(R.id.message);
        TextView message2 = (TextView) hintView.findViewById(R.id.message1);
        message1.setText("Review and manage your local posts you previously left somewhere.");
        message2.setText("Start by adding your local post to your current location and let anyone around your current location see it");
        hintAddLocalPost.setText("Add new local post");
        hintAddLocalPost.setTextColor(Color.parseColor("#660045"));
        hintBackground.setBackgroundColor(Color.parseColor("#660045"));
        lv1.addHeaderView(hintView);

    }

    private void setList(ArrayList<MyLocalPost> list) {
        if (list.isEmpty()) {
            handleHint(list);
        }
        circle.setVisibility(View.GONE);
        ListAdapterMyLocalPosts listAdapter = new ListAdapterMyLocalPosts(
                mActivity, list, token);
        lv1.setAdapter(listAdapter);
        if (list.size() < Integer.parseInt(PAGE_SIZE)) {
            footerCircle.setVisibility(View.GONE);
        }
        // liveFeedList.addAll(listOfPostsPersonal);
        // liveFeedList.addAll(listOfPostsLocal);
        /*
        yesNoResumeComments = resumeComm.getBoolean("yesNoComments", true);
        if (yesNoResumeComments == false) {
            // refill
            Log.d("LOG", "LOG");
            if (lv1.getAdapter() == null) {
                circle.setVisibility(View.GONE);
                ListAdapterMyLocalPosts listAdapter = new ListAdapterMyLocalPosts(
                        mActivity, listOfPosts, token);
                lv1.setAdapter(listAdapter);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            } else {
                Log.d("LISTLIVEFEED", String.valueOf(listOfPosts.size()));
                ((ListAdapterMyLocalPosts) lv1.getAdapter()).refill(listOfPosts);
                edt = resume.edit();
                edt.putBoolean("yesNoComments", true);
                edt.commit();
            }
        } else {
            Log.d("LOG2", "LOG2");
            circle.setVisibility(View.GONE);
            ListAdapterMyLocalPosts listAdapter = new ListAdapterMyLocalPosts(
                    mActivity, listOfPosts, token);
            lv1.setAdapter(listAdapter);
        }
        */
    }

    @Override
    public void onResume() {
        super.onResume();
        if (loginType.equals("facebook")) {
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
            }
        }
        if (ab != null) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
        if (resumeHandler.getBoolean("backtomylocal", false)) {
            edtResume.putBoolean("backtomylocal", false);
            edtResume.commit();
            FragmentMyLocalPosts fragment = (FragmentMyLocalPosts)
                    getFragmentManager().findFragmentByTag("FragmentMyLocalPosts");
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.replace(R.id.mainContent, fragment);
            tr.commit();
        }

        /*
        resumeComm = mActivity.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        resume = mActivity.getSharedPreferences("resume", Context.MODE_PRIVATE);
        yesNoResume = resume.getBoolean("yesNo", true);
        if (yesNoResume) {
            requestData();
        } else {
            edt = resume.edit();
            edt.putBoolean("yesNo", true);
            edt.commit();
        }
        */
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        // inflater.inflate(R.menu.mp_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b1) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (mActivity == null) {
            return;
        }
        if (ab == null) {
            return;
        }
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //listOfPosts = null;
        if (background.getBackground() != null) {
            background.getBackground().setCallback(null);
        }
        if (image != null) {
            image.setCallback(null);
            //image.getBitmap().recycle();
            image = null;
        }
    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(mActivity);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }
}