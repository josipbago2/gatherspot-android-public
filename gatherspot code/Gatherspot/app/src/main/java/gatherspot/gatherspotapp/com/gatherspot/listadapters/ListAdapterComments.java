package gatherspot.gatherspotapp.com.gatherspot.listadapters;

/**
 * Created by Josip on 29.5.2015..
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.modules.myuserprofile.ActivityUserProfile;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentItemFill;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListAdapterComments extends BaseAdapter {

    private Context context;
    private List<CommentItemFill> listData;
    private LayoutInflater layoutInflater;
    private int postType;
    String loginType;
    SharedPreferences myTokenPrefs, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edtToken;

    String userId, token, postId;
    ApiInterface api;
    PopupMenu popup;

    Picasso picasso;

    boolean identifier;
    // za prepoznavanje dali je intent iz my profile ili user profile u vezi dropmenua

    private static final int PERSONAL = 1;
    private static final int ADS = 2;
    private static final int LOCAL = 3;
    private static final int MY_PROFILE = 4;

    public ListAdapterComments(Context context, List<CommentItemFill> listData,
                               int postType, boolean identifier, String postId) {
        this.context = context;
        this.listData = listData;
        Collections.reverse(this.listData);
        layoutInflater = LayoutInflater.from(context);
        this.postType = postType;
        this.identifier = identifier;
        this.postId = postId;

        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");

        myTokenPrefs = context.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        token = myTokenPrefs.getString("mytoken", "");
        userId = myTokenPrefs.getString("myuserid", "");

        api = InitHttpClient.getInstance().getApiInterface();
        /*
         * insideUser = context.getSharedPreferences("insideuser",
		 * Context.MODE_PRIVATE);
		 *
		 * edt = insideUser.edit(); edt.putBoolean("inside", false);
		 * edt.commit();
		 */
        picasso = InitHttpClient.getInstance().getImageLoader();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listData.size();

    }


    public void refillNewComment(CommentItemFill commentItemFill) {
        listData.add(commentItemFill);
        this.notifyDataSetChanged();
    }

    public void pagingRefill(List<CommentItemFill> updatePage) {
        Collections.reverse(updatePage);
        listData.addAll(0, updatePage);
        this.notifyDataSetChanged();
    }


    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getViewTypeCount() {

        if (getCount() != 0)
            return getCount();

        return 1;
    }

	/*
     * public int getItemViewType(int position) { String type = ((ListItem)
	 * listData.get(position)).getType(); if (type == "header") { return 2; }
	 * else { return 1; } }
	 */

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.feed_item_c, null);

            RelativeLayout layout = (RelativeLayout) convertView
                    .findViewById(R.id.layoutComments);

            if (postType == PERSONAL || postType == MY_PROFILE) {
                layout.setBackgroundResource(R.drawable.personalpost_bg);
            } else if (postType == ADS) {
                layout.setBackgroundColor(Color.parseColor("#D2FB6648"));
            } else {
                layout.setBackgroundResource(R.drawable.localpost_bg);
            }
            holder.profilePic = (ImageView) convertView
                    .findViewById(R.id.profilePicC);
            holder.dropMenu = (ImageButton) convertView.findViewById(R.id.dropMenu);
            holder.status = (TextView) convertView.findViewById(R.id.statusC);
            holder.timeStamp = (TextView) convertView.findViewById(R.id.timeStampC);
            holder.username = (Button) convertView.findViewById(R.id.btnUsernameC);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ((listData.get(position)).getProfile_pic_url().isEmpty()
                || listData.get(position).getProfile_pic_url() == null) {
            holder.profilePic
                    .setImageResource(R.mipmap.ic_action_icon_person);
        } else {
            if (listData.get(position).getProfile_pic_url().contains("gatherspot")) {
                picasso.load((listData.get(position)).getProfile_pic_url())
                        .into(holder.profilePic);
            } else {
                Picasso.with(context)
                        .load((listData.get(position)).getProfile_pic_url())
                        .into(holder.profilePic);
            }
        }

        holder.dropMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View drop = holder.dropMenu;
                popup = new PopupMenu(context, drop);
                switch (postType) {
                    case PERSONAL:
                        personalOptions(position);
                        break;
                    case LOCAL:
                        localMyLocalOptions(position);
                        break;
                    case MY_PROFILE:
                        myPersonalOptions(position);
                        break;
                    case ADS:

                        break;
                    default:
                        localMyLocalOptions(position);
                        break;
                }

            }
        });

        holder.status.setText((listData.get(position)).getStatus());
        if (listData.get(position).getTimestamp().equals("Just now")) {
            holder.timeStamp.setText("Just now");
        } else {
            holder.timeStamp.setText((listData.get(position)).getTimestamp() + " ago");
        }
        holder.username.setText((listData.get(position)).getUsername());
        holder.username.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(context, ActivityUserProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userid", (listData.get(position)).getUser_id());
                context.startActivity(intent);

            }
        });
        if (userId.equals((listData.get(position)).getUser_id())) {
            holder.username.setClickable(false);
        }

        return convertView;
    }

    private void personalOptions(final int position) {
        if (identifier == true) {
            popup.getMenuInflater().inflate(R.menu.dropmenu_mycomment,
                    popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals("Delete")) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setTitle("Delete comment");
                        alertBuilder.setMessage("Delete this comment?");
                        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                api.deletePersonalComment(token,
                                        listData.get(position).getId(), new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {
                                                Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void failure(RetrofitError arg0) {
                                                handleBan(arg0);
                                                Response r = arg0.getResponse();
                                                if (r != null && r.getStatus() == 401) {
                                                    if (loginType.equals("facebook")) {
                                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deletePersonalComment(arg0.getToken(),
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deletePersonalComment(arg0.getToken(),
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    }
                                                }
                                            }
                                        });
                                listData.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    return true;
                }
            });
        } else {
            if (userId.equals(listData.get(position).getUser_id())) {
                popup.getMenuInflater().inflate(R.menu.dropmenu_mycomment,
                        popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Delete")) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                            alertBuilder.setTitle("Delete comment");
                            alertBuilder.setMessage("Delete this comment?");
                            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    api.deletePersonalComment(token,
                                            listData.get(position).getId(), new Callback<Response>() {
                                                @Override
                                                public void success(Response response, Response response2) {
                                                    Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void failure(RetrofitError arg0) {
                                                    handleBan(arg0);
                                                    Response r = arg0.getResponse();
                                                    if (r != null && r.getStatus() == 401) {
                                                        if (loginType.equals("facebook")) {
                                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                        @Override
                                                                        public void success(LoginItemToken arg0, Response response) {
                                                                            edtToken.putString("mytoken", arg0.getToken());
                                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                                            edtToken.commit();
                                                                            token = arg0.getToken();
                                                                            api.deletePersonalComment(arg0.getToken(),
                                                                                    listData.get(position).getId(), new Callback<Response>() {
                                                                                        @Override
                                                                                        public void success(Response response, Response response2) {
                                                                                            Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                        }

                                                                                        @Override
                                                                                        public void failure(RetrofitError error) {
                                                                                            handleBan(error);
                                                                                        }
                                                                                    });
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {

                                                                        }
                                                                    });
                                                        }
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deletePersonalComment(arg0.getToken(),
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    }
                                                }
                                            });
                                    listData.remove(position);
                                    notifyDataSetChanged();
                                }
                            });
                            AlertDialog alertDialog = alertBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });

            } else {
                popup.getMenuInflater().inflate(R.menu.dropmenu_comment,
                        popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Report")) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                            alertBuilder.setTitle("Report");
                            alertBuilder.setMessage("Please, write us a reason for your report on this comment. " +
                                    "If required, we'll do necessary actions as soon as possible.");
                            final EditText message = new EditText(context);
                            alertBuilder.setView(message);
                            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertBuilder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    api.reportComment(token, listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                        @Override
                                        public void success(Response response, Response response2) {
                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void failure(RetrofitError arg0) {
                                            handleBan(arg0);
                                            Response r = arg0.getResponse();
                                            if (r != null && r.getStatus() == 401) {
                                                if (loginType.equals("facebook")) {
                                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                @Override
                                                                public void success(LoginItemToken arg0, Response response) {
                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                    edtToken.commit();
                                                                    token = arg0.getToken();
                                                                    api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                        @Override
                                                                        public void success(Response response, Response response2) {

                                                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {

                                                                }
                                                            });
                                                } else {
                                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                @Override
                                                                public void success(LoginItemToken arg0, Response response) {
                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                    edtToken.commit();
                                                                    token = arg0.getToken();
                                                                    api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                        @Override
                                                                        public void success(Response response, Response response2) {

                                                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {

                                                                }
                                                            });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            AlertDialog alertDialog = alertBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });

            }
        }
        popup.show();
    }

    private void myPersonalOptions(final int position) {
        if (identifier == false) {
            if (!userId.equals(listData.get(position).getUser_id())) {
                popup.getMenuInflater().inflate(R.menu.dropmenu_comment,
                        popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Report")) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                            alertBuilder.setTitle("Report");
                            alertBuilder.setMessage("Please, write us a reason for your report on this comment. " +
                                    "If required, we'll do necessary actions as soon as possible.");
                            final EditText message = new EditText(context);
                            alertBuilder.setView(message);
                            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertBuilder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    api.reportComment(token, listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                        @Override
                                        public void success(Response response, Response response2) {
                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void failure(RetrofitError arg0) {
                                            handleBan(arg0);
                                            Response r = arg0.getResponse();
                                            if (r != null && r.getStatus() == 401) {
                                                if (loginType.equals("facebook")) {
                                                    api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                            AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                @Override
                                                                public void success(LoginItemToken arg0, Response response) {
                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                    edtToken.commit();
                                                                    token = arg0.getToken();
                                                                    api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                        @Override
                                                                        public void success(Response response, Response response2) {
                                                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {

                                                                }
                                                            });
                                                } else {
                                                    api.Login(gcmPrefs.getString("gcmToken", ""),
                                                            myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                @Override
                                                                public void success(LoginItemToken arg0, Response response) {
                                                                    edtToken.putString("mytoken", arg0.getToken());
                                                                    edtToken.putString("myuserid", arg0.getUser_id());
                                                                    edtToken.commit();
                                                                    token = arg0.getToken();
                                                                    api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                        @Override
                                                                        public void success(Response response, Response response2) {
                                                                            Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {
                                                                            handleBan(error);
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void failure(RetrofitError error) {

                                                                }
                                                            });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            AlertDialog alertDialog = alertBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });
            } else {
                popup.getMenuInflater().inflate(R.menu.dropmenu_mycomment,
                        popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Delete")) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                            alertBuilder.setTitle("Delete comment");
                            alertBuilder.setMessage("Delete this comment?");
                            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    api.deletePersonalComment(token,
                                            listData.get(position).getId(), new Callback<Response>() {
                                                @Override
                                                public void success(Response response, Response response2) {
                                                    Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void failure(RetrofitError arg0) {
                                                    handleBan(arg0);
                                                    Response r = arg0.getResponse();
                                                    if (r != null && r.getStatus() == 401) {
                                                        if (loginType.equals("facebook")) {
                                                            api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                    AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                        @Override
                                                                        public void success(LoginItemToken arg0, Response response) {
                                                                            edtToken.putString("mytoken", arg0.getToken());
                                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                                            edtToken.commit();
                                                                            token = arg0.getToken();
                                                                            api.deletePersonalComment(arg0.getToken(),
                                                                                    listData.get(position).getId(), new Callback<Response>() {
                                                                                        @Override
                                                                                        public void success(Response response, Response response2) {
                                                                                            Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                        }

                                                                                        @Override
                                                                                        public void failure(RetrofitError error) {
                                                                                            handleBan(error);
                                                                                        }
                                                                                    });
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {

                                                                        }
                                                                    });
                                                        } else {
                                                            api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                    myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                        @Override
                                                                        public void success(LoginItemToken arg0, Response response) {
                                                                            edtToken.putString("mytoken", arg0.getToken());
                                                                            edtToken.putString("myuserid", arg0.getUser_id());
                                                                            edtToken.commit();
                                                                            token = arg0.getToken();
                                                                            api.deletePersonalComment(arg0.getToken(),
                                                                                    listData.get(position).getId(), new Callback<Response>() {
                                                                                        @Override
                                                                                        public void success(Response response, Response response2) {
                                                                                            Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                        }

                                                                                        @Override
                                                                                        public void failure(RetrofitError error) {
                                                                                            handleBan(error);
                                                                                        }
                                                                                    });
                                                                        }

                                                                        @Override
                                                                        public void failure(RetrofitError error) {

                                                                        }
                                                                    });
                                                        }
                                                    }
                                                }
                                            });
                                    listData.remove(position);
                                    notifyDataSetChanged();
                                }
                            });
                            AlertDialog alertDialog = alertBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });
            }
        } else {
            popup.getMenuInflater().inflate(R.menu.dropmenu_mycomment,
                    popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals("Delete")) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setTitle("Delete comment");
                        alertBuilder.setMessage("Delete this comment?");
                        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                api.deletePersonalComment(token,
                                        listData.get(position).getId(), new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {
                                                Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void failure(RetrofitError arg0) {
                                                handleBan(arg0);
                                                Response r = arg0.getResponse();
                                                if (r != null && r.getStatus() == 401) {
                                                    if (loginType.equals("facebook")) {
                                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deletePersonalComment(arg0.getToken(),
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deletePersonalComment(arg0.getToken(),
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    }
                                                }
                                            }
                                        });
                                listData.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    return true;
                }
            });
        }
        popup.show();
    }

    private void localMyLocalOptions(final int position) {
        if (userId.equals(listData.get(position).getUser_id())) {
            popup.getMenuInflater().inflate(R.menu.dropmenu_mycomment,
                    popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals("Delete")) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setTitle("Delete comment");
                        alertBuilder.setMessage("Delete this comment?");
                        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                api.deleteLocalComment(token, postId,
                                        listData.get(position).getId(), new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {
                                                Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void failure(RetrofitError arg0) {
                                                handleBan(arg0);
                                                Response r = arg0.getResponse();
                                                if (r != null && r.getStatus() == 401) {
                                                    if (loginType.equals("facebook")) {
                                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deleteLocalComment(arg0.getToken(), postId,
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    } else {
                                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                                    @Override
                                                                    public void success(LoginItemToken arg0, Response response) {
                                                                        edtToken.putString("mytoken", arg0.getToken());
                                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                                        edtToken.commit();
                                                                        token = arg0.getToken();
                                                                        api.deleteLocalComment(arg0.getToken(), postId,
                                                                                listData.get(position).getId(), new Callback<Response>() {
                                                                                    @Override
                                                                                    public void success(Response response, Response response2) {
                                                                                        Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show();
                                                                                    }

                                                                                    @Override
                                                                                    public void failure(RetrofitError error) {
                                                                                        handleBan(error);
                                                                                    }
                                                                                });
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {

                                                                    }
                                                                });
                                                    }
                                                }
                                            }
                                        });
                                listData.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    return true;
                }
            });

        } else {
            popup.getMenuInflater().inflate(R.menu.dropmenu_comment,
                    popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals("Report")) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setTitle("Report");
                        alertBuilder.setMessage("Please, write us a reason for your report on this comment. " +
                                "If required, we'll do necessary actions as soon as possible.");
                        final EditText message = new EditText(context);
                        alertBuilder.setView(message);
                        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertBuilder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                api.reportComment(token, listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                    @Override
                                    public void success(Response response, Response response2) {
                                        Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void failure(RetrofitError arg0) {
                                        handleBan(arg0);
                                        Response r = arg0.getResponse();
                                        if (r != null && r.getStatus() == 401) {
                                            if (loginType.equals("facebook")) {
                                                api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                        AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                            @Override
                                                            public void success(LoginItemToken arg0, Response response) {
                                                                edtToken.putString("mytoken", arg0.getToken());
                                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                                edtToken.commit();
                                                                token = arg0.getToken();
                                                                api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                    @Override
                                                                    public void success(Response response, Response response2) {
                                                                        Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {

                                                            }
                                                        });
                                            } else {
                                                api.Login(gcmPrefs.getString("gcmToken", ""),
                                                        myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                            @Override
                                                            public void success(LoginItemToken arg0, Response response) {
                                                                edtToken.putString("mytoken", arg0.getToken());
                                                                edtToken.putString("myuserid", arg0.getUser_id());
                                                                edtToken.commit();
                                                                token = arg0.getToken();
                                                                api.reportComment(arg0.getToken(), listData.get(position).getId(), message.getText().toString(), new Callback<Response>() {
                                                                    @Override
                                                                    public void success(Response response, Response response2) {
                                                                        Toast.makeText(context, "Comment reported", Toast.LENGTH_LONG).show();
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {

                                                            }
                                                        });
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    return true;
                }
            });

        }
        popup.show();
    }

    static class ViewHolder {
        ImageButton dropMenu;
        ImageView profilePic;
        Button username;
        TextView status, timeStamp;

    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

}
