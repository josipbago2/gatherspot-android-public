package gatherspot.gatherspotapp.com.gatherspot.modules.newpost;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.utils.BackgroundImageHandler;

/**
 * Created by Josip on 21.9.2015..
 */
public class ActivityNewPost extends AppCompatActivity {

    SharedPreferences resumeHandler, firstRun;
    SharedPreferences.Editor edtResume, edtFirstRun;
    Button nextPersonal, nextLocal;

    BitmapDrawable image;

    LinearLayout background;

    RelativeLayout newPersonal, newLocal, hintPersonal, hintLocal;

    String intentUrlData = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpost_layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Choose type of post");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));

        background = (LinearLayout) findViewById(R.id.background);

        if (BackgroundImageHandler.fileExistance(ActivityNewPost.this)) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.45f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image = new BitmapDrawable(getResources(),
                    BackgroundImageHandler.loadImageFromStorage(this));
            if (image != null) {
                image.setColorFilter(filter);
                if (Build.VERSION.SDK_INT > 15) {
                    background.setBackground(image);
                } else {
                    background.setBackgroundDrawable(image);
                }
            }
        }

        firstRun = getSharedPreferences("FIRST", Context.MODE_PRIVATE);
        edtFirstRun = firstRun.edit();

        if (getIntent().getAction() != null) {
            if (getIntent().getAction().equals(Intent.ACTION_SEND)) {
                intentUrlData = getIntent().getStringExtra(Intent.EXTRA_TEXT);
            }
        }

        resumeHandler = getSharedPreferences("RESUMEHANDLER", Context.MODE_PRIVATE);
        edtResume = resumeHandler.edit();

        newPersonal = (RelativeLayout) findViewById(R.id.containerPersonal);
        newLocal = (RelativeLayout) findViewById(R.id.containerLocal);


        newPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityNewPost.this, ActivityNewPersonalPost.class);
                if (intentUrlData != null) {
                    intent.putExtra("shared", intentUrlData);
                }
                intent.putExtra("FromLiveFeed", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        newLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityNewPost.this, ActivityNewLocalPost.class);
                if (intentUrlData != null) {
                    intent.putExtra("shared", intentUrlData);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        if (firstRun.getBoolean("tutorialnewpost", true)) {


            hintLocal = (RelativeLayout) findViewById(R.id.containerHintLocal);
            hintPersonal = (RelativeLayout) findViewById(R.id.containerHintPersonal);
            nextPersonal = (Button) findViewById(R.id.nextPersonal);
            nextLocal = (Button) findViewById(R.id.nextLocal);

            hintPersonal.setVisibility(View.VISIBLE);
            newLocal.setVisibility(View.GONE);
            newPersonal.setClickable(false);

            nextPersonal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newPersonal.setClickable(true);
                    newLocal.setClickable(false);

                    hintPersonal.setVisibility(View.GONE);
                    newLocal.setVisibility(View.VISIBLE);

                    hintLocal.setVisibility(View.VISIBLE);
                    newPersonal.setVisibility(View.GONE);
                }
            });

            nextLocal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newLocal.setClickable(true);
                    hintLocal.setVisibility(View.GONE);
                    newPersonal.setVisibility(View.VISIBLE);
                    edtFirstRun.putBoolean("tutorialnewpost", false);
                    edtFirstRun.commit();
                }
            });

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        edtResume.putBoolean("resume?", true);
        edtResume.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                edtResume.putBoolean("resume?", true);
                edtResume.commit();
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
