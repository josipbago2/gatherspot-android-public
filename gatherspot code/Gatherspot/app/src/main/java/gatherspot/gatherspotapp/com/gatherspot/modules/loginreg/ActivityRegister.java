package gatherspot.gatherspotapp.com.gatherspot.modules.loginreg;

/**
 * Created by Josip on 29.5.2015..
 */

import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.RegisterItemResponse;
import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

public class ActivityRegister extends Activity {

    String username, password;
    EditText usernameInput, passwordInput, passwordInputCheck;
    Button register;
    ImageView logo, background;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        register = (Button) findViewById(R.id.registerButton);
        usernameInput = (EditText) findViewById(R.id.usernameInput);
        passwordInput = (EditText) findViewById(R.id.passwordInput);
        passwordInputCheck = (EditText) findViewById(R.id.passwordInputCheck);
        background = (ImageView) findViewById(R.id.background);

        if (getEmail() != null) {
            usernameInput.setText(getEmail());
        }


        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isValidEmail(usernameInput.getText().toString())) {
                    if (passwordInput.getText().toString().length() > 5) {
                        if (passwordInput.getText().toString().equals(passwordInputCheck.getText().toString())) {
                            pb.setVisibility(View.VISIBLE);
                            register.setClickable(false);
                            username = usernameInput.getText().toString();
                            password = passwordInput.getText().toString();

                            ApiInterface api = InitHttpClient.getInstance().getApiInterface();
                            api.Register(username, password, "password", new Callback<RegisterItemResponse>() {

                                @Override
                                public void success(RegisterItemResponse arg0, Response arg1) {
                                    //recycleBitmaps();
                                    finish();

                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Response r = error.getResponse();
                                    pb.setVisibility(View.GONE);
                                    register.setClickable(true);
                                    if (r != null && r.getStatus() == 400) {
                                        Toast.makeText(ActivityRegister.this, "Already registered", Toast.LENGTH_SHORT).show();
                                    }
                                }


                            });
                        } else {
                            Toast.makeText(ActivityRegister.this, "Confirmed password doesn't match your password", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ActivityRegister.this, "Password must be at least 6 characters long", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityRegister.this, "Not a valid email address.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void recycleBitmaps() {
        Drawable drawable1 = background.getDrawable();
        if (drawable1 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable1;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
        Drawable drawable2 = logo.getDrawable();
        if (drawable2 instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable2;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.recycle();
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private String getEmail() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();
        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }
        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            return email;
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!register.isClickable()) {
            register.setClickable(true);
        }
        //recycleBitmaps();
    }
}

