package gatherspot.gatherspotapp.com.gatherspot.listadapters;

/**
 * Created by Josip on 29.5.2015..
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import gatherspot.gatherspotapp.com.gatherspot.utils.ApiInterface;
import gatherspot.gatherspotapp.com.gatherspot.modules.shared.ActivityComments;
import gatherspot.gatherspotapp.com.gatherspot.utils.InitHttpClient;
import gatherspot.gatherspotapp.com.gatherspot.R;
import gatherspot.gatherspotapp.com.gatherspot.pojo.CommentData;
import gatherspot.gatherspotapp.com.gatherspot.pojo.LoginItemToken;
import gatherspot.gatherspotapp.com.gatherspot.pojo.PersonalPostItem;
import gatherspot.gatherspotapp.com.gatherspot.pojo.UpDownVoteResponse;
import gatherspot.gatherspotapp.com.gatherspot.utils.FullScreenImageActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListAdapterMyProfile extends BaseAdapter {

    private String token, myUserId;
    private List<PersonalPostItem> listData;
    String loginType;
    SharedPreferences resume, resumeComments, myPersonalExistPrefs, myTokenPrefs, loginTypePrefs, gcmPrefs;
    SharedPreferences.Editor edt, edt2, edtToken;
    ApiInterface api;
    private LayoutInflater layoutInflater;
    private Context context;

    Picasso picasso;

    public ListAdapterMyProfile(Context context,
                                List<PersonalPostItem> listOfMyPersonal, String token) {
        this.listData = listOfMyPersonal;
        this.token = token;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        api = InitHttpClient.getInstance().getApiInterface();

        gcmPrefs = context.getApplicationContext().
                getSharedPreferences("GCMPREF", Context.MODE_PRIVATE);
        loginTypePrefs = context.getSharedPreferences("LOGINTYPE",
                Context.MODE_PRIVATE);
        loginType = loginTypePrefs.getString("type", "");
        myTokenPrefs = context.getSharedPreferences("MYTOKEN",
                Context.MODE_PRIVATE);
        edtToken = myTokenPrefs.edit();
        myUserId = myTokenPrefs.getString("myuserid", "");
        picasso = InitHttpClient.getInstance().getImageLoader();

    }

    public void refill(List<PersonalPostItem> updateList) {
        listData.clear();
        listData.addAll(updateList);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public PersonalPostItem getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * public int getItemViewType(int position) { String type = ((ListItem)
     * listData.get(position)).getType(); if (type == "header") { return 1; }
     * else { return 2; } }
     */
    @Override
    public int getViewTypeCount() {

        if (getCount() != 0)
            return getCount();

        return 1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        final int postType = 3;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = layoutInflater.inflate(R.layout.feed_item_mp, null);
            holder.post = (RelativeLayout) convertView.findViewById(R.id.post);
            holder.upVote = (ImageButton) convertView.findViewById(R.id.btnUpVote);
            holder.downVote = (ImageButton) convertView
                    .findViewById(R.id.btnDownVote);
            holder.status = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);
            holder.timeStamp = (TextView) convertView.findViewById(R.id.timeStamp);
            holder.feedImage = (ImageView) convertView
                    .findViewById(R.id.feedImage1);

            holder.voteValue = (TextView) convertView.findViewById(R.id.voteValue);
            holder.comments = (Button) convertView.findViewById(R.id.btnComment);
            holder.deletePost = (ImageButton) convertView
                    .findViewById(R.id.btnDeletePost);
            holder.progressBar = (ProgressBar) convertView
                    .findViewById(R.id.progressBar1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.postId = (listData.get(position)).getId();
        final String idPost = holder.postId;

        holder.status.setText(listData.get(position).getStatus());
        holder.timeStamp.setText(listData.get(position).getTimeStamp() + " ago");

        if ((listData.get(position)).getImage().isEmpty()) {

            holder.progressBar.setVisibility(View.GONE);
            holder.feedImage.setImageDrawable(null);
            holder.feedImage.setVisibility(View.GONE);
        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.feedImage.setVisibility(View.VISIBLE);

            picasso.load((listData.get(position)).getImage())
                    .placeholder(R.drawable.grey_image)
                    .into(holder.feedImage,
                            new com.squareup.picasso.Callback() {

                                @Override
                                public void onError() {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onSuccess() {
                                    // TODO Auto-generated method stub
                                    holder.progressBar.setVisibility(View.GONE);
                                }

                            });
        }

        holder.feedImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                resume = context.getSharedPreferences("MYTOKEN",
                        Context.MODE_PRIVATE);
                edt = resume.edit();
                edt.putBoolean("yesNo", false);
                edt.commit();
                Intent intent = new Intent(context,
                        FullScreenImageActivity.class);
                intent.putExtra("URLimage", (listData.get(position).getImage()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.downVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                api.downVotePersonal(token, idPost,
                        new Callback<UpDownVoteResponse>() {

                            @Override
                            public void failure(RetrofitError arg0) {
                                handleBan(arg0);
                                Response r = arg0.getResponse();
                                if (r != null && r.getStatus() == 401) {
                                    if (loginType.equals("facebook")) {
                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.downVotePersonal(token, idPost,
                                                                new Callback<UpDownVoteResponse>() {
                                                                    @Override
                                                                    public void success(UpDownVoteResponse arg0, Response response) {
                                                                        if (arg0.getResponse().equalsIgnoreCase("down")) {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes--;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setDownvoted("true");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.upVote.setColorFilter(Color
                                                                                    .parseColor("#80000000"));
                                                                            holder.upVote.setClickable(false);
                                                                        } else {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes++;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setDownvoted("false");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.upVote.setColorFilter(Color
                                                                                    .parseColor("#00000000"));
                                                                            holder.upVote.setClickable(true);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    } else {
                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.downVotePersonal(token, idPost,
                                                                new Callback<UpDownVoteResponse>() {
                                                                    @Override
                                                                    public void success(UpDownVoteResponse arg0, Response response) {
                                                                        if (arg0.getResponse().equalsIgnoreCase("down")) {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes--;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setDownvoted("true");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.upVote.setColorFilter(Color
                                                                                    .parseColor("#80000000"));
                                                                            holder.upVote.setClickable(false);
                                                                        } else {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes++;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setDownvoted("false");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.upVote.setColorFilter(Color
                                                                                    .parseColor("#00000000"));
                                                                            holder.upVote.setClickable(true);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    }
                                }

                            }

                            @Override
                            public void success(UpDownVoteResponse arg0,
                                                Response arg1) {
                                if (arg0.getResponse().equalsIgnoreCase("down")) {
                                    int votes = Integer
                                            .parseInt(holder.voteValue
                                                    .getText().toString());
                                    votes--;
                                    (listData.get(position)).setUp_votes(String
                                            .valueOf(votes));
                                    (listData.get(position))
                                            .setDownvoted("true");
                                    holder.voteValue.setText(String
                                            .valueOf(votes));
                                    holder.upVote.setColorFilter(Color
                                            .parseColor("#80000000"));
                                    holder.upVote.setClickable(false);
                                } else {
                                    int votes = Integer
                                            .parseInt(holder.voteValue
                                                    .getText().toString());
                                    votes++;
                                    (listData.get(position)).setUp_votes(String
                                            .valueOf(votes));
                                    (listData.get(position))
                                            .setDownvoted("false");
                                    holder.voteValue.setText(String
                                            .valueOf(votes));
                                    holder.upVote.setColorFilter(Color
                                            .parseColor("#00000000"));
                                    holder.upVote.setClickable(true);
                                }
                            }

                        });

            }
        });

        holder.upVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                api.upVotePersonal(token, idPost,
                        new Callback<UpDownVoteResponse>() {

                            @Override
                            public void failure(RetrofitError arg0) {
                                handleBan(arg0);
                                Response r = arg0.getResponse();
                                if (r != null && r.getStatus() == 401) {
                                    if (loginType.equals("facebook")) {
                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.upVotePersonal(token, idPost,
                                                                new Callback<UpDownVoteResponse>() {
                                                                    @Override
                                                                    public void success(UpDownVoteResponse arg0, Response response) {
                                                                        if (arg0.getResponse().equalsIgnoreCase("up")) {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes++;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position)).setUpvoted("true");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.downVote.setColorFilter(Color
                                                                                    .parseColor("#80000000"));
                                                                            holder.downVote.setClickable(false);
                                                                        } else {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes--;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setUpvoted("false");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.downVote.setColorFilter(Color
                                                                                    .parseColor("#00000000"));
                                                                            holder.downVote.setClickable(true);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    } else {
                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.upVotePersonal(token, idPost,
                                                                new Callback<UpDownVoteResponse>() {
                                                                    @Override
                                                                    public void success(UpDownVoteResponse arg0, Response response) {
                                                                        if (arg0.getResponse().equalsIgnoreCase("up")) {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes++;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position)).setUpvoted("true");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.downVote.setColorFilter(Color
                                                                                    .parseColor("#80000000"));
                                                                            holder.downVote.setClickable(false);
                                                                        } else {
                                                                            int votes = Integer
                                                                                    .parseInt(holder.voteValue
                                                                                            .getText().toString());
                                                                            votes--;
                                                                            (listData.get(position)).setUp_votes(String
                                                                                    .valueOf(votes));
                                                                            (listData.get(position))
                                                                                    .setUpvoted("false");
                                                                            holder.voteValue.setText(String
                                                                                    .valueOf(votes));
                                                                            holder.downVote.setColorFilter(Color
                                                                                    .parseColor("#00000000"));
                                                                            holder.downVote.setClickable(true);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void failure(RetrofitError error) {
                                                                        handleBan(error);
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    }
                                }

                            }

                            @Override
                            public void success(UpDownVoteResponse arg0,
                                                Response arg1) {
                                if (arg0.getResponse().equalsIgnoreCase("up")) {
                                    int votes = Integer
                                            .parseInt(holder.voteValue
                                                    .getText().toString());
                                    votes++;
                                    (listData.get(position)).setUp_votes(String
                                            .valueOf(votes));
                                    (listData.get(position)).setUpvoted("true");
                                    holder.voteValue.setText(String
                                            .valueOf(votes));
                                    holder.downVote.setColorFilter(Color
                                            .parseColor("#80000000"));
                                    holder.downVote.setClickable(false);
                                } else {
                                    int votes = Integer
                                            .parseInt(holder.voteValue
                                                    .getText().toString());
                                    votes--;
                                    (listData.get(position)).setUp_votes(String
                                            .valueOf(votes));
                                    (listData.get(position))
                                            .setUpvoted("false");
                                    holder.voteValue.setText(String
                                            .valueOf(votes));
                                    holder.downVote.setColorFilter(Color
                                            .parseColor("#00000000"));
                                    holder.downVote.setClickable(true);
                                }
                            }
                        });
            }

        });

        holder.voteValue.setText(listData.get(position).getUp_votes());

        holder.upvoted = (listData.get(position)).getUpvoted();
        holder.downvoted = (listData.get(position)).getDownvoted();

        if (holder.upvoted.equalsIgnoreCase("true")) {
            holder.downVote.setClickable(false);
            holder.downVote.setColorFilter(Color.parseColor("#80000000"));
        } else {
            holder.downVote.setClickable(true);
            holder.downVote.setColorFilter(Color.parseColor("#00000000"));
        }
        if (holder.downvoted.equalsIgnoreCase("true")) {
            holder.upVote.setClickable(false);
            holder.upVote.setColorFilter(Color.parseColor("#80000000"));
        } else {
            holder.upVote.setClickable(true);
            holder.upVote.setColorFilter(Color.parseColor("#00000000"));
        }

        holder.comments.setText(listData.get(position).getComments_number()
                + " Comments");
        holder.post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeComments = context.getSharedPreferences("MYTOKEN",
                        Context.MODE_PRIVATE);
                edt = resumeComments.edit();
                edt.putBoolean("yesNoComments", false);
                edt.commit();

                CommentData commentData = new CommentData();
                commentData.setUserId(myUserId);
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setVoteValueSend(listData.get(position)
                        .getUp_votes());
                commentData.setPostId(idPost);

                if (holder.upVote.isClickable()
                        && holder.downVote.isClickable()) {
                    commentData.setUpvoted("");
                    commentData.setDownvoted("");
                } else {
                    if (holder.upVote.isClickable()) {
                        commentData.setUpvoted("true");
                    } else {
                        commentData.setUpvoted("");
                    }
                    if (holder.downVote.isClickable()) {
                        commentData.setDownvoted("true");
                    } else {
                        commentData.setDownvoted("");
                    }
                }

                if ((listData.get(position)).getImage().isEmpty()) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }

                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", postType);
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resumeComments = context.getSharedPreferences("MYTOKEN",
                        Context.MODE_PRIVATE);
                edt = resumeComments.edit();
                edt.putBoolean("yesNoComments", false);
                edt.commit();

                CommentData commentData = new CommentData();
                commentData.setUserId(myUserId);
                commentData.setStatusSend(listData.get(position).getStatus());
                commentData.setTimeStampSend(listData.get(position)
                        .getTimeStamp());
                commentData.setVoteValueSend(listData.get(position)
                        .getUp_votes());
                commentData.setPostId(idPost);

                if (holder.upVote.isClickable()
                        && holder.downVote.isClickable()) {
                    commentData.setUpvoted("");
                    commentData.setDownvoted("");
                } else {
                    if (holder.upVote.isClickable()) {
                        commentData.setUpvoted("true");
                    } else {
                        commentData.setUpvoted("");
                    }
                    if (holder.downVote.isClickable()) {
                        commentData.setDownvoted("true");
                    } else {
                        commentData.setDownvoted("");
                    }
                }

                if ((listData.get(position)).getImage().isEmpty()) {
                    commentData.setImageNumberFeedImage("");
                } else {
                    commentData.setImageNumberFeedImage((listData.get(position))
                            .getImage());
                }

                Intent intent = new Intent(context, ActivityComments.class);
                intent.putExtra("postType", postType);
                intent.putExtra("commentData", commentData);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.deletePost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                myPersonalExistPrefs = context.getSharedPreferences(
                        "mypersonalexist", Context.MODE_PRIVATE);
                edt2 = myPersonalExistPrefs.edit();


                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                alertBuilder.setMessage("Delete this post?");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        api.deletePersonalPost(token, idPost, new Callback<Response>() {

                            @Override
                            public void failure(RetrofitError arg0) {
                                handleBan(arg0);
                                Response r = arg0.getResponse();
                                if (r != null && r.getStatus() == 401) {
                                    if (loginType.equals("facebook")) {
                                        api.LoginFB(gcmPrefs.getString("gcmToken", ""),
                                                AccessToken.getCurrentAccessToken().getToken(), "facebook", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.deletePersonalPost(token, idPost, new Callback<Response>() {
                                                            @Override
                                                            public void success(Response response, Response response2) {

                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    } else {
                                        api.Login(gcmPrefs.getString("gcmToken", ""),
                                                myTokenPrefs.getString("myemail", ""), myTokenPrefs.getString("mypass", ""), "password", new Callback<LoginItemToken>() {
                                                    @Override
                                                    public void success(LoginItemToken arg0, Response response) {
                                                        edtToken.putString("mytoken", arg0.getToken());
                                                        edtToken.putString("myuserid", arg0.getUser_id());
                                                        edtToken.commit();
                                                        token = arg0.getToken();
                                                        api.deletePersonalPost(token, idPost, new Callback<Response>() {
                                                            @Override
                                                            public void success(Response response, Response response2) {

                                                            }

                                                            @Override
                                                            public void failure(RetrofitError error) {
                                                                handleBan(error);
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        handleBan(error);
                                                    }
                                                });
                                    }
                                }

                            }

                            @Override
                            public void success(Response arg0, Response arg1) {

                            }

                        });
                        listData.remove(position);

                        // ako je listData prazan isključi location updatese
                        /*
                        if (listData.isEmpty()) {
                            edt2.putBoolean("exist", false);
                            ((MainActivity) context).stopBackgroundService();
                        }else{
                            // ako su uključeni, počni location updates
                        }
                        */
                        notifyDataSetChanged();
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        RelativeLayout post;
        ImageButton deletePost;
        ProgressBar progressBar;
        Button comments;
        String postId, upvoted, downvoted;
        ImageView feedImage;
        ImageButton upVote, downVote;
        ExpandableTextView status;
        TextView timeStamp, voteValue;

    }

    private void handleBan(RetrofitError error) {
        Response r = error.getResponse();
        if (r != null && r.getStatus() == 403) {
            android.app.AlertDialog.Builder alertBuilder = new android.app.AlertDialog.Builder(context);
            alertBuilder.setTitle("Gatherspot");
            alertBuilder.setMessage("Due to misuse and receiving to many reports on your posts " +
                    "and/or comments, you have been banned from Gatherspot");
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

}
